import React from 'react';
import PropTypes from 'prop-types';
import {Component} from 'react';
import {connect} from 'react-redux'

import Checkbox from 'material-ui/Checkbox';
import TextField from 'material-ui/TextField';

import {Card, CardTitle} from 'material-ui/Card';
import {
  systemChangesAnswerSpeed, systemChangesCustomizeStandards,
  systemChangesMonitorDuplication
} from "../reducers";

export class BasicCheckCard extends Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    customize_standards: PropTypes.bool.isRequired,
    monitor_duplication: PropTypes.object.isRequired,
    answer_speed: PropTypes.object.isRequired,
    changesCustomizeStandards: PropTypes.func.isRequired,
    changeMonitorDuplication: PropTypes.func.isRequired,
    changeAnswerSpeed: PropTypes.func.isRequired,
  };

  render() {
    console.log(this.props.answer_speed);
    return (
      <Card style={{marginTop: 10}}>
        <CardTitle title="基本的な回答除外ロジック"/>
        <Checkbox
          label="除外対象のカスタマイズ"
          checked={this.props.customize_standards}
          onCheck={() => {
            this.props.changesCustomizeStandards(!this.props.customize_standards);
          }}
        />
        {(() => {
          if (this.props.customize_standards) {
            return (
              <div>
                <div style={{marginLeft: 40}}>
                  <Checkbox
                    label="モニタ重複対象"
                    checked={this.props.monitor_duplication.active}
                    onCheck={() => {
                      const flag = !this.props.monitor_duplication.active;
                      const newMonitorDuplication = {
                        active: flag,
                        user_agent: flag,
                        ip_address: flag,
                        sex: flag,
                        age: flag,
                        area: flag
                      };

                      this.props.changeMonitorDuplication(newMonitorDuplication);
                    }}
                  />
                  <div style={{marginLeft: 40}}>
                    <Checkbox
                      label="ユーザーエージェント"
                      checked={this.props.monitor_duplication.user_agent}
                      onCheck={() => {
                        const newMonitorDuplication = Object.assign({}, this.props.monitor_duplication);
                        newMonitorDuplication.user_agent = !this.props.monitor_duplication.user_agent;
                        newMonitorDuplication.active
                          = MonitorDuplication.getActivenessFromStandards(newMonitorDuplication);

                        this.props.changeMonitorDuplication(newMonitorDuplication);
                      }}
                    />
                    <Checkbox
                      label="IPアドレス"
                      checked={this.props.monitor_duplication.ip_address}
                      onCheck={() => {
                        const newMonitorDuplication = Object.assign({}, this.props.monitor_duplication);
                        newMonitorDuplication.ip_address = !this.props.monitor_duplication.ip_address;

                        newMonitorDuplication.active
                          = MonitorDuplication.getActivenessFromStandards(newMonitorDuplication);

                        this.props.changeMonitorDuplication(newMonitorDuplication);
                      }}
                    />
                    <Checkbox
                      label="性別"
                      checked={this.props.monitor_duplication.sex}
                      onCheck={() => {
                        const newMonitorDuplication = Object.assign({}, this.props.monitor_duplication);
                        newMonitorDuplication.sex = !this.props.monitor_duplication.sex;

                        newMonitorDuplication.active
                          = MonitorDuplication.getActivenessFromStandards(newMonitorDuplication);

                        this.props.changeMonitorDuplication(newMonitorDuplication);
                      }}
                    />
                    <Checkbox
                      label="年齢"
                      checked={this.props.monitor_duplication.age}
                      onCheck={() => {
                        const newMonitorDuplication = Object.assign({}, this.props.monitor_duplication);
                        newMonitorDuplication.age = !this.props.monitor_duplication.age;

                        newMonitorDuplication.active
                          = MonitorDuplication.getActivenessFromStandards(newMonitorDuplication);

                        this.props.changeMonitorDuplication(newMonitorDuplication);
                      }}
                    />
                    <Checkbox
                      label="地域"
                      checked={this.props.monitor_duplication.area}
                      onCheck={() => {
                        const newMonitorDuplication = Object.assign({}, this.props.monitor_duplication);
                        newMonitorDuplication.area = !this.props.monitor_duplication.area;
                        newMonitorDuplication.active
                          = MonitorDuplication.getActivenessFromStandards(newMonitorDuplication);

                        this.props.changeMonitorDuplication(newMonitorDuplication);
                      }}
                    />
                  </div>

                </div>
                <div style={{marginLeft: 40}}>
                  <Checkbox
                    label="回答速度チェック"
                    checked={this.props.answer_speed.active}
                    onCheck={() => {
                      const newAnswerSpeed = Object.assign({}, this.props.answer_speed);
                      newAnswerSpeed.active = !this.props.answer_speed.active;

                      this.props.changeAnswerSpeed(newAnswerSpeed);
                    }}
                  />
                  <div style={{marginLeft: 40}}>
                    全体のうち早い順で<TextField
                    value={this.props.answer_speed.cut_ratio}
                    onChange={(event, newValue) => {
                      const newAnswerSpeed = Object.assign({}, this.props.answer_speed);
                      newAnswerSpeed.cut_ratio = newValue;

                      this.props.changeAnswerSpeed(newAnswerSpeed);
                    }}
                    type="number"
                    name="speed_answer_cut_ratio"
                    style={{width: 70}}

                  />%の回答者を除外する。
                  </div>
                </div>
                <div style={{marginLeft: 40}}>

                </div>
              </div>

            );
          }
        })()};

      </Card>
    );
  }


}

class MonitorDuplication {
  static allStandardsAreInactive(monitorDuplication) {
    return monitorDuplication.user_agent === false
      && monitorDuplication.ip_address === false
      && monitorDuplication.sex === false
      && monitorDuplication.age === false
      && monitorDuplication.area === false;
  }

  static someStandardsAreActive(monitorDuplication) {
    return monitorDuplication.user_agent === false
      || monitorDuplication.ip_address === false
      || monitorDuplication.sex === false
      || monitorDuplication.age === false
      || monitorDuplication.area === false;
  }

  static getActivenessFromStandards(monitorDuplication) {
    let active = monitorDuplication.active;
    if (MonitorDuplication.allStandardsAreInactive(monitorDuplication)) {
      active = false;
    } else if (MonitorDuplication.someStandardsAreActive(monitorDuplication)) {
      active = true;
    }
    return active;
  }
}


const mapStateToProps = (state) => ({
  customize_standards: state.basic_check.customize_standards,
  monitor_duplication: state.basic_check.monitor_duplication,
  answer_speed: state.basic_check.answer_speed,
});

const mapDispatchToProps = (dispatch) => {
  return {
    changesCustomizeStandards: (activeFlag) => {
      dispatch(systemChangesCustomizeStandards(activeFlag));
    },
    changeMonitorDuplication: (monitorDuplication) => {
      dispatch(systemChangesMonitorDuplication(monitorDuplication));
    },
    changeAnswerSpeed: (answerSpeed) => {
      dispatch(systemChangesAnswerSpeed(answerSpeed));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BasicCheckCard);
