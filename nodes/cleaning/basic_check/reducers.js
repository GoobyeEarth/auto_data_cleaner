export const initialState = {
  customize_standards: false,
  monitor_duplication: {
    active: true,
    user_agent: true,
    ip_address: true,
    sex: true,
    age: true,
    area: true,
  },
  answer_speed: {
    active: true,
    cut_ratio: 1, //%
  },

};

export const ActionTypes = {
  SYSTEM_CHANGES_MONITOR_DUPLICATION: 'SYSTEM_CHANGES_MONITOR_DUPLICATION',
  SYSTEM_CHANGES_CUSTOMIZE_STANDARDS: `SYSTEM_CHANGES_CUSTOMIZE_STANDARDS`,
  SYSTEM_CHANGES_ANSWER_SPEED: 'SYSTEM_CHANGES_ANSWER_SPEED',
};


export const systemChangesCustomizeStandards = (customizeStandards) => {
  return {
    type: ActionTypes.SYSTEM_CHANGES_CUSTOMIZE_STANDARDS,
    customize_standards: customizeStandards,
  };
};

export const systemChangesMonitorDuplication= (monitorDuplication) => {
  return {
    type: ActionTypes.SYSTEM_CHANGES_MONITOR_DUPLICATION,
    monitor_duplication: monitorDuplication,
  };
};

export const systemChangesAnswerSpeed = (answerSpeed) => {
  return {
    type: ActionTypes.SYSTEM_CHANGES_ANSWER_SPEED,
    answer_speed: answerSpeed,
  };
};

export function basicCheckReducer(state = initialState, action) {
  let updated = state;
  switch (action.type) {
  case ActionTypes.SYSTEM_CHANGES_MONITOR_DUPLICATION:
    updated.monitor_duplication = action.monitor_duplication;
    return Object.assign({}, updated);

  case ActionTypes.SYSTEM_CHANGES_CUSTOMIZE_STANDARDS:
    updated.customize_standards = action.customize_standards;
    return Object.assign({}, updated);

  case ActionTypes.SYSTEM_CHANGES_ANSWER_SPEED:
    updated.answer_speed = action.answer_speed;
    return Object.assign({}, updated);

  default:
    return state
  }
}
