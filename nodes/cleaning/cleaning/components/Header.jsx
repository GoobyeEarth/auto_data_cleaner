import React from 'react';
import {Component} from 'react';

import AppBar from 'material-ui/AppBar';

export default class Header extends Component {
  constructor() {
    super();
  }

  render() {
    return (
      <div>
        <AppBar
          title="Title"
          iconClassNameRight="muidocs-icon-navigation-expand-more"
        />
      </div>
    );
  }
}