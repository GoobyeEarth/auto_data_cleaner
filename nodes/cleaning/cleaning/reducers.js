import {ACTION_TYPES} from "../file_upload/events";

export const REDUCER_TYPES = {
  RESPONDED_SYSTEM_SEE_FILE_AS_UPLOADED: 'RESPONDED_SYSTEM_SEE_FILE_AS_UPLOADED',
};

export function systemUploadsFile(cleaning_id) {
  return {
    type: REDUCER_TYPES.RESPONDED_SYSTEM_SEE_FILE_AS_UPLOADED,
    cleaning_id: cleaning_id
  };
}

export const CLEANING_STATUSES = {
  NEW: 'NEW',
  UPLOADING: 'UPLOADING',
  UPLOADED: 'UPLOADED',
};

export const initialState = {
  status: CLEANING_STATUSES.NEW,
  cleaning_id: null,
};


export const cleaningReducer = (state = initialState, action) => {
  let updated;
  switch (action.type) {
  case ACTION_TYPES.USER_UPLOADS_FILES:
    updated = state;
    updated.status = CLEANING_STATUSES.UPLOADING;
    return Object.assign({}, updated);

  case REDUCER_TYPES.RESPONDED_SYSTEM_SEE_FILE_AS_UPLOADED:
    updated = state;
    updated.cleaning_id = action.cleaning_id;
    updated.status = CLEANING_STATUSES.UPLOADED;
    return Object.assign({}, updated);
  default:
    return state;
  }
};