/* eslint-env browser */

import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import App from './react';
import {createCleaningStore} from './redux';

import axios from 'axios';
axios.defaults.xsrfCookieName = 'csrftoken';
axios.defaults.xsrfHeaderName = 'X-CSRFToken';

const store = createCleaningStore();

render(
  <Provider store={store}>
    <App/>
  </Provider>,
  document.getElementById('root')
);
