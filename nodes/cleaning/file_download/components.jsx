import React from 'react';
import {connect} from 'react-redux'
import {Component} from 'react';
import {Card, CardActions, CardTitle, CardText} from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import Checkbox from 'material-ui/Checkbox';
import PropTypes from 'prop-types';
import DjangoCSRFToken from 'django-react-csrftoken'
import {StateParser} from "../text_answer/reducers";

class FileDownloadCard extends Component {
  constructor() {
    super();

    this.state = {
      leave_flags: true,
    };
  }

  static PropTypes = {
    cleaning_id: PropTypes.number.isRequired,
    excluded_answers_list: PropTypes.array.isRequired,
    basic_check: PropTypes.object.isRequired,
  };

  render() {
    return (
      <Card style={{marginTop: 10}}>
        <CardTitle title="ファイルダウンロード"/>
        <CardText>
          <Checkbox
            checked={this.state.leave_flags}
            onCheck={() => {
                this.setState({leave_flags: !this.state.leave_flags});
              }}
            style={{marginBottom: 20}}
            label="除外対象者を除外せずにフラグとして出力する。"
          />
        </CardText>
        <CardActions>
          <RaisedButton
            label="ダウンロード"
            primary={true}
            onClick={() => {
              const form = document.forms.file_download;
              form.submit();
            }}
          />
          <form name='file_download' target='_blank' action='/cleaning/download/raw-data/' method='POST'>
            <DjangoCSRFToken/>
            <input type="hidden" name="cleaning_id" value={this.props.cleaning_id}/>
            <input type="hidden" name="leave_flags" value={this.state.leave_flags}/>
            <input type="hidden" name="excluded_answers_list"
                   value={JSON.stringify(this.props.excluded_answers_list)}/>
            <input type="hidden" name="basic_check"
                   value={JSON.stringify(this.props.basic_check)}/>
          </form>
        </CardActions>

      </Card>
    );
  }
}

const mapStateToProps = (state) => ({
  cleaning_id: state.cleaning.cleaning_id,
  excluded_answers_list: StateParser.getExcludedAnswersList(state.text_answer),
  basic_check: state.basic_check,
});

const mapDispatchToProps = (dispatch) => {
  return {}
};

export default connect(mapStateToProps, mapDispatchToProps)(FileDownloadCard);