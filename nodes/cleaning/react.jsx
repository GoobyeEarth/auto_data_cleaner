import React from 'react';
import {Component} from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {Grid, Row, Col} from 'react-flexbox-grid';

import Header from './cleaning/components/Header';
import SideBar from './cleaning/components/SideBar';
import UploadCard from "./file_upload/components";
import {FAExclusionLogicCardContainer} from './text_answer/exclusion_logic_card/components';
import {FAExclusionByHandContainer} from "./text_answer/exclusion_by_hand/components/index";
import FileDownloadCard from "./file_download/components";
import SingleAnswerCardContainer from "./single_answer/components";
import BasicCheckCardContainer from "./basic_check/components";

class App extends Component {
  constructor() {
    super();
  }

  render() {
    return (
      <div className="app">
        <MuiThemeProvider>
          <View/>
        </MuiThemeProvider>
      </div>
    );
  }
}


class View extends Component {
  constructor() {
    super();
  }

  render() {
    return (
      <div>
        <Grid>
          <Row>
            <Col xs={12}><Header/></Col>
            <Col xs={2}><SideBar/></Col>
            <Col xs={10}><MainView/></Col>
          </Row>
        </Grid>
      </div>
    );
  }
}


class MainView extends Component {
  constructor() {
    super();
  }

  render() {
    return (
      <div>
        <UploadCard/>
        <BasicCheckCardContainer />
        <SingleAnswerCardContainer/>
        <FAExclusionLogicCardContainer/>
        <FAExclusionByHandContainer/>
        <FileDownloadCard/>
      </div>
    );
  }
}

export default App;
