import React from 'react';
import PropTypes from 'prop-types';
import {Component} from 'react';
import {connect} from 'react-redux'

import {Card, CardActions, CardTitle, CardText} from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import {userChangeJudgement} from "../events";
import {TextAnswerExcludingTable} from "./parts";

export class FAExcludingByHandCard extends Component {
  constructor(props) {
    super(props);
    this.handleRowSelection = this.handleRowSelection.bind(this);
  }

  static propTypes = {
    unique_answers_list: PropTypes.object.isRequired,
    cleaning_id: PropTypes.number.isRequired,
    changeJudgment: PropTypes.func.isRequired,
  };

  handleRowSelection(selected_rows, column_no) {
    this.props.changeJudgment(selected_rows, column_no);
  }

  render() {
    return (
      <Card style={{marginTop: 10}}>
        <CardTitle title="FAの不正回答自動除外"/>
        <CardText>
          {(() => {
            const tables = [];
            Object.keys(this.props.unique_answers_list).forEach((column_no) => {
              tables.push(
                <TextAnswerExcludingTable
                  key={column_no}
                  unique_answers={this.props.unique_answers_list[column_no]}
                  handleRowSelection={
                    (selected_rows) => {
                      this.handleRowSelection(selected_rows, column_no)
                    }
                  }
                />
              );
            });
            return tables;
          })()}

        </CardText>
      </Card>
    );
  }
}


const mapStateToProps = (state) => ({
  unique_answers_list: state.text_answer.unique_answers_list,
  cleaning_id: state.cleaning.cleaning_id,
});

const mapDispatchToProps = (dispatch) => ({
    changeJudgment: (selected_answers, column_no) => {
      dispatch(userChangeJudgement(selected_answers, column_no));
    },
  });

export const
  FAExclusionByHandContainer = connect(mapStateToProps, mapDispatchToProps)(FAExcludingByHandCard);