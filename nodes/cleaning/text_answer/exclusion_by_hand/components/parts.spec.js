import assert from 'power-assert';
import {shallow} from 'enzyme';

import getMuiTheme from 'material-ui/styles/getMuiTheme';
import PropTypes from 'prop-types';
import {TextAnswerExcludingTable} from "./parts";

describe('createTableRowColumn', () => {
  it('displays', () => {
    const muiTheme = getMuiTheme();
    const wrapper = shallow(TextAnswerExcludingTable.createTableRowColumn(true, 'answer test'),
      {
        context: {muiTheme},
        childContextTypes: {muiTheme: PropTypes.object}
      });


    assert.deepEqual(wrapper.debug(),
      `<tr className={[undefined]} style={{...}}>
  <TableRowColumn style={{...}} hoverable={false} columnNumber={0} onClick={[Function]} onHover={[Function]} onHoverExit={[Function]}>
    answer test
  </TableRowColumn>
</tr>`);
  });
});
