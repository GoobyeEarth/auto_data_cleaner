import React from 'react';
import PropTypes from 'prop-types';
import {Component} from 'react';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';


export class TextAnswerExcludingTable extends Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    unique_answers: PropTypes.object.isRequired,
    handleRowSelection: PropTypes.func.isRequired,
  };

  static createTableRowColumn(selected, answer, key) {
    return (
      <TableRow
        selected={selected}
        key={key}
      >
        <TableRowColumn
          style={{wordWrap: 'break-word'}}
        >
          {answer}
        </TableRowColumn>
      </TableRow>
    );
  }

  static createTableBodies(answers) {
    const row_columns = [];
    let key = 0;
    answers.forEach(answer => {
      key += 1;
      row_columns.push(
        TextAnswerExcludingTable.createTableRowColumn(answer.judgement, answer.answer_value, key)
      );
    });
    return row_columns;
  }

  render() {
    return (
      <Table
        multiSelectable={true}
        onRowSelection={(selected_rows) => {
          this.props.handleRowSelection(selected_rows);

          return [1];
        }}
      >
        <TableHeader>
          <TableRow>
            <TableHeaderColumn>{this.props.unique_answers.column_header}</TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody
          deselectOnClickaway={false}
        >
          {(() => {
            return TextAnswerExcludingTable.createTableBodies(this.props.unique_answers.answers);
          })()}

        </TableBody>
      </Table>
    );
  }
}