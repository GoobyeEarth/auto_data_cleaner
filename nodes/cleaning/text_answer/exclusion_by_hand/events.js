import {put, takeEvery} from 'redux-saga/effects'
import {systemChangesUniqueAnswersList} from "../reducers";

export const ACTION_TYPES = {
  USER_CHANGE_ANSWERS_JUDGEMENT: 'USER_CHANGE_ANSWERS_JUDGEMENT',
};


export const userChangeJudgement = (selected_rows, column_no) => {
  return {
    type: ACTION_TYPES.USER_CHANGE_ANSWERS_JUDGEMENT,
    selected_answers: selected_rows,
    column_no: column_no,
  };
};

function* handleChangeJudgement(action) {
  yield put(systemChangesUniqueAnswersList(action.selected_answers, action.column_no));
}

export function* exclusionByHandSaga() {
  yield takeEvery(ACTION_TYPES.USER_CHANGE_ANSWERS_JUDGEMENT, handleChangeJudgement);
}