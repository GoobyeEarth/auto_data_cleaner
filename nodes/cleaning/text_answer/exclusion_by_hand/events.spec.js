import assert from "power-assert";
import {userChangeJudgement} from "./events";
import {systemSetsUniqueAnswersList} from "../reducers";
import {createCleaningStore} from "~/nodes/cleaning/redux";

describe('handleChangeJudgement', () => {
  it('change judgement', () => {
    const unique_answers_list = {
      "5": {
        "item_name": "",
        "column_header": "q1t1",
        "column_no": 5,
        "answers": [
          {
            "answer_value": "ナスが好きだから",
            "monitor_ids": [
              "100000001",
              "100000002"
            ],
            "too_less_words": false,
            "too_match_consonants": false,
            "redundant_word": false,
            "judgement": false
          }
        ]
      },
      "6": {
        "item_name": "",
        "column_header": "q1t2",
        "column_no": 6,
        "answers": [
          {
            "answer_value": "aaaa",
            "monitor_ids": [
              "100000001"
            ],
            "too_less_words": false,
            "too_match_consonants": false,
            "redundant_word": true,
            "judgement": true
          },
          {
            "answer_value": "test",
            "monitor_ids": [
              "100000002"
            ],
            "too_less_words": false,
            "too_match_consonants": true,
            "redundant_word": false,
            "judgement": false
          }
        ]
      }
    };

    const store = createCleaningStore();

    store.dispatch(systemSetsUniqueAnswersList(unique_answers_list));
    store.dispatch(userChangeJudgement([0], '5'));

    const expected = {
      unique_answers_list:
        {
          '5':
            {
              item_name: '',
              column_header: 'q1t1',
              column_no: 5,
              answers: [{
                answer_value: 'ナスが好きだから',
                monitor_ids: ['100000001', '100000002'],
                too_less_words: false,
                too_match_consonants: false,
                redundant_word: false,
                judgement: true
              }]
            },
          '6':
            {
              item_name: '',
              column_header: 'q1t2',
              column_no: 6,
              answers: [{
                answer_value: 'aaaa',
                monitor_ids: ['100000001'],
                too_less_words: false,
                too_match_consonants: false,
                redundant_word: true,
                judgement: true
              },
              {
                answer_value: 'test',
                monitor_ids: ['100000002'],
                too_less_words: false,
                too_match_consonants: true,
                redundant_word: false,
                judgement: false
              }]
            }
        }
    };

    assert.deepEqual(store.getState().text_answer, expected);
  });
});
