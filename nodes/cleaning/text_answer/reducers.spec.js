import assert from "power-assert";
import {selectedAnswersToUniqueAnswersList, StateParser} from "./reducers";

describe('selectedAnswersToUniqueAnswersList', () => {
  const uniqueAnswersList = {
    "5": {
      "item_name": "",
      "column_header": "q1t1",
      "column_no": 5,
      "answers": [
        {
          "answer_value": "ナスが好きだから",
          "monitor_ids": [
            "100000001",
            "100000002"
          ],
          "too_less_words": false,
          "too_match_consonants": false,
          "redundant_word": false,
          "judgement": false
        }
      ]
    },
    "6": {
      "item_name": "",
      "column_header": "q1t2",
      "column_no": 6,
      "answers": [
        {
          "answer_value": "aaaa",
          "monitor_ids": [
            "100000001"
          ],
          "too_less_words": false,
          "too_match_consonants": false,
          "redundant_word": true,
          "judgement": true
        },
        {
          "answer_value": "test",
          "monitor_ids": [
            "100000002"
          ],
          "too_less_words": false,
          "too_match_consonants": true,
          "redundant_word": false,
          "judgement": true
        }
      ]
    }
  };

  it('convert correctly', () => {
    const result = selectedAnswersToUniqueAnswersList([1], '6', uniqueAnswersList);
    assert.deepEqual(result, {
      "5": {
        "item_name": "",
        "column_header": "q1t1",
        "column_no": 5,
        "answers": [
          {
            "answer_value": "ナスが好きだから",
            "monitor_ids": [
              "100000001",
              "100000002"
            ],
            "too_less_words": false,
            "too_match_consonants": false,
            "redundant_word": false,
            "judgement": false
          }
        ]
      },
      "6": {
        "item_name": "",
        "column_header": "q1t2",
        "column_no": 6,
        "answers": [
          {
            "answer_value": "aaaa",
            "monitor_ids": [
              "100000001"
            ],
            "too_less_words": false,
            "too_match_consonants": false,
            "redundant_word": true,
            "judgement": false,
          },
          {
            "answer_value": "test",
            "monitor_ids": [
              "100000002"
            ],
            "too_less_words": false,
            "too_match_consonants": true,
            "redundant_word": false,
            "judgement": true
          }
        ]
      }
    });

  });

  it('all means all row selected', () => {
    const result = selectedAnswersToUniqueAnswersList('all', '6', uniqueAnswersList);
    assert.deepEqual(result, {
      "5": {
        "item_name": "",
        "column_header": "q1t1",
        "column_no": 5,
        "answers": [
          {
            "answer_value": "ナスが好きだから",
            "monitor_ids": [
              "100000001",
              "100000002"
            ],
            "too_less_words": false,
            "too_match_consonants": false,
            "redundant_word": false,
            "judgement": false,
          }
        ]
      },
      "6": {
        "item_name": "",
        "column_header": "q1t2",
        "column_no": 6,
        "answers": [
          {
            "answer_value": "aaaa",
            "monitor_ids": [
              "100000001"
            ],
            "too_less_words": false,
            "too_match_consonants": false,
            "redundant_word": true,
            "judgement": true,
          },
          {
            "answer_value": "test",
            "monitor_ids": [
              "100000002"
            ],
            "too_less_words": false,
            "too_match_consonants": true,
            "redundant_word": false,
            "judgement": true
          }
        ]
      }
    });

  });

  it('none means 0 row selected', () => {
    const result = selectedAnswersToUniqueAnswersList('none', '6', uniqueAnswersList);
    assert.deepEqual(result, {
      "5": {
        "item_name": "",
        "column_header": "q1t1",
        "column_no": 5,
        "answers": [
          {
            "answer_value": "ナスが好きだから",
            "monitor_ids": [
              "100000001",
              "100000002"
            ],
            "too_less_words": false,
            "too_match_consonants": false,
            "redundant_word": false,
            "judgement": false,
          }
        ]
      },
      "6": {
        "item_name": "",
        "column_header": "q1t2",
        "column_no": 6,
        "answers": [
          {
            "answer_value": "aaaa",
            "monitor_ids": [
              "100000001"
            ],
            "too_less_words": false,
            "too_match_consonants": false,
            "redundant_word": true,
            "judgement": false,
          },
          {
            "answer_value": "test",
            "monitor_ids": [
              "100000002"
            ],
            "too_less_words": false,
            "too_match_consonants": true,
            "redundant_word": false,
            "judgement": false
          }
        ]
      }
    });

  });

});

describe('StateParser.getExcludedAnswersList', () => {
  it('returns correctly', () => {
    const unique_answers_list = {
      "5": {
        "item_name": "",
        "column_header": "q1t1",
        "column_no": 5,
        "answers": [
          {
            "answer_value": "ナスが好きだから",
            "monitor_ids": [
              "100000001",
              "100000002"
            ],
            "too_less_words": false,
            "too_match_consonants": false,
            "redundant_word": false,
            "judgement": false
          }
        ]
      },
      "6": {
        "item_name": "",
        "column_header": "q1t2",
        "column_no": 6,
        "answers": [
          {
            "answer_value": "aaaa",
            "monitor_ids": [
              "100000001"
            ],
            "too_less_words": false,
            "too_match_consonants": false,
            "redundant_word": true,
            "judgement": true
          },
          {
            "answer_value": "test",
            "monitor_ids": [
              "100000002"
            ],
            "too_less_words": false,
            "too_match_consonants": true,
            "redundant_word": false,
            "judgement": true
          }
        ]
      }
    };

    const state = {
      unique_answers_list: unique_answers_list,
    };

    const excludedAnswersList = StateParser.getExcludedAnswersList(state);

    assert.deepEqual(excludedAnswersList, { '5': [], '6': [ 'aaaa', 'test' ] });
  });

  it('returns {} if unique_answers_list is {}', () => {
    const unique_answers_list = {};

    const state = {
      unique_answers_list: unique_answers_list,
    };

    const excludedAnswersList = StateParser.getExcludedAnswersList(state);

    assert.deepEqual(excludedAnswersList, {});
  });

});