export const REDUCER_TYPES = {
  SYSTEM_SETS_UNIQUE_ANSWERS_LIST: 'SYSTEM_SETS_UNIQUE_ANSWERS_LIST',
  SYSTEM_CHANGES_UNIQUE_ANSWERS_LIST: 'SYSTEM_CHANGES_UNIQUE_ANSWERS_LIST',
};

export const systemSetsUniqueAnswersList = (unique_answers_list) => {
  return {
    type: REDUCER_TYPES.SYSTEM_SETS_UNIQUE_ANSWERS_LIST,
    unique_answers_list: unique_answers_list
  };
};

export const systemChangesUniqueAnswersList = (selectedRows, columnNo) => {
  return {
    type: REDUCER_TYPES.SYSTEM_CHANGES_UNIQUE_ANSWERS_LIST,
    selected_rows: selectedRows,
    column_no: columnNo,
  };
};

export const selectedAnswersToUniqueAnswersList
  = (selectedRows, columnNo, uniqueAnswersList) => {
    let unique_answers = uniqueAnswersList[columnNo];

    if (typeof selectedRows === 'string') {
      if (selectedRows === 'all') {
        for (const key in unique_answers.answers) {
          unique_answers.answers[key].judgement = true;
        }
      } else if (selectedRows === 'none') {
        for (const key in unique_answers.answers) {
          unique_answers.answers[key].judgement = false;
        }
      }

    } else {
      for (const key in unique_answers.answers) {
        unique_answers.answers[key].judgement = selectedRows.includes(parseInt(key));
      }
    }

    uniqueAnswersList[columnNo] = unique_answers;

    return uniqueAnswersList;
  };

export const initialState = {
  unique_answers_list: {},
};

export const textAnswerReducer = (state = initialState, action) => {
  let updated;
  switch (action.type) {
  case REDUCER_TYPES.SYSTEM_SETS_UNIQUE_ANSWERS_LIST: {
    updated = state;
    updated.unique_answers_list = action.unique_answers_list;
    return Object.assign({}, updated);
  }
  case REDUCER_TYPES.SYSTEM_CHANGES_UNIQUE_ANSWERS_LIST: {
    const unique_answers_list = selectedAnswersToUniqueAnswersList(
      action.selected_rows,
      action.column_no,
      state.unique_answers_list
    );
    return {
      ...state,
      unique_answers_list: Object.assign({}, unique_answers_list)
    };
  }
  default:
    return state;
  }
};


export class StateParser {
  static getExcludedAnswersList(state) {
    const unique_answers_list =state.unique_answers_list;
    const excluded_answers_list = {};
    Object.keys(unique_answers_list).forEach((key) => {
      excluded_answers_list[key] = [];
      unique_answers_list[key].answers.forEach((answer_info) => {
        if (answer_info.judgement) {
          excluded_answers_list[key].push(answer_info.answer_value);
        }
      });

    });
    return excluded_answers_list;
  }


}