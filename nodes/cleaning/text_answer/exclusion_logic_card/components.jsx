import React from 'react';
import {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux'

import {Card, CardActions, CardTitle, CardText} from 'material-ui/Card';
import Checkbox from 'material-ui/Checkbox';
import RaisedButton from 'material-ui/RaisedButton';
import {setExclusionSetting}  from './events';


export class FAExclusionLogicCard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      customize_standards: false,
      standards: {
        too_less_words: true,
        too_match_consonants: true,
        redundant_word: true,
      },
      confirm_after_upload: true,
    }
  }

  static propTypes = {
    cleaning_id: PropTypes.number.isRequired,
    handleExclusionSetting: PropTypes.func.isRequired,
  };

  handleExclusionSetting() {
    this.props.handleExclusionSetting(this.props.cleaning_id, this.state.standards);
  }

  render() {
    return (
      <Card style={{marginTop: 10}}>
        <CardTitle title="FAの不正回答除外ロジック設定"/>
        <CardText>
          <div style={{marginBottom: 20}}>
            <Checkbox
              label="除外対象のカスタマイズ"
              checked={this.state.customize_standards}
              onCheck={() => {
                this.setState({customize_standards: !this.state.customize_standards});
              }}
            />
            {(() => {
              if (this.state.customize_standards) {
                return (
                  <div style={{marginLeft: 40}}>
                    <Checkbox
                      label="1文字のみの場合は除外対象とする。"
                      checked={this.state.standards.too_less_words}
                      onCheck={() => {
                        const new_standards = this.state.standards;
                        new_standards.too_less_words = !this.state.standards.too_less_words;
                        this.setState({standards: new_standards});
                      }}
                    />
                    <Checkbox
                      label="7割以上子音の回答を除外対象とする。"
                      checked={this.state.standards.too_match_consonants}
                      onCheck={() => {
                        const new_standards = this.state.standards;
                        new_standards.too_match_consonants = !this.state.standards.too_match_consonants;
                        this.setState({standards: new_standards});
                      }}
                    />
                    <Checkbox
                      label="同一文字が3回繰り返されたら除外対象とする。"
                      checked={this.state.standards.redundant_word}
                      onCheck={() => {
                        const new_standards = this.state.standards;
                        new_standards.redundant_word = !this.state.standards.redundant_word;
                        this.setState({standards: new_standards});
                      }}
                    />
                  </div>
                );
              }
            })()}
          </div>

          <Checkbox
            style={{marginBottom: 20}}
            label="FAの回答を確認する"
            checked={this.state.confirm_after_upload}
            onCheck={() => {
              this.setState({confirm_after_upload: !this.state.confirm_after_upload});
            }}
          />
        </CardText>
        <CardActions>
          <RaisedButton
            label="実行"
            primary={true}
            onClick={() => {
              this.handleExclusionSetting();
            }}
          />
        </CardActions>
      </Card>
    );
  }

}

const mapStateToProps = (state) => ({
  cleaning_id: state.cleaning.cleaning_id,
});

const mapDispatchToProps = (dispatch) => ({
  handleExclusionSetting: (cleaning_id, standards) => {
    dispatch(setExclusionSetting(cleaning_id, standards));
  }
});

export const FAExclusionLogicCardContainer =  connect(mapStateToProps, mapDispatchToProps)(FAExclusionLogicCard);
