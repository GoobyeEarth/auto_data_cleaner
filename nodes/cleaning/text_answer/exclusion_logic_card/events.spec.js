import assert from "power-assert";
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

import {setExclusionSetting} from "./events";
import {createCleaningStore} from "~/nodes/cleaning/redux";
import {delay} from "~/nodes/testUtil";
import api_excluded_answer from '~/cleaning/tests/views/expected_responses/api_excluded_answer.json';


describe('setExclusionSetting', () => {
  it('excluded correctly', (done) => {
    const mockAxios = new MockAdapter(axios);
    mockAxios
      .onGet('/cleaning/api/excluded_answer/')
      .reply(200,
        api_excluded_answer.ok_pattern_open.response
      );

    const store = createCleaningStore();
    store.dispatch(setExclusionSetting(1, api_excluded_answer.ok_pattern_open.request));

    delay(0).then(() => {
      assert.deepEqual(store.getState().text_answer, {
        unique_answers_list:
          {
            "5": {
              "item_name": "",
              "column_header": "q1t1",
              "column_no": 5,
              "answers": [
                {
                  "answer_value": "ナスが好きだから",
                  "judgement": false
                }
              ]
            },
            "6": {
              "item_name": "",
              "column_header": "q1t2",
              "column_no": 6,
              "answers": [
                {
                  "answer_value": "aaaa",
                  "judgement": true
                },
                {
                  "answer_value": "test",
                  "judgement": true
                }
              ]
            }
          }
      });
      done();
    });
  });
});



