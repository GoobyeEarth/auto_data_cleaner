import {call, put, takeEvery} from 'redux-saga/effects'
import axios from 'axios';
import {systemSetsUniqueAnswersList} from "../reducers";

export const ACTION_TYPES = {
  USER_POSTS_EXCLUSION_SETTING: 'USER_POSTS_EXCLUSION_SETTING',
};



export const setExclusionSetting = (cleaning_id, standards) => {
  return {
    type: ACTION_TYPES.USER_POSTS_EXCLUSION_SETTING,
    cleaning_id: cleaning_id,
    standards: standards
  };
};

const requestExclusionSetting = (cleaning_id, standards) => {

  return axios.get('/cleaning/api/excluded_answer/', {
    params: {...standards, cleaning_id: cleaning_id},
  })
    .then(function (response) {
      return response;
    })
    .catch(function (error) {
      window.alert('error');
      return error;
    });
};

function* waitForPostExclusionSetting(action) {
  const response = yield call(requestExclusionSetting, action.cleaning_id, action.standards);
  yield put(systemSetsUniqueAnswersList(response.data.payload));
}

export function* freeAnswerExclusionSettingSaga() {
  yield takeEvery(ACTION_TYPES.USER_POSTS_EXCLUSION_SETTING, waitForPostExclusionSetting);
}

