import React from 'react';
import {connect} from 'react-redux'
import {Component} from 'react';

import {Card, CardActions, CardTitle, CardText} from 'material-ui/Card';

class SingleAnswerCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      customize_standards: true,
    };

  }

  render() {
    return (
      <Card style={{marginTop: 10}}>
        <CardTitle title="saの不正回答除外ロジック"/>
      </Card>
    );
  }
}

const mapStateToProps = (state) => ({
  cleaning_id: state.cleaning.cleaning_id,
});

const mapDispatchToProps = (dispatch) => {
  return {}
};

export default connect(mapStateToProps, mapDispatchToProps)(SingleAnswerCard);