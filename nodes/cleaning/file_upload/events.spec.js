import assert from "power-assert";
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

import {uploadFile} from "./events";
import {createCleaningStore} from "~/nodes/cleaning/redux";
import {delay} from "~/nodes/testUtil";

describe('cleaning files uploaded', () => {
  it('upload correctly', (done) => {
    const mockAxios = new MockAdapter(axios);
    mockAxios
      .onPost('/cleaning/api/upload/')
      .reply(200,
        {
          response_code: "0000",
          message: "file uploaded",
          payload: {cleaning_id: 1}
        }
      );

    const store = createCleaningStore();

    store.dispatch(uploadFile(
      {
        files_type: 'forSurvey',
        files:
          {
            raw_data: 'test',
            layout: 'test'
          }
      }
    ));

    delay(0).then(() => {
      assert.deepEqual(store.getState().cleaning,
        {status: 'UPLOADED', cleaning_id: 1}
      );

      done();
    });

  });


});