import {call, put, takeEvery} from 'redux-saga/effects'
import axios from 'axios';
import {systemUploadsFile} from "../cleaning/reducers";

export const ACTION_TYPES = {
  USER_UPLOADS_FILES: 'USER_UPLOADS_FILES',
};

export function uploadFile(fileState) {
  return {
    type: ACTION_TYPES.USER_UPLOADS_FILES,
    fileState: fileState
  };
}

function requestFileUpload(fileState) {
  let formParams = new FormData();
  formParams.append('files_type', fileState.files_type);
  formParams.append('raw_data', fileState.files.raw_data);
  formParams.append('layout', fileState.files.layout);
  return axios.post('/cleaning/api/upload/', formParams)
    .then(function (response) {
      return response;
    })
    .catch(function (error) {
      window.alert('error');
      return error;
    });
}

function* waitForFileUploadResponse(action) {
  const response = yield call(requestFileUpload, action.fileState);
  yield put(systemUploadsFile(response.data.payload.cleaning_id));
}

export function* fileUploadSaga() {
  yield takeEvery(ACTION_TYPES.USER_UPLOADS_FILES, waitForFileUploadResponse);
}
