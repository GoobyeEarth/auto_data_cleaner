import React from 'react';
import PropTypes from 'prop-types';
import {Component} from 'react';

import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';

export class FileTypeSelector extends Component {
  constructor() {
    super();
  }

  static propTypes = {
    changeFileTypeState: PropTypes.func,
  };

  render() {
    return (
      <div>
        <RadioButtonGroup
          name="files_type"
          defaultSelected="forSurvey"
          onChange={(event, value) => {
            this.props.changeFileTypeState(value)
          }}
        >
          <RadioButton
            value="forSurvey"
            label="forSurvey形式"
          />
        </RadioButtonGroup>
      </div>
    );
  }
}

export class UploadFilesSelector extends Component {
  constructor() {
    super();
  }

  static changeFileState = {
    changeFileTypeState: PropTypes.func,
  };

  render() {
    return (
      <div style={{marginLeft: 10}}>
        <ui>
          <li style={{marginTop: 10}}>
            <span style={{marginRight: 16}}>レイアウトファイル</span>
            <input type="file" name="layout"
                   onChange={(e) => this.props.changeFileState('layout', e)}/>
          </li>
          <li style={{marginTop: 10}}>
            <span style={{marginRight: 16}}>ローデータファイル</span>
            <input type="file" name="raw_data"
                   onChange={(e) => this.props.changeFileState('raw_data', e)}/>
          </li>
        </ui>
      </div>
    );
  }
}