import React from 'react';
import PropTypes from 'prop-types';
import {Component} from 'react';
import {Card, CardActions, CardTitle, CardText} from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import {FileTypeSelector, UploadFilesSelector} from './parts';


import {connect} from 'react-redux'
import {uploadFile} from "../events";
import {CLEANING_STATUSES} from "../../cleaning/reducers";

export class FileUploadCard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      file: {
        files_type: 'forSurvey',
        files: {}
      }
    };
  }

  static propTypes = {
    cleaningStatus: PropTypes.string,
    handleFileUploadButtonClick: PropTypes.func,
  };

  changeFileState(filename, e) {
    const target = e.target;
    const file = target.files.item(0);
    let fileState = this.state.file;
    fileState.files[filename] = file;
    this.setState({file: fileState});
  }

  changeFileTypeState(filesType) {
    let fileState = this.state.file;
    fileState.files_type = filesType;
    fileState.files = {};
    this.setState({file: fileState});
  }

  handleUploadFile(fileState) {
    this.props.handleFileUploadButtonClick(fileState);
  }

  render() {
    return (
      <Card style={{marginTop: 10}}>
        <CardTitle title="クリーニング対象ファイル"/>
        {(() => {
          if (this.props.cleaningStatus === CLEANING_STATUSES.UPLOADED) {
            return (
              <div>
                <CardText>
                  ファイルアップロードしました。
                </CardText>
                <CardActions>
                  <RaisedButton
                    label="やり直す"
                    onClick={() => {
                    }}
                  />
                </CardActions>
              </div>
            );
          } else {
            return (
              <div>
                <CardText>
                  <FileTypeSelector
                    changeFileTypeState={this.changeFileTypeState}
                  />
                  <UploadFilesSelector
                    changeFileState={this.changeFileState.bind(this)}
                  />

                </CardText>

                <CardActions>
                  <RaisedButton
                    label="ファイルアップロード"
                    primary={true}
                    onClick={() => {
                      this.handleUploadFile(this.state.file);
                    }}
                  />
                </CardActions>
              </div>
            );
          }
        })()}

      </Card>
    );
  }
}


const mapStateToProps = (state) => ({
  cleaningStatus: state.cleaning.status
});

function mapDispatchToProps(dispatch) {
  return {
    handleFileUploadButtonClick: (fileState) => {
      dispatch(uploadFile(fileState));
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(FileUploadCard);
