import {combineReducers} from 'redux';
import {fileUploadReducer} from './file_upload/reducers';
import {fileUploadSaga} from "./file_upload/events";
import {cleaningReducer} from "./cleaning/reducers";

import {fork} from 'redux-saga/effects'
import {createStore, applyMiddleware} from 'redux';
import createSagaMiddleware from 'redux-saga';
import {freeAnswerExclusionSettingSaga} from "./text_answer/exclusion_logic_card/events";
import {textAnswerReducer} from "./text_answer/reducers";
import {exclusionByHandSaga} from "./text_answer/exclusion_by_hand/events";
import {basicCheckSaga} from "./basic_check/events";
import {basicCheckReducer} from "./basic_check/reducers";


export function* rootSaga() {
  yield [
    fork(fileUploadSaga),
    fork(freeAnswerExclusionSettingSaga),
    fork(exclusionByHandSaga),
    fork(basicCheckSaga),
  ];
}

export const rootReducer = combineReducers({
  cleaning: cleaningReducer,
  file_upload: fileUploadReducer,
  text_answer: textAnswerReducer,
  basic_check: basicCheckReducer,
});


export const createCleaningStore = () => {
  const sagaMiddleware = createSagaMiddleware();
  const store = createStore(
    rootReducer,
    applyMiddleware(sagaMiddleware)
  );

  sagaMiddleware.run(rootSaga);
  return store;
};


