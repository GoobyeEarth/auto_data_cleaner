FROM centos

RUN yum -y update
RUN yum -y install yum-utils
RUN yum install -y which sudo git-core vim
RUN yum -y groupinstall development
RUN yum -y install https://centos7.iuscommunity.org/ius-release.rpm
RUN yum -y install python36u python36u-pip python36u-devel
RUN yum install -y zlib-devel bzip2-devel openssl-devel ncurses-devel sqlite-devel readline-devel tk-devel
RUN yum install -y httpd httpd-devel
RUN yum -y install epel-release nodejs npm



# タイミングを見計らってインフラの調整を行う
#http://christina04.hatenablog.com/entry/2016/05/04/134323