import io
import json
import urllib.parse

from django.core.handlers.wsgi import WSGIRequest
from django.db import transaction
from django.http import HttpResponse
from django.http import HttpResponseBadRequest
from django.shortcuts import render

from auto_data_cleaner.utils.api import ApiSpec
from auto_data_cleaner.utils.exceptions import BadRequestException
from cleaning.services.download_raw_data import DownloadRawDataFileService
from cleaning.services.excluded_answer import ExcludedAnswerService
from cleaning.services.upload_file import UploadFileService


def index(request):
    return render(request, 'cleaning/index.html')


@transaction.atomic
def api_upload(request: WSGIRequest):
    """/cleaning/で最初にファイルをアップロードするAPI.

    :param request:
    :return:

    """
    files_type: str = request.POST.get('files_type')
    uploaded_files = {
        'layout': request.FILES.get('layout', None),
        'raw_data': request.FILES.get('raw_data', None)
    }

    request_params = {
        'files_type': files_type,
        'uploaded_files': uploaded_files
    }

    try:
        cleaning_id = UploadFileService.save_uploaded_file(request_params)
        return HttpResponse(ApiSpec.create_response(
            ApiSpec.ResponseCode.SUCCESS,
            'file uploaded',
            {'cleaning_id': cleaning_id}
        ))
    except BadRequestException as e:
        return HttpResponseBadRequest(e)


def api_excluded_answer(request: WSGIRequest):
    """アップロードしたファイルからFAの重複排除、条件排除された解答値を取得するAPI.

    :param request:
    :return:

    """
    request_params = {
        'cleaning_id': request.GET.get('cleaning_id'),
        'too_less_words': request.GET.get('too_less_words'),
        'too_match_consonants': request.GET.get('too_match_consonants'),
        'redundant_word': request.GET.get('redundant_word'),
    }
    excluded_answer_service = ExcludedAnswerService()

    excluded_answers = excluded_answer_service.get_duplication_excluded_answers(
        request_params)

    return HttpResponse(ApiSpec.create_response(
        ApiSpec.ResponseCode.SUCCESS,
        'message',
        excluded_answers
    ))


def download_raw_data_file(request: WSGIRequest):
    """設定値を元にクリーニングしてダウンロードする。 時間かかることがわかったらボタンをいくつか導入して非同期かする。
    現在forSurveyにのみ対応されている。

    :param request:
    :return:

    """

    request_params = {
        'cleaning_id': request.POST.get('cleaning_id'),
        'excluded_answers_list': json.loads(request.POST.get('excluded_answers_list')),
        'leave_flags': request.POST.get('leave_flags'),
        'basic_check': json.loads(request.POST.get('basic_check')),
    }

    result = DownloadRawDataFileService().make_string_io(request_params)

    response = HttpResponse(result['io'].getvalue(),
                            content_type='text/plain; charset=' + result['encode'])
    response['Content-Disposition'] = 'attachment; filename="{fn}"'.format(
        fn=urllib.parse.quote(result['filename']))

    return response


def test(request):
    # cleaning_id = request.POST.get('cleaning_id')
    # excluded_answers_list = request.POST.get('excluded_answers_list')

    output = io.StringIO()
    output.write('First line.\n')
    response = HttpResponse(output.getvalue(), content_type='text/plain')
    response['Content-Disposition'] = 'attachment; filename="{fn}"'.format(
        fn=urllib.parse.quote('ファイル.txt'))
    return response
