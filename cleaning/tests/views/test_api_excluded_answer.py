import json

from typing import Dict

from django.core.files import File
from django.test import Client
from django.test import TestCase
from django.urls import reverse


class ApiExcludeAnswer(TestCase):
    def test_ok_pattern_open(self):
        """exclude from forSurvey files."""
        client = Client()
        expect = load_test_request_and_response()['ok_pattern_open']

        layout_file_path = expect['files']['layout']
        layout_file = File(open(layout_file_path, 'r'))
        raw_data_file_path = expect['files']['raw_data']
        raw_data_file = File(open(raw_data_file_path, 'r'))

        client.post(reverse('cleaning_api_upload'), {
            'files_type': 'forSurvey',
            'layout': layout_file,
            'raw_data': raw_data_file,
        })

        response = client.get(
            reverse(
                'cleaning_api_excluded_answer'), expect['request']
        )
        self.maxDiff = None
        self.assertEqual(json.loads(response.content), expect['response'])

    def test_ok_pattern_closed(self):
        """exclude from forSurvey files."""
        client = Client()
        expect = load_test_request_and_response()['ok_pattern_closed']

        layout_file_path = expect['files']['layout']
        layout_file = File(open(layout_file_path, 'r'))
        raw_data_file_path = expect['files']['raw_data']
        raw_data_file = File(open(raw_data_file_path, 'r'))

        client.post(reverse('cleaning_api_upload'), {
            'files_type': 'forSurvey',
            'layout': layout_file,
            'raw_data': raw_data_file,
        })

        client.get(
            reverse(
                'cleaning_api_excluded_answer'), expect['request']
        )

        # テストしたいがテストコードが重くなりそうなのでいったんなしで。


def load_test_request_and_response() -> Dict:
    f = open('cleaning/tests/views/expected_responses/api_excluded_answer.json', 'r')
    return json.load(f)
