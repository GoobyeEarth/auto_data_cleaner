from django.core.files import File
from django.test import Client
from django.test import TestCase
from django.urls import reverse

from cleaning.models import Cleaning
from cleaning.models import FileGroup
from cleaning.models import UploadedFile


class ApiUpload(TestCase):
    def test_ok_pattern(self):
        """uploaded forSurvey files."""

        client = Client()
        layout_file_path = './cleaning/tests/modules/files/format/test_files/simple_fs_excluded/070005568_Layout.txt'  # NOQA
        layout_file = File(open(layout_file_path, 'r'))

        raw_data_file_path = './cleaning/tests/modules/files/format/test_files/simple_fs_excluded/070005568_Rawdata.txt'  # NOQA
        raw_data_file = File(open(raw_data_file_path, 'r'))

        response = client.post(
            reverse('cleaning_api_upload'),
            {
                'files_type': 'forSurvey',
                'layout': layout_file,
                'raw_data': raw_data_file,
            })

        self.assertEqual(response.content,
                         b'{"response_code": "0000", "message": "file uploaded", '
                         b'"payload": {"cleaning_id": 1}}')
        cleanings_saved = Cleaning.objects.all()
        self.assertEqual(cleanings_saved.__len__(), 1)
        self.assertEqual(cleanings_saved[0].status, 'NEW')

        file_groups_saved = FileGroup.objects.filter(
            cleaning=cleanings_saved[0])
        self.assertEqual(file_groups_saved.__len__(), 1)

        saved = UploadedFile.objects.all()
        self.assertEqual(saved.__len__(), 2)

        layout_file_saved = UploadedFile.objects.filter(
            file_group=file_groups_saved[0], file_type='LAYOUT')
        self.assertEqual(layout_file_saved.__len__(), 1)

        raw_data_file_saved = UploadedFile.objects.filter(
            file_group=file_groups_saved[0], file_type='RAW_DATA')
        self.assertEqual(raw_data_file_saved.__len__(), 1)
