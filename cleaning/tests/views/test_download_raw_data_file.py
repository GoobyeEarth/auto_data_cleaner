import json

from django.core.files import File
from django.test import Client
from django.test import TestCase
from django.urls import reverse

import io


class DownloadRawDataFile(TestCase):
    def test_ok_pattern(self):
        client = Client()
        expects = load_test_request_and_response()

        layout_file_path = './cleaning/tests/modules/files/format/test_files/simple_fs_excluded/070005568_Layout.txt'  # NOQA
        layout_file = File(open(layout_file_path, 'r'))
        raw_data_file_path = './cleaning/tests/modules/files/format/test_files/simple_fs_excluded/070005568_Rawdata.txt'  # NOQA
        raw_data_file = File(open(raw_data_file_path, 'r'))

        client.post(reverse('cleaning_api_upload'), {
            'files_type': 'forSurvey',
            'layout': layout_file,
            'raw_data': raw_data_file,
        })

        response = client.post(reverse('cleaning_download_raw_data_file'), {
            'cleaning_id': expects['ok_pattern']['request']['cleaning_id'],
            'excluded_answers_list': json.dumps(
                expects['ok_pattern']['request']['excluded_answers_list']),
            'leave_flags': expects['ok_pattern']['request']['leave_flags'],
            'basic_check': json.dumps(expects['ok_pattern']['request']['basic_check']),
        })

        self.maxDiff = None

        self.assertEqual(response.get('Content-Disposition'),
                         'attachment; filename="070005568_Rawdata.txt"')

        string_io = io.BytesIO(response.content)
        self.assertEqual(string_io.readline().decode('utf-8'),
                         'NO\tSTART\tEND\tTIME\tq1t1\tq1t2\tSTA\tQUO\tUserAgent\tAPPID\tIPAddress\tFA_FLAG\r\n')  # NOQA
        self.assertEqual(string_io.readline().decode('utf-8'),
                         '100000001\t2017/11/05-15:51:17\t2017/11/05-15:51:30\t0:0:14\tナスが好きだから\taaaa\tCOMP\t1\tMozilla/5.0 (Macintosh Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36\t  \t106.181.117.144\t1\r\n')  # NOQA
        self.assertEqual(string_io.readline().decode('utf-8'),
                         '100000002\t2017/11/05-15:51:17\t2017/11/05-15:51:30\t0:0:13\tナスが好きだから\ttest\tCOMP\t1\tMozilla/5.0 (Macintosh Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36\t  \t106.181.117.144\t1\r\n')  # NOQA

    def test_ok_pattern_shift_js(self):
        client = Client()
        expects = load_test_request_and_response()

        layout_file_path = './cleaning/tests/modules/files/format/test_files/simple_fs_shift_js/046005989_Layout.txt'  # NOQA
        layout_file = File(
            open(layout_file_path, mode='r', encoding='shift_jis'))
        raw_data_file_path = './cleaning/tests/modules/files/format/test_files/simple_fs_shift_js/046005989_Rawdata.txt'  # NOQA
        raw_data_file = File(
            open(raw_data_file_path, mode='r', encoding='shift_jis'))

        client.post(reverse('cleaning_api_upload'), {
            'files_type': 'forSurvey',
            'layout': layout_file,
            'raw_data': raw_data_file,

        })

        response = client.post(reverse('cleaning_download_raw_data_file'), {
            'cleaning_id': expects['ok_pattern']['request']['cleaning_id'],
            'excluded_answers_list': json.dumps(
                expects['ok_pattern']['request']['excluded_answers_list']),
            'leave_flags': expects['ok_pattern']['request']['leave_flags'],
            'basic_check': json.dumps(expects['ok_pattern']['request']['basic_check']),
        })
        self.assertEqual(response.get('Content-Disposition'),
                         'attachment; filename="046005989_Rawdata.txt"')

        string_io = io.BytesIO(response.content)

        self.assertEqual(string_io.readline().decode('utf-8'),
                         'NO\tSTART\tEND\tTIME\tq1t1\tSTA\tQUO\tUserAgent\tAPPID\tIPAddress\tFA_FLAG\r\n')  # NOQA
        self.assertEqual(string_io.readline().decode('utf-8'),
                         '1000000\t2018/01/08-13:05:31\t2018/01/08-13:05:39\t0:0:8\tああああああ\tCOMP\t1\tMozilla/5.0 (Macintosh Intel Mac OS X 10_13_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36\t  \t106.161.214.1\t0\r\n')  # NOQA


def load_test_request_and_response():
    f = open(
        'cleaning/tests/views/expected_responses/download_raw_data_file.json', 'r')
    return json.load(f)
