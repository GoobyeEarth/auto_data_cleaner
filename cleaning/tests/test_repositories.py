from django.test import TestCase

from cleaning.models import Cleaning
from cleaning.repositories import CleaningRepository


class CleaningRepositoryTest(TestCase):
    def test_create(self):
        cleaning = CleaningRepository.create()
        self.assertEqual(cleaning.id, 1)
        self.assertEqual(cleaning.status, 'NEW')

        cleaning_saved = Cleaning.objects.all()[0]
        self.assertEqual(cleaning_saved.id, 1)
        self.assertEqual(cleaning_saved.status, 'NEW')

    def test_save(self):
        cleaning = CleaningRepository.create()
        cleaning.status = 'CLEANING_ENDED'
        self.assertEqual(cleaning.status, 'CLEANING_ENDED')
