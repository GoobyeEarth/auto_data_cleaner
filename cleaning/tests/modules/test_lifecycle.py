import mock

from django.core.files import File
from django.test import TestCase

from cleaning.models import Cleaning
from cleaning.models import FileGroup
from cleaning.models import FilesTypes
from cleaning.models import UploadedFile
from cleaning.modules.lifecycles import CleaningLifeCycle


class cleaningLifeCycleTest(TestCase):
    def test_upload_target_files(self):
        """
                user action: uploaded 2 files
                1. cleaning table insert 1 as 'NEW'
                2. file_group table insert 1
                3. uploaded_file table insert 2 (files)

                :return:
                """
        layout_file: File = mock.MagicMock(spec=File, name='LayoutMock')
        layout_file.name = 'layout.txt'
        layout_file.read = lambda: b'aaaaaa'

        raw_data_file: File = mock.MagicMock(spec=File, name='RawDataMock')
        raw_data_file.name = 'raw_data.txt'

        cleaning_id = CleaningLifeCycle.upload_target_files(FilesTypes.FOR_SURVEY, {
            'layout': layout_file,
            'raw_data': raw_data_file
        })

        self.assertEqual(cleaning_id, 1)
        cleanings_saved = Cleaning.objects.all()
        self.assertEqual(cleanings_saved.__len__(), 1)
        self.assertEqual(cleanings_saved[0].status, 'NEW')

        file_groups_saved = FileGroup.objects.filter(
            cleaning=cleanings_saved[0])
        self.assertEqual(file_groups_saved.__len__(), 1)

        saved = UploadedFile.objects.all()
        self.assertEqual(saved.__len__(), 2)

        layout_file_saved = UploadedFile.objects.filter(
            file_group=file_groups_saved[0], file_type='LAYOUT')
        self.assertEqual(layout_file_saved.__len__(), 1)

        raw_data_file_saved = UploadedFile.objects.filter(
            file_group=file_groups_saved[0], file_type='RAW_DATA')
        self.assertEqual(raw_data_file_saved.__len__(), 1)
