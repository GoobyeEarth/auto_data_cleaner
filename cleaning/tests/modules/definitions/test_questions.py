from ddt import data
from ddt import ddt
from ddt import unpack
from django.test import TestCase
from mock import Mock

from cleaning.modules.definitions.answer_column import AnswerColumn
from cleaning.modules.definitions.answer_column import MultiSelectedAnswerColumn
from cleaning.modules.definitions.answer_column import NumberAnswerColumn
from cleaning.modules.definitions.answer_column import SingleSelectedAnswerColumn
from cleaning.modules.definitions.answer_column import TextAnswerColumn
from cleaning.modules.definitions.exceptions import DefinitionException
from cleaning.modules.definitions.questions import MatrixMultiSelectedQuestion
from cleaning.modules.definitions.questions import MatrixQuestion
from cleaning.modules.definitions.questions import MatrixSingleSelectedQuestion
from cleaning.modules.definitions.questions import MultiSelectedQuestion
from cleaning.modules.definitions.questions import NumberAnsweredQuestion
from cleaning.modules.definitions.questions import Question
from cleaning.modules.definitions.questions import SingleSelectedQuestion
from cleaning.modules.definitions.questions import TextAnsweredQuestion


class QuestionTest(TestCase):
    def test_add_answer_ok(self):
        question = Question('これは設問文', 'Q1')
        question._is_answers_available = Mock()
        question._is_answers_available.return_value = True

        question.add_answers(TextAnswerColumn('Q1T1', 'q1t1', 5))

        self.assertEqual(question.title, 'これは設問文')
        self.assertEqual(question.question_no, 'Q1')
        self.assertEqual(question.question_no, 'Q1')
        self.assertEqual(question._answers.__len__(), 1)

    def test_add_answer_ng(self):
        question = Question('これは設問文', 'Q1')
        question._is_answers_available = Mock()
        question._is_answers_available.return_value = False

        self.assertRaises(DefinitionException,
                          lambda: question.add_answers(TextAnswerColumn('Q1T1', 'q1t1', 5)))


@ddt
class SingleSelectedQuestionTest(TestCase):
    @unpack
    @data(
        {'answers': SingleSelectedAnswerColumn, 'expected': True},
        {'answers': MultiSelectedAnswerColumn, 'expected': False},
        {'answers': TextAnswerColumn, 'expected': True},
        {'answers': NumberAnswerColumn, 'expected': True},

    )
    def test__is_answer_available(self, answers: AnswerColumn, expected):
        sa = SingleSelectedQuestion('これは設問文', 'Q1')
        result = sa._is_answers_available(answers)
        self.assertEqual(result, expected)


@ddt
class MultiSelectedQuestionTest(TestCase):
    @unpack
    @data(
        {'answers': SingleSelectedAnswerColumn, 'expected': False},
        {'answers': MultiSelectedAnswerColumn, 'expected': True},
        {'answers': TextAnswerColumn, 'expected': True},
        {'answers': NumberAnswerColumn, 'expected': True},

    )
    def test__is_answer_available(self, answers: AnswerColumn, expected):
        ma = MultiSelectedQuestion('これは設問文', 'Q1')
        result = ma._is_answers_available(answers)
        self.assertEqual(result, expected)


@ddt
class TextAnsweredQuestionTest(TestCase):
    @unpack
    @data(
        {'answers': SingleSelectedAnswerColumn, 'expected': False},
        {'answers': MultiSelectedAnswerColumn, 'expected': False},
        {'answers': TextAnswerColumn, 'expected': True},
        {'answers': NumberAnswerColumn, 'expected': False},

    )
    def test__is_answer_available(self, answers: AnswerColumn, expected):
        ma = TextAnsweredQuestion('これは設問文', 'Q1')
        result = ma._is_answers_available(answers)
        self.assertEqual(result, expected)


@ddt
class NumberAnsweredQuestionTest(TestCase):
    @unpack
    @data(
        {'answers': SingleSelectedAnswerColumn, 'expected': False},
        {'answers': MultiSelectedAnswerColumn, 'expected': False},
        {'answers': TextAnswerColumn, 'expected': False},
        {'answers': NumberAnswerColumn, 'expected': True},

    )
    def test__is_answer_available(self, answers: AnswerColumn, expected):
        ma = NumberAnsweredQuestion('これは設問文', 'Q1')
        result = ma._is_answers_available(answers)
        self.assertEqual(result, expected)


@ddt
class MatrixSingleAnsweredQuestionTest(TestCase):
    @unpack
    @data(
        {'answers': SingleSelectedAnswerColumn, 'expected': True},
        {'answers': MultiSelectedAnswerColumn, 'expected': False},
        {'answers': TextAnswerColumn, 'expected': True},
        {'answers': NumberAnswerColumn, 'expected': True},

    )
    def test__is_answer_available(self, answers: AnswerColumn, expected):
        ma = MatrixSingleSelectedQuestion('これは設問文', 'Q1')
        result = ma._is_answers_available(answers)
        self.assertEqual(result, expected)


@ddt
class MatrixMultiAnsweredQuestionTest(TestCase):
    @unpack
    @data(
        {'answers': SingleSelectedAnswerColumn, 'expected': False},
        {'answers': MultiSelectedAnswerColumn, 'expected': True},
        {'answers': TextAnswerColumn, 'expected': True},
        {'answers': NumberAnswerColumn, 'expected': True},

    )
    def test__is_answer_available(self, answers: AnswerColumn, expected):
        ma = MatrixMultiSelectedQuestion('これは設問文', 'Q1')
        result = ma._is_answers_available(answers)
        self.assertEqual(result, expected)


@ddt
class MatrixQuestionTest(TestCase):
    @unpack
    @data(
        {'answers': SingleSelectedAnswerColumn, 'expected': True},
        {'answers': MultiSelectedAnswerColumn, 'expected': True},
        {'answers': TextAnswerColumn, 'expected': True},
        {'answers': NumberAnswerColumn, 'expected': True},

    )
    def test__is_answer_available(self, answers: AnswerColumn, expected):
        ma = MatrixQuestion('これは設問文', 'Q1')
        result = ma._is_answers_available(answers)
        self.assertEqual(result, expected)
