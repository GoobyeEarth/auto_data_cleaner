from django.test import TestCase

from cleaning.modules.definitions.answer_column import MultiSelectedAnswerColumn  # NOQA
from cleaning.modules.definitions.answer_column import NumberAnswerColumn  # NOQA
from cleaning.modules.definitions.answer_column import SingleSelectedAnswerColumn  # NOQA
from cleaning.modules.definitions.answer_column import TextAnswerColumn  # NOQA


class SingleSelectedAnswerColumnTest(TestCase):
    def test_constructor(self):
        sa = SingleSelectedAnswerColumn(
            'Q1', 'q1', 5, {1: '選択肢1', 2: '選択肢2', 3: '選択肢3'})

        self.assertEqual(sa.item_name, 'Q1')
        self.assertEqual(sa.column_header, 'q1')
        self.assertEqual(sa.column_no, 5)
        self.assertEqual(sa.choice_labels, {1: '選択肢1', 2: '選択肢2', 3: '選択肢3'})

    def test_add_answer(self):
        sa = SingleSelectedAnswerColumn(
            'Q1', 'q1', 5, {1: '選択肢1', 2: '選択肢2', 3: '選択肢3'})
        sa.add_answer('1', 1)

        self.assertEqual(sa._answers, {'1': 1})


class MultiSelectedAnswerColumnTest(TestCase):
    def test_constructor_as_multi_column(self):
        ma = MultiSelectedAnswerColumn('Q1', {1: 'q1c1', 2: 'q1c2', 3: 'q1c3'},
                                       {1: 5, 2: 6, 3: 7, 4: 8, 5: 9},
                                       {1: '選択肢1', 2: '選択肢2', 3: '選択肢3'})

        self.assertEqual(ma.item_name, 'Q1')
        self.assertEqual(ma.column_header, {1: 'q1c1', 2: 'q1c2', 3: 'q1c3'})
        self.assertEqual(ma.column_no, {1: 5, 2: 6, 3: 7, 4: 8, 5: 9})
        self.assertEqual(ma.choice_labels, {1: '選択肢1', 2: '選択肢2', 3: '選択肢3'})

    def test_add_answer(self):
        ma = MultiSelectedAnswerColumn('Q1', {1: 'q1c1', 2: 'q1c2', 3: 'q1c3'}, 5,
                                       {1: '選択肢1', 2: '選択肢2', 3: '選択肢3'})
        ma.add_answer('1', [1, 2])

        self.assertEqual(ma._answers, {'1': [1, 2]})


class TextAnswerColumnTest(TestCase):
    def test_constructor(self):
        fa = TextAnswerColumn('Q1T1', 'q1t1', 5)
        self.assertEqual(fa.item_name, 'Q1T1')
        self.assertEqual(fa.column_header, 'q1t1')
        self.assertEqual(fa.column_no, 5)

    def test_add_answer(self):
        fa = TextAnswerColumn('Q1T1', 'q1t1', 5)
        fa.add_answer('1', 'フリーアンサー解答値')
        self.assertEqual(fa._answers, {'1': 'フリーアンサー解答値'})


class NumberAnswerColumnTest(TestCase):
    def test_constructor(self):
        nf = NumberAnswerColumn('Q1T1', 'q1t1', 5)
        self.assertEqual(nf.item_name, 'Q1T1')
        self.assertEqual(nf.column_header, 'q1t1')
        self.assertEqual(nf.column_no, 5)

    def test_add_answer(self):
        nf = NumberAnswerColumn('Q1T1', 'q1t1', 5)
        nf.add_answer('1', 1)
        self.assertEqual(nf._answers, {'1': 1})
