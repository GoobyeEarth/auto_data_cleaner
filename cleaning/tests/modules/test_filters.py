from ddt import data
from ddt import ddt
from ddt import unpack
from django.test import TestCase

from cleaning.modules.filters import KanjiToRomajiGenerator
from cleaning.modules.filters import TextAnswerFilter


@ddt
class TextAnswerFilterTest(TestCase):
    @unpack
    @data(
        {'answer_value': 'aa', 'expected': False},
        {'answer_value': 'a', 'expected': True},
        {'answer_value': 'ああ', 'expected': False},
        {'answer_value': 'あ', 'expected': True},
        {'answer_value': '', 'expected': False},
    )
    def test_filter_too_less_words(self, answer_value, expected):
        text_answer_filter = TextAnswerFilter()
        actual = text_answer_filter.filter_too_less_words(answer_value)
        self.assertEqual(actual, expected)

    @unpack
    @data(
        {'answer_value': 'ああ', 'expected': False},
        {'answer_value': '漢字', 'expected': False},
        {'answer_value': 'qwertyuiopasdfghjklzxcvbnm', 'expected': True},
        {'answer_value': '', 'expected': False},
    )
    def test_filter_too_match_consonants(self, answer_value, expected):
        text_answer_filter = TextAnswerFilter()
        actual = text_answer_filter.filter_too_match_consonants(answer_value)
        self.assertEqual(actual, expected)

    @unpack
    @data(
        {'answer_value': 'ああああ', 'expected': True},
        {'answer_value': 'いあああ', 'expected': True},
        {'answer_value': 'あああい', 'expected': True},
        {'answer_value': 'いあああい', 'expected': True},
        {'answer_value': 'ああ', 'expected': False},
        {'answer_value': 'いああい', 'expected': False},
        {'answer_value': 'いああい', 'expected': False},
    )
    def test_filter_redundant_word(self, answer_value, expected):
        text_answer_filter = TextAnswerFilter()
        actual = text_answer_filter.filter_redundant_word(answer_value)
        self.assertEqual(actual, expected)

    @unpack
    @data(
        {'text': 'aa', 'expected': 0},
        {'text': 'kanji', 'expected': 0.6},
        {'text': 'qwertyuiopasdfghjklzxcvbnm', 'expected': 0.8076923076923077},
    )
    def test__calculate_consonants_rate(self, text, expected):
        text_answer_filter = TextAnswerFilter()
        actual = text_answer_filter._calculate_consonants_rate(text)
        self.assertEqual(actual, expected)

    @unpack
    @data(
        {'indexes': {}, 'expected': False},
        {
            'indexes': {
                'redundant_word': False,
                'too_less_words': False,
                'too_match_consonants': False
            },
            'expected': False
        },
        {
            'indexes': {
                'redundant_word': True,
                'too_less_words': True,
                'too_match_consonants': True
            },
            'expected': True
        },
        {
            'indexes': {
                'redundant_word': True,
                'too_less_words': False,
                'too_match_consonants': False
            },
            'expected': True
        },

    )
    def test_judge_by_indexes(self, indexes, expected):
        text_answer_filter = TextAnswerFilter()
        actual = text_answer_filter.judge_by_indexes(indexes)
        self.assertEqual(actual, expected)


@ddt
class KanjiToRomajiGeneratorTest(TestCase):
    @unpack
    @data(
        {'text': 'ああ', 'expected': 'aa'},
        {'text': '漢字', 'expected': 'kanji'},
        {'text': 'ご飯を食べる', 'expected': 'gomeshiwotaberu'},
        {'text': 'テスト', 'expected': 'tesuto'},
    )
    def test_generate(self, text, expected):
        generator = KanjiToRomajiGenerator()
        actual = generator.generate(text)
        self.assertEqual(actual, expected)
