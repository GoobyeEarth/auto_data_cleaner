070005569_Layout	070005569_GT	調査名	●●●に関するアンケート	調査ID	070005569	調査方法	インターネットリサーチ	商品種別	オープン	実施期間		有効サンプル数	1	
質問番号	質問タイプ	アイテム名	ラベル	回答タイプ	カテゴリ数	カラム	選択肢番号	質問文／選択肢	設問タイトル／選択肢グループキャプション
NO	D	NO	NO	MID		1		NO	
START	D	START	START	D		2		開始日時	
END	D	END	END	D		3		終了日時	
TIME	D	TIME	TIME	D		4		回答時間	
Q1	F	Q1						ここに設問文を入力します。	
			q1t1	FA		5	1	選択肢A	
			q1t2	FA		6	2	選択肢B	
STA	D	STA	STA	D		7		STA	
QUO	S	QUO	QUO	SA	1	8		QUO	
							1	全員対象	
UserAgent	D	UserAgent	UserAgent	D		9		UserAgent	
APPID	D	APPID	APPID	D		10		APPID	
IPAddress	D	IPAddress	IPAddress	D		11		IPAddress	
