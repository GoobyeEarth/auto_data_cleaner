from django.test import TestCase

from cleaning.modules.definitions.questions import MatrixSingleSelectedQuestion  # NOQA
from cleaning.modules.files.format.for_survey.question_converters.matrix_single_selected import \
    MatrixSingleSelectedQuestionConverter


class MatrixSingleSelectedQuestionConverterTest(TestCase):
    def test_convert_array_to_entity(self):
        converter = MatrixSingleSelectedQuestionConverter()
        target = \
            [['Q1', 'MTS', '', '', '', '', '', '', 'ここに設問文を入力します。', ''],
             ['', '', 'Q1S1', 'q1s1', 'SA', '3', '5', '', '選択肢A', ''],
             ['', '', '', '', '', '', '', '1', '選択肢A', ''],
             ['', '', '', '', '', '', '', '2', '選択肢B', ''],
             ['', '', '', '', '', '', '', '3', 'その他', ''],
             ['', '', 'Q1S1T3', 'q1s1t3', 'FA', '', '6', '3', 'その他', ''],
             ['', '', 'Q1S2', 'q1s2', 'SA', '3', '7', '', '選択肢B', ''],
             ['', '', '', '', '', '', '', '1', '選択肢A', ''],
             ['', '', '', '', '', '', '', '2', '選択肢B', ''],
             ['', '', '', '', '', '', '', '3', 'その他', ''],
             ['', '', 'Q1S2T3', 'q1s2t3', 'FA', '', '8', '3', 'その他', ''],
             ['', '', 'Q1S3', 'q1s3', 'SA', '3', '9', '', 'その他', ''],
             ['', '', '', '', '', '', '', '1', '選択肢A', ''],
             ['', '', '', '', '', '', '', '2', '選択肢B', ''],
             ['', '', '', '', '', '', '', '3', 'その他', ''],
             ['', '', 'Q1S3T3', 'q1s3t3', 'FA', '', '10', '3', 'その他', ''],
             ['', '', 'Q1S3ST', 'q1s3st', 'FA', '', '11', '', 'その他', '']]
        question: MatrixSingleSelectedQuestion = converter.convert_array_to_entity(
            target)
        self.assertEqual(question._answers.__len__(), 7)
        self.assertEqual(question._answers[0].item_name, 'Q1S1')
        self.assertEqual(
            question._answers[0].ANSWERS_TYPE, 'SINGLE_SELECTED_ANSWERS')
        self.assertEqual(question._answers[0].column_header, 'q1s1')

        self.assertEqual(question._answers[1].item_name, 'Q1S1T3')
        self.assertEqual(question._answers[1].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(question._answers[1].column_header, 'q1s1t3')

        self.assertEqual(question._answers[2].item_name, 'Q1S2')
        self.assertEqual(
            question._answers[2].ANSWERS_TYPE, 'SINGLE_SELECTED_ANSWERS')
        self.assertEqual(question._answers[2].column_header, 'q1s2')

        self.assertEqual(question._answers[3].item_name, 'Q1S2T3')
        self.assertEqual(question._answers[3].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(question._answers[3].column_header, 'q1s2t3')

        self.assertEqual(question._answers[4].item_name, 'Q1S3')
        self.assertEqual(
            question._answers[4].ANSWERS_TYPE, 'SINGLE_SELECTED_ANSWERS')
        self.assertEqual(question._answers[4].column_header, 'q1s3')

        self.assertEqual(question._answers[5].item_name, 'Q1S3T3')
        self.assertEqual(question._answers[5].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(question._answers[5].column_header, 'q1s3t3')

        self.assertEqual(question._answers[6].item_name, 'Q1S3ST')
        self.assertEqual(question._answers[6].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(question._answers[6].column_header, 'q1s3st')
