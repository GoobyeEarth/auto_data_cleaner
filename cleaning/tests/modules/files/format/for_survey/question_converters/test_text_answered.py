from typing import List  # NOQA

from django.test import TestCase

from cleaning.modules.definitions.questions import TextAnsweredQuestion  # NOQA
from cleaning.modules.files.format.for_survey.question_converters.text_answered import \
    TextAnsweredQuestionConverter


class TextAnsweredQuestionConverterTest(TestCase):
    def test_convert_array_to_entity(self):
        converter = TextAnsweredQuestionConverter()
        fs_array: List[List[str]] = \
            [['Q1', 'FS', 'Q1', '', '', '', '', '', 'ここに設問文を入力します。', ''],
             ['', '', '', 'q1t1', 'FA', '', '5', '1', '選択肢A', ''],
             ['', '', '', 'q1t2', 'FA', '', '6', '2', '選択肢B', ''],
             ['', '', '', 'q1t3', 'FA', '', '7', '3', '選択肢C', ''],
             ['', '', '', 'q1t4', 'FA', '', '8', '4', '選択肢D', ''],
             ['', '', '', 'q1t5', 'FA', '', '9', '5', '選択肢E', '']]

        question: TextAnsweredQuestion = converter.convert_array_to_entity(
            fs_array)

        self.assertEqual(question.title, 'ここに設問文を入力します。')
        self.assertEqual(question.question_no, 'Q1')
        self.assertEqual(question._answers.__len__(), 5)

        self.assertEqual(question._answers[0].item_name, '')
        self.assertEqual(question._answers[0].column_header, 'q1t1')
        self.assertEqual(question._answers[1].item_name, '')
        self.assertEqual(question._answers[1].column_header, 'q1t2')
        self.assertEqual(question._answers[2].item_name, '')
        self.assertEqual(question._answers[2].column_header, 'q1t3')
        self.assertEqual(question._answers[3].item_name, '')
        self.assertEqual(question._answers[3].column_header, 'q1t4')
        self.assertEqual(question._answers[4].item_name, '')
        self.assertEqual(question._answers[4].column_header, 'q1t5')
