from typing import List  # NOQA

from django.test import TestCase

from cleaning.modules.files.format.for_survey.question_converters.single_selected import \
    SingleSelectedQuestionConverter
from cleaning.modules.definitions.questions import SingleSelectedQuestion  # NOQA


class SingleSelectedQuestionConverterTest(TestCase):
    def test_convert_array_to_entity(self):
        converter = SingleSelectedQuestionConverter()
        sa_array: List[List[str]] = \
            [['Q1', 'S', 'Q1', 'q1', 'SA', '3', '5', '', 'ここに設問文を入力します。', ''],
             ['', '', '', '', '', '', '', '1', '選択肢A', ''],
             ['', '', '', '', '', '', '', '2', '選択肢B', ''],
             ['', '', '', '', '', '', '', '3', 'その他', ''],
             ['', '', 'Q1T3', 'q1t3', 'FA', '', '6', '3', 'その他', '']]

        question: SingleSelectedQuestion = converter.convert_array_to_entity(
            sa_array)

        self.assertEqual(question.title, 'ここに設問文を入力します。')
        self.assertEqual(question.question_no, 'Q1')
        self.assertEqual(question._answers.__len__(), 2)

        self.assertEqual(question._answers[0].item_name, 'Q1')
        self.assertEqual(question._answers[0].column_header, 'q1')
        self.assertEqual(question._answers[0].choice_labels, {
                         1: '選択肢A', 2: '選択肢B', 3: 'その他'})

        self.assertEqual(question._answers[1].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(question._answers[1].item_name, 'Q1T3')
        self.assertEqual(question._answers[1].column_header, 'q1t3')
