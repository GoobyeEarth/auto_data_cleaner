from typing import List  # NOQA

from django.test import TestCase

from cleaning.modules.definitions.questions import NumberAnsweredQuestion  # NOQA
from cleaning.modules.files.format.for_survey.question_converters.number_answered import \
    NumberAnsweredQuestionConverter


class NumberAnsweredQuestionConverterTest(TestCase):
    def test_convert_array_to_entity(self):
        converter = NumberAnsweredQuestionConverter()
        fs_array: List[List[str]] = \
            [['Q1', 'NUM', 'Q1', '', '', '', '', '', 'ここに設問文を入力します。', ''],
             ['', '', '', 'q1t1', 'NUM', '', '5', '1', '', '']]

        question: NumberAnsweredQuestion = converter.convert_array_to_entity(
            fs_array)

        self.assertEqual(question.title, 'ここに設問文を入力します。')
        self.assertEqual(question.question_no, 'Q1')
        self.assertEqual(question._answers.__len__(), 1)

        self.assertEqual(question._answers[0].item_name, '')
        self.assertEqual(question._answers[0].ANSWERS_TYPE, 'NUMBER_ANSWERS')
        self.assertEqual(question._answers[0].column_header, 'q1t1')
