from typing import List  # NOQA

from django.test import TestCase

from cleaning.modules.definitions.questions import MultiSelectedQuestion  # NOQA
from cleaning.modules.files.format.for_survey.question_converters.multi_selected import \
    MultiSelectedQuestionConverter


class MultiSelectedQuestionConverterTest(TestCase):
    def test_convert_array_to_entity(self):
        converter = MultiSelectedQuestionConverter()
        ma_array: List[List[str]] = \
            [['Q1', 'M', 'Q1', '', 'MA', '5', '', '', 'ここに設問文を入力します。', ''],
             ['', '', '', 'q1c1', '', '', '5', '1', '選択肢A', ''],
             ['', '', '', 'q1c2', '', '', '6', '2', '選択肢B', ''],
             ['', '', '', 'q1c3', '', '', '7', '3', '選択肢C', ''],
             ['', '', '', 'q1c4', '', '', '8', '4', '選択肢D', ''],
             ['', '', '', 'q1c5', '', '', '9', '5', 'その他', ''],
             ['', '', 'Q1T5', 'q1t5', 'FA', '', '10', '5', 'その他', '']]

        question: MultiSelectedQuestion = converter.convert_array_to_entity(
            ma_array)

        self.assertEqual(question.title, 'ここに設問文を入力します。')
        self.assertEqual(question.question_no, 'Q1')
        self.assertEqual(question._answers.__len__(), 2)

        self.assertEqual(question._answers[0].item_name, 'Q1')
        self.assertEqual(question._answers[0].column_header,
                         {1: 'q1c1', 2: 'q1c2', 3: 'q1c3', 4: 'q1c4', 5: 'q1c5'})
        self.assertEqual(question._answers[0].column_no,
                         {1: 5, 2: 6, 3: 7, 4: 8, 5: 9})
        self.assertEqual(question._answers[0].choice_labels,
                         {1: '選択肢A', 2: '選択肢B', 3: '選択肢C', 4: '選択肢D', 5: 'その他'})

        self.assertEqual(question._answers[1].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(question._answers[1].item_name, 'Q1T5')
        self.assertEqual(question._answers[1].column_header, 'q1t5')

    def test_converter_array_to_entity2(self):
        converter = MultiSelectedQuestionConverter()
        ma_array = \
            [['SQ4', 'M', 'SQ4', '', 'MA', '5', '', '',
              '次のSNS（ソーシャルネットワークサービス）について、あなたが現在、利用しているものをお答え下さい。', ''],
             ['', '', '', 'SQ4c1', '', '', '9', '1', 'Twitter（ツイッター）', ''],
             ['', '', '', 'SQ4c2', '', '', '10', '2', 'Facebook（フェイスブック）', ''],
             ['', '', '', 'SQ4c3', '', '', '11', '3', 'LINE（ライン）', ''],
             ['', '', '', 'SQ4c4', '', '', '12', '4', 'その他SNS（ソーシャルネットワーク）', ''],
             ['', '', '', 'SQ4c5', '', '', '13', '5', 'いずれも全く使っていない', '']]

        question: MultiSelectedQuestion = converter.convert_array_to_entity(
            ma_array)

        self.assertEqual(
            question.title,
            '次のSNS（ソーシャルネットワークサービス）について、あなたが現在、利用しているものをお答え下さい。')
        self.assertEqual(question.question_no, 'SQ4')
        self.assertEqual(question._answers.__len__(), 1)

        self.assertEqual(question._answers[0].item_name, 'SQ4')
        self.assertEqual(question._answers[0].column_header,
                         {1: 'SQ4c1', 2: 'SQ4c2', 3: 'SQ4c3', 4: 'SQ4c4', 5: 'SQ4c5'})
        self.assertEqual(question._answers[0].column_no,
                         {1: 9, 2: 10, 3: 11, 4: 12, 5: 13})
        self.assertEqual(question._answers[0].choice_labels,
                         {1: 'Twitter（ツイッター）', 2: 'Facebook（フェイスブック）',
                          3: 'LINE（ライン）',
                          4: 'その他SNS（ソーシャルネットワーク）', 5: 'いずれも全く使っていない'})
