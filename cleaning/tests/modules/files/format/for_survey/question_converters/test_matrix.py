from django.test import TestCase

from cleaning.modules.definitions.questions import MatrixQuestion  # NOQA
from cleaning.modules.files.format.for_survey.question_converters.matrix import \
    MatrixQuestionConverter


class MatrixMultiSelectedQuestionConverterTest(TestCase):
    def test_convert_array_to_entity(self):
        converter = MatrixQuestionConverter()
        target = \
            [['Q1', 'MTX', '', '', '', '', '', '', 'ここに設問文を入力します。', ''],
             ['', '', 'Q1S1', 'q1s1', 'SA', '5', '5', '', '選択肢A', ''],
             ['', '', '', '', '', '', '', '1', '選択肢A', ''],
             ['', '', '', '', '', '', '', '2', '選択肢B', ''],
             ['', '', '', '', '', '', '', '3', '選択肢C', ''],
             ['', '', '', '', '', '', '', '4', '選択肢D', ''],
             ['', '', '', '', '', '', '', '5', 'その他', ''],
             ['', '', 'Q1S2', '', 'MA', '5', '', '', '選択肢B', ''],
             ['', '', '', 'q1s2c1', '', '', '6', '1', '選択肢A', ''],
             ['', '', '', 'q1s2c2', '', '', '7', '2', '選択肢B', ''],
             ['', '', '', 'q1s2c3', '', '', '8', '3', '選択肢C', ''],
             ['', '', '', 'q1s2c4', '', '', '9', '4', '選択肢D', ''],
             ['', '', '', 'q1s2c5', '', '', '10', '5', 'その他', ''],
             ['', '', 'Q1S3', '', '', '', '', '', '選択肢C', ''],
             ['', '', '', 'q1s3t1', 'FA', '', '11', '1', '選択肢A', ''],
             ['', '', '', 'q1s3t2', 'FA', '', '12', '2', '選択肢B', ''],
             ['', '', '', 'q1s3t3', 'FA', '', '13', '3', '選択肢C', ''],
             ['', '', '', 'q1s3t4', 'FA', '', '14', '4', '選択肢D', ''],
             ['', '', '', 'q1s3t5', 'FA', '', '15', '5', 'その他', ''],
             ['', '', 'Q1S4', '', '', '', '', '', '選択肢D', ''],
             ['', '', '', 'q1s4t1', 'FA', '', '16', '1', '選択肢A', ''],
             ['', '', '', 'q1s4t2', 'FA', '', '17', '2', '選択肢B', ''],
             ['', '', '', 'q1s4t3', 'FA', '', '18', '3', '選択肢C', ''],
             ['', '', '', 'q1s4t4', 'FA', '', '19', '4', '選択肢D', ''],
             ['', '', '', 'q1s4t5', 'FA', '', '20', '5', 'その他', '']]

        question: MatrixQuestion = converter.convert_array_to_entity(
            target)

        self.assertEqual(question._answers.__len__(), 12)
        self.assertEqual(question.QUESTION_TYPE, 'MATRIX_QUESTION')
        self.assertEqual(question.title, 'ここに設問文を入力します。')
        self.assertEqual(question._answers[0].item_name, 'Q1S1')
        self.assertEqual(
            question._answers[0].ANSWERS_TYPE, 'SINGLE_SELECTED_ANSWERS')
        self.assertEqual(question._answers[0].column_header, 'q1s1')

        self.assertEqual(question._answers[1].item_name, 'Q1S2')
        self.assertEqual(
            question._answers[1].ANSWERS_TYPE, 'MULTI_SELECTED_ANSWERS')
        self.assertEqual(question._answers[1].column_header, {
            1: 'q1s2c1', 2: 'q1s2c2', 3: 'q1s2c3', 4: 'q1s2c4', 5: 'q1s2c5'})

        self.assertEqual(question._answers[2].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(question._answers[2].item_name, 'Q1S3')
        self.assertEqual(question._answers[2].column_header, 'q1s3t1')

        self.assertEqual(question._answers[3].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(question._answers[3].item_name, 'Q1S3')
        self.assertEqual(question._answers[3].column_header, 'q1s3t2')

        self.assertEqual(question._answers[4].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(question._answers[4].item_name, 'Q1S3')
        self.assertEqual(question._answers[4].column_header, 'q1s3t3')

        self.assertEqual(question._answers[5].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(question._answers[5].item_name, 'Q1S3')
        self.assertEqual(question._answers[5].column_header, 'q1s3t4')

        self.assertEqual(question._answers[6].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(question._answers[6].item_name, 'Q1S3')
        self.assertEqual(question._answers[6].column_header, 'q1s3t5')

        self.assertEqual(question._answers[7].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(question._answers[7].item_name, 'Q1S4')
        self.assertEqual(question._answers[7].column_header, 'q1s4t1')

        self.assertEqual(question._answers[8].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(question._answers[8].item_name, 'Q1S4')
        self.assertEqual(question._answers[8].column_header, 'q1s4t2')

        self.assertEqual(question._answers[9].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(question._answers[9].item_name, 'Q1S4')
        self.assertEqual(question._answers[9].column_header, 'q1s4t3')

        self.assertEqual(question._answers[10].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(question._answers[10].item_name, 'Q1S4')
        self.assertEqual(question._answers[10].column_header, 'q1s4t4')

        self.assertEqual(question._answers[11].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(question._answers[11].item_name, 'Q1S4')
        self.assertEqual(question._answers[11].column_header, 'q1s4t5')
