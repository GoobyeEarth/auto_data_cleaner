from typing import List  # NOQA

from django.test import TestCase

from cleaning.modules.files.format.for_survey.question_converters.index import QuestionConverter
from cleaning.modules.definitions.questions import MultiSelectedQuestion  # NOQA
from cleaning.modules.definitions.questions import NumberAnsweredQuestion  # NOQA
from cleaning.modules.definitions.questions import SingleSelectedQuestion  # NOQA
from cleaning.modules.definitions.questions import TextAnsweredQuestion  # NOQA


class QuestionConverterTest(TestCase):
    def test_convert_array_to_entity_sa(self):
        converter = QuestionConverter()
        sa_array: List[List[str]] = \
            [['Q1', 'S', 'Q1', 'q1', 'SA', '3', '5', '', 'ここに設問文を入力します。', ''],
             ['', '', '', '', '', '', '', '1', '選択肢A', ''],
             ['', '', '', '', '', '', '', '2', '選択肢B', ''],
             ['', '', '', '', '', '', '', '3', 'その他', ''],
             ['', '', 'Q1T3', 'q1t3', 'FA', '', '6', '3', 'その他', '']]

        question: SingleSelectedQuestion = converter.convert_array_to_entity(
            sa_array)

        self.assertEqual(question.title, 'ここに設問文を入力します。')
        self.assertEqual(question.question_no, 'Q1')
        self.assertEqual(question._answers.__len__(), 2)
        self.assertEqual(
            question._answers[0].ANSWERS_TYPE, 'SINGLE_SELECTED_ANSWERS')
        self.assertEqual(question._answers[1].ANSWERS_TYPE, 'TEXT_ANSWERS')

    def test_convert_array_to_entity_ma(self):
        converter = QuestionConverter()
        ma_array: List[List[str]] = \
            [['Q1', 'M', 'Q1', '', 'MA', '5', '', '', 'ここに設問文を入力します。', ''],
             ['', '', '', 'q1c1', '', '', '5', '1', '選択肢A', ''],
             ['', '', '', 'q1c2', '', '', '6', '2', '選択肢B', ''],
             ['', '', '', 'q1c3', '', '', '7', '3', '選択肢C', ''],
             ['', '', '', 'q1c4', '', '', '8', '4', '選択肢D', ''],
             ['', '', '', 'q1c5', '', '', '9', '5', 'その他', ''],
             ['', '', 'Q1T5', 'q1t5', 'FA', '', '10', '5', 'その他', '']]

        question: MultiSelectedQuestion = converter.convert_array_to_entity(
            ma_array)

        self.assertEqual(question.title, 'ここに設問文を入力します。')
        self.assertEqual(question.question_no, 'Q1')
        self.assertEqual(question._answers.__len__(), 2)

    def test_convert_array_to_entity_fs(self):
        converter = QuestionConverter()
        fs_array: List[List[str]] = \
            [['Q1', 'FS', 'Q1', '', '', '', '', '', 'ここに設問文を入力します。', ''],
             ['', '', '', 'q1t1', 'FA', '', '5', '1', '選択肢A', ''],
             ['', '', '', 'q1t2', 'FA', '', '6', '2', '選択肢B', ''],
             ['', '', '', 'q1t3', 'FA', '', '7', '3', '選択肢C', ''],
             ['', '', '', 'q1t4', 'FA', '', '8', '4', '選択肢D', ''],
             ['', '', '', 'q1t5', 'FA', '', '9', '5', '選択肢E', '']]

        question: TextAnsweredQuestion = converter.convert_array_to_entity(
            fs_array)

        self.assertEqual(question.title, 'ここに設問文を入力します。')
        self.assertEqual(question.question_no, 'Q1')
        self.assertEqual(question._answers.__len__(), 5)

    def test_convert_array_to_entity_nf(self):
        converter = QuestionConverter()
        fs_array: List[List[str]] = \
            [['Q1', 'NUM', 'Q1', '', '', '', '', '', 'ここに設問文を入力します。', ''],
             ['', '', '', 'q1t1', 'NUM', '', '5', '1', '', '']]

        question: NumberAnsweredQuestion = converter.convert_array_to_entity(
            fs_array)

        self.assertEqual(question.title, 'ここに設問文を入力します。')
        self.assertEqual(question.question_no, 'Q1')
        self.assertEqual(question._answers.__len__(), 1)
