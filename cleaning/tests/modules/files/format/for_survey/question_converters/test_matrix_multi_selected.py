from django.test import TestCase

from cleaning.modules.definitions.questions import MatrixMultiSelectedQuestion  # NOQA
from cleaning.modules.files.format.for_survey.question_converters.matrix_multi_selected import \
    MatrixMultiSelectedQuestionConverter


class MatrixMultiSelectedQuestionConverterTest(TestCase):
    def test_convert_array_to_entity(self):
        converter = MatrixMultiSelectedQuestionConverter()
        target = \
            [['Q1', 'MTM', '', '', '', '', '', '', 'ここに設問文を入力します。', ''],
             ['', '', 'Q1S1', '', 'MA', '3', '', '', '選択肢A', ''],
             ['', '', '', 'q1s1c1', '', '', '5', '1', '選択肢A', ''],
             ['', '', '', 'q1s1c2', '', '', '6', '2', '選択肢B', ''],
             ['', '', '', 'q1s1c3', '', '', '7', '3', 'その他', ''],
             ['', '', 'Q1S1T3', 'q1s1t3', 'FA', '', '8', '3', 'その他', ''],
             ['', '', 'Q1S2', '', 'MA', '3', '', '', '選択肢B', ''],
             ['', '', '', 'q1s2c1', '', '', '9', '1', '選択肢A', ''],
             ['', '', '', 'q1s2c2', '', '', '10', '2', '選択肢B', ''],
             ['', '', '', 'q1s2c3', '', '', '11', '3', 'その他', ''],
             ['', '', 'Q1S2T3', 'q1s2t3', 'FA', '', '12', '3', 'その他', ''],
             ['', '', 'Q1S3', '', 'MA', '3', '', '', 'その他', ''],
             ['', '', '', 'q1s3c1', '', '', '13', '1', '選択肢A', ''],
             ['', '', '', 'q1s3c2', '', '', '14', '2', '選択肢B', ''],
             ['', '', '', 'q1s3c3', '', '', '15', '3', 'その他', ''],
             ['', '', 'Q1S3T3', 'q1s3t3', 'FA', '', '16', '3', 'その他', ''],
             ['', '', 'Q1S3ST', 'q1s3st', 'FA', '', '17', '', 'その他', '']]

        question: MatrixMultiSelectedQuestion = converter.convert_array_to_entity(
            target)

        self.assertEqual(question._answers.__len__(), 7)
        self.assertEqual(question._answers[0].item_name, 'Q1S1')
        self.assertEqual(
            question._answers[0].ANSWERS_TYPE, 'MULTI_SELECTED_ANSWERS')
        self.assertEqual(question._answers[0].column_header, {
                         1: 'q1s1c1', 2: 'q1s1c2', 3: 'q1s1c3'})

        self.assertEqual(question._answers[1].item_name, 'Q1S1T3')
        self.assertEqual(question._answers[1].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(question._answers[1].column_header, 'q1s1t3')

        self.assertEqual(question._answers[2].item_name, 'Q1S2')
        self.assertEqual(
            question._answers[2].ANSWERS_TYPE, 'MULTI_SELECTED_ANSWERS')
        self.assertEqual(question._answers[2].column_header, {
                         1: 'q1s2c1', 2: 'q1s2c2', 3: 'q1s2c3'})

        self.assertEqual(question._answers[3].item_name, 'Q1S2T3')
        self.assertEqual(question._answers[3].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(question._answers[3].column_header, 'q1s2t3')

        self.assertEqual(question._answers[4].item_name, 'Q1S3')
        self.assertEqual(
            question._answers[4].ANSWERS_TYPE, 'MULTI_SELECTED_ANSWERS')
        self.assertEqual(question._answers[4].column_header, {
                         1: 'q1s3c1', 2: 'q1s3c2', 3: 'q1s3c3'})

        self.assertEqual(question._answers[5].item_name, 'Q1S3T3')
        self.assertEqual(question._answers[5].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(question._answers[5].column_header, 'q1s3t3')

        self.assertEqual(question._answers[6].item_name, 'Q1S3ST')
        self.assertEqual(question._answers[6].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(question._answers[6].column_header, 'q1s3st')
