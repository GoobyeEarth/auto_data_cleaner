from typing import Dict  # NOQA

from django.test import TestCase

from cleaning.modules.definitions.answer_column import AnswerColumn  # NOQA
from cleaning.modules.files.format.for_survey.uniques_answer_column import \
    UniqueAnswerColumnConverter


class UniqueAnswerColumnConverterTest(TestCase):
    def test_convert_array_to_entity_from_headers_open(self):
        converter = UniqueAnswerColumnConverter()
        target = \
            [[['NO', 'D', 'NO', 'NO', 'MID', '', '1', '', 'NO', '']],
             [['START', 'D', 'START', 'START', 'D', '', '2', '', '開始日時', '']],
             [['END', 'D', 'END', 'END', 'D', '', '3', '', '終了日時', '']],
             [['TIME', 'D', 'TIME', 'TIME', 'D', '', '4', '', '回答時間', '']],
             [['Q1', 'MTM', '', '', '', '', '', '', 'ここに設問文を入力します。', ''],
              ['', '', 'Q1S1', '', 'MA', '3', '', '', '選択肢A', ''],
              ['', '', '', 'q1s1c1', '', '', '5', '1', '選択肢A', ''],
              ['', '', '', 'q1s1c2', '', '', '6', '2', '選択肢B', ''],
              ['', '', '', 'q1s1c3', '', '', '7', '3', 'その他', ''],
              ['', '', 'Q1S1T3', 'q1s1t3', 'FA', '', '8', '3', 'その他', ''],
              ['', '', 'Q1S2', '', 'MA', '3', '', '', '選択肢B', ''],
              ['', '', '', 'q1s2c1', '', '', '9', '1', '選択肢A', ''],
              ['', '', '', 'q1s2c2', '', '', '10', '2', '選択肢B', ''],
              ['', '', '', 'q1s2c3', '', '', '11', '3', 'その他', ''],
              ['', '', 'Q1S2T3', 'q1s2t3', 'FA', '', '12', '3', 'その他', ''],
              ['', '', 'Q1S3', '', 'MA', '3', '', '', 'その他', ''],
              ['', '', '', 'q1s3c1', '', '', '13', '1', '選択肢A', ''],
              ['', '', '', 'q1s3c2', '', '', '14', '2', '選択肢B', ''],
              ['', '', '', 'q1s3c3', '', '', '15', '3', 'その他', ''],
              ['', '', 'Q1S3T3', 'q1s3t3', 'FA', '', '16', '3', 'その他', ''],
              ['', '', 'Q1S3ST', 'q1s3st', 'FA', '', '17', '', 'その他', '']],
             [['STA', 'D', 'STA', 'STA', 'D', '', '18', '', 'STA', '']],
             [['QUO', 'S', 'QUO', 'QUO', 'SA', '1', '19', '', 'QUO', ''],
              ['', '', '', '', '', '', '', '1', '全員対象', '']],
             [['UserAgent', 'D', 'UserAgent', 'UserAgent',
               'D', '', '20', '', 'UserAgent', '']],
             [['APPID', 'D', 'APPID', 'APPID', 'D', '', '21', '', 'APPID', '']],
             [['IPAddress', 'D', 'IPAddress', 'IPAddress', 'D', '', '22', '', 'IPAddress', '']]]

        actual = converter.convert_array_to_entity_from_headers(target)
        header_answer_columns: Dict[str,
                                    AnswerColumn] = actual['header_answer_columns']

        self.assertEqual(actual['questions_start'], 4)

        self.assertEqual(
            header_answer_columns['MONITOR_ID'].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(header_answer_columns['MONITOR_ID'].item_name, 'NO')
        self.assertEqual(
            header_answer_columns['MONITOR_ID'].column_header, 'NO')

        self.assertEqual(
            header_answer_columns['START_DATETIME'].ANSWERS_TYPE, 'DATETIME_ANSWERS')
        self.assertEqual(
            header_answer_columns['START_DATETIME'].item_name, 'START')
        self.assertEqual(
            header_answer_columns['START_DATETIME'].column_header, 'START')

        self.assertEqual(
            header_answer_columns['END_DATETIME'].ANSWERS_TYPE, 'DATETIME_ANSWERS')
        self.assertEqual(
            header_answer_columns['END_DATETIME'].item_name, 'END')
        self.assertEqual(
            header_answer_columns['END_DATETIME'].column_header, 'END')

        self.assertEqual(
            header_answer_columns['ANSWER_TIME'].ANSWERS_TYPE, 'PERIOD_ANSWERS')
        self.assertEqual(
            header_answer_columns['ANSWER_TIME'].item_name, 'TIME')
        self.assertEqual(
            header_answer_columns['ANSWER_TIME'].column_header, 'TIME')

    def test_convert_array_to_entity_from_headers_closed(self):
        converter = UniqueAnswerColumnConverter()
        target = \
            [[['RMID', 'MID', 'RMID', 'RMID', 'MID', '', '1', '', 'RMID', '']],
             [['MID', 'MID', 'MID', 'MID', 'MID', '', '2', '', 'モニタコード', '']],
             [['START', 'D', 'START', 'START', 'D', '', '3', '', '開始日時', '']],
             [['END', 'D', 'END', 'END', 'D', '', '4', '', '終了日時', '']],
             [['TIME', 'D', 'TIME', 'TIME', 'D', '', '5', '', '回答時間', '']],
             [['SQ1', 'S', 'SQ1', 'SQ1', 'SA', '2', '6', '', 'あなたの性別をお知らせください。', ''],
              ['', '', '', '', '', '', '', '1', '男性', ''],
              ['', '', '', '', '', '', '', '2', '女性', '']]]

        actual = converter.convert_array_to_entity_from_headers(target)
        header_answer_columns: Dict[str,
                                    AnswerColumn] = actual['header_answer_columns']

        self.assertEqual(actual['questions_start'], 5)

        self.assertEqual(
            header_answer_columns['MONITOR_ID'].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(header_answer_columns['MONITOR_ID'].item_name, 'RMID')
        self.assertEqual(
            header_answer_columns['MONITOR_ID'].column_header, 'RMID')

        self.assertEqual(
            header_answer_columns['MONITOR_CODE'].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(
            header_answer_columns['MONITOR_CODE'].item_name, 'MID')
        self.assertEqual(
            header_answer_columns['MONITOR_CODE'].column_header, 'MID')

        self.assertEqual(
            header_answer_columns['START_DATETIME'].ANSWERS_TYPE, 'DATETIME_ANSWERS')
        self.assertEqual(
            header_answer_columns['START_DATETIME'].item_name, 'START')
        self.assertEqual(
            header_answer_columns['START_DATETIME'].column_header, 'START')

        self.assertEqual(
            header_answer_columns['END_DATETIME'].ANSWERS_TYPE, 'DATETIME_ANSWERS')
        self.assertEqual(
            header_answer_columns['END_DATETIME'].item_name, 'END')
        self.assertEqual(
            header_answer_columns['END_DATETIME'].column_header, 'END')

        self.assertEqual(
            header_answer_columns['ANSWER_TIME'].ANSWERS_TYPE, 'PERIOD_ANSWERS')
        self.assertEqual(
            header_answer_columns['ANSWER_TIME'].item_name, 'TIME')
        self.assertEqual(
            header_answer_columns['ANSWER_TIME'].column_header, 'TIME')

    def test_convert_array_to_entity_from_footers(self):
        converter = UniqueAnswerColumnConverter()
        target = \
            [[['NO', 'D', 'NO', 'NO', 'MID', '', '1', '', 'NO', '']],
             [['START', 'D', 'START', 'START', 'D', '', '2', '', '開始日時', '']],
             [['END', 'D', 'END', 'END', 'D', '', '3', '', '終了日時', '']],
             [['TIME', 'D', 'TIME', 'TIME', 'D', '', '4', '', '回答時間', '']],
             [['Q1', 'MTM', '', '', '', '', '', '', 'ここに設問文を入力します。', ''],
              ['', '', 'Q1S1', '', 'MA', '3', '', '', '選択肢A', ''],
              ['', '', '', 'q1s1c1', '', '', '5', '1', '選択肢A', ''],
              ['', '', '', 'q1s1c2', '', '', '6', '2', '選択肢B', ''],
              ['', '', '', 'q1s1c3', '', '', '7', '3', 'その他', ''],
              ['', '', 'Q1S1T3', 'q1s1t3', 'FA', '', '8', '3', 'その他', ''],
              ['', '', 'Q1S2', '', 'MA', '3', '', '', '選択肢B', ''],
              ['', '', '', 'q1s2c1', '', '', '9', '1', '選択肢A', ''],
              ['', '', '', 'q1s2c2', '', '', '10', '2', '選択肢B', ''],
              ['', '', '', 'q1s2c3', '', '', '11', '3', 'その他', ''],
              ['', '', 'Q1S2T3', 'q1s2t3', 'FA', '', '12', '3', 'その他', ''],
              ['', '', 'Q1S3', '', 'MA', '3', '', '', 'その他', ''],
              ['', '', '', 'q1s3c1', '', '', '13', '1', '選択肢A', ''],
              ['', '', '', 'q1s3c2', '', '', '14', '2', '選択肢B', ''],
              ['', '', '', 'q1s3c3', '', '', '15', '3', 'その他', ''],
              ['', '', 'Q1S3T3', 'q1s3t3', 'FA', '', '16', '3', 'その他', ''],
              ['', '', 'Q1S3ST', 'q1s3st', 'FA', '', '17', '', 'その他', '']],
             [['STA', 'D', 'STA', 'STA', 'D', '', '18', '', 'STA', '']],
             [['QUO', 'S', 'QUO', 'QUO', 'SA', '1', '19', '', 'QUO', ''],
              ['', '', '', '', '', '', '', '1', '全員対象', '']],
             [['UserAgent', 'D', 'UserAgent', 'UserAgent',
               'D', '', '20', '', 'UserAgent', '']],
             [['APPID', 'D', 'APPID', 'APPID', 'D', '', '21', '', 'APPID', '']],
             [['IPAddress', 'D', 'IPAddress', 'IPAddress', 'D', '', '22', '', 'IPAddress', '']]]

        actual = converter.convert_array_to_entity_from_footers(target, 5)

        footer_answer_columns: AnswerColumn = actual['footer_answer_columns']
        self.assertEqual(actual['attribute_start'], 10)

        self.assertEqual(
            footer_answer_columns['STATUS'].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(footer_answer_columns['STATUS'].item_name, 'STA')
        self.assertEqual(footer_answer_columns['STATUS'].column_header, 'STA')

        self.assertEqual(
            footer_answer_columns['QUO'].ANSWERS_TYPE, 'SINGLE_SELECTED_ANSWERS')
        self.assertEqual(footer_answer_columns['QUO'].item_name, 'QUO')
        self.assertEqual(footer_answer_columns['QUO'].column_header, 'QUO')
        self.assertEqual(
            footer_answer_columns['QUO'].choice_labels, {1: '全員対象'})

        self.assertEqual(
            footer_answer_columns['USER_AGENT'].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(
            footer_answer_columns['USER_AGENT'].item_name, 'UserAgent')
        self.assertEqual(
            footer_answer_columns['USER_AGENT'].column_header, 'UserAgent')

        self.assertEqual(
            footer_answer_columns['APP_ID'].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(footer_answer_columns['APP_ID'].item_name, 'APPID')
        self.assertEqual(
            footer_answer_columns['APP_ID'].column_header, 'APPID')

        self.assertEqual(
            footer_answer_columns['IP_ADDRESS'].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(
            footer_answer_columns['IP_ADDRESS'].item_name, 'IPAddress')
        self.assertEqual(
            footer_answer_columns['IP_ADDRESS'].column_header, 'IPAddress')
