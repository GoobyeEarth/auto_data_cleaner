from ddt import ddt
from django.core.files import File
from django.test import TestCase

from cleaning.modules.files.format.for_survey.raw_data import ForSurveyRawDataEditor


@ddt
class ForSurveyRawDataEditorTest(TestCase):
    def test_get_all_answer_dict(self):
        editor = ForSurveyRawDataEditor()
        editor.set_encode('utf-8')

        path = 'cleaning/tests/modules/files/format/test_files/simple_fs_excluded/070005568_Rawdata.txt'  # NOQA
        f = open(path, 'r')
        file = File(f)
        editor._ForSurveyRawDataEditor__make_reader_from_file = lambda param: file

        actual = editor.get_all_answer_dict([5, 6], None)

        self.assertEqual(actual, {
            5: {'100000001': 'ナスが好きだから', '100000002': 'ナスが好きだから'},
            6: {'100000001': 'aaaa', '100000002': 'test'}})

    def test_get_all_answer_list(self):
        editor = ForSurveyRawDataEditor()
        editor.set_encode('utf-8')

        path = 'cleaning/tests/modules/files/format/test_files/simple_fs_excluded/070005568_Rawdata.txt'  # NOQA
        f = open(path, 'r')
        file = File(f)
        editor._ForSurveyRawDataEditor__make_reader_from_file = lambda param: file

        actual = editor.get_all_answer_list([5, 6], None)
        self.assertEqual(
            actual, {5: ['ナスが好きだから', 'ナスが好きだから'], 6: ['aaaa', 'test']})

    def test_get_all_answer_data_frame(self):
        editor = ForSurveyRawDataEditor()

        path = 'cleaning/tests/modules/files/format/test_files/simple_fs_excluded/070005568_Rawdata.txt'  # NOQA
        f = open(path, 'r')
        file = File(f)
        editor._ForSurveyRawDataEditor__make_reader_from_file = lambda param: file

        actual = editor.get_all_answer_data_frame([5, 6], None)

        self.assertEqual(actual.__len__(), 2)

        self.assertEqual(actual['q1t1'].to_dict('records'),
                         [{'answer_value': 'ナスが好きだから'}, {'answer_value': 'ナスが好きだから'}])

        self.assertEqual(actual['q1t2'].to_dict('records'),
                         [{'answer_value': 'aaaa'}, {'answer_value': 'test'}])

    def test_get_data_frame(self):
        editor = ForSurveyRawDataEditor()

        path = 'cleaning/tests/modules/files/format/test_files/simple_fs_excluded/070005568_Rawdata.txt'  # NOQA
        f = open(path, 'r')
        file = File(f)
        editor._ForSurveyRawDataEditor__make_reader_from_file = lambda param: file
        editor._get_file_path = lambda param: path
        actual = editor.get_data_frame([5, 6], file)
        self.assertEqual(actual.to_dict(),
                         {'q1t2': {0: 'aaaa', 1: 'test'}, 'STA': {0: 'COMP', 1: 'COMP'}})

    def test_get_string_io(self):
        editor = ForSurveyRawDataEditor()
        editor.set_encode('utf-8')

        path = 'cleaning/tests/modules/files/format/test_files/simple_fs_excluded/070005568_Rawdata.txt'  # NOQA
        f = open(path, 'r')
        file = File(f)
        editor._ForSurveyRawDataEditor__make_reader_from_file = lambda param: file

        actual = editor.get_string_io({0: True, 1: False}, None, 'true')
        actual.seek(0)

        self.assertEqual(actual.readlines(), [
            'NO\tSTART\tEND\tTIME\tq1t1\tq1t2\tSTA\tQUO\tUserAgent\tAPPID\tIPAddress\tFA_FLAG\r\n',
            '100000001\t2017/11/05-15:51:17\t2017/11/05-15:51:30\t0:0:14\tナスが好きだから\taaaa\tCOMP\t1\tMozilla/5.0 (Macintosh Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36\t  \t106.181.117.144\t1\r\n',  # NOQA
            '100000002\t2017/11/05-15:51:17\t2017/11/05-15:51:30\t0:0:13\tナスが好きだから\ttest\tCOMP\t1\tMozilla/5.0 (Macintosh Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36\t  \t106.181.117.144\t0\r\n'])  # NOQA

    def test_get_string_io_delete(self):
        editor = ForSurveyRawDataEditor()
        editor.set_encode('utf-8')

        path = 'cleaning/tests/modules/files/format/test_files/simple_fs_excluded/070005568_Rawdata.txt'  # NOQA
        f = open(path, 'r')
        file = File(f)
        editor._ForSurveyRawDataEditor__make_reader_from_file = lambda param: file

        actual = editor.get_string_io({0: True, 1: False}, None, 'false')

        actual.seek(0)
        self.maxDiff = None
        self.assertEqual(actual.readlines(), [
            'NO\tSTART\tEND\tTIME\tq1t1\tq1t2\tSTA\tQUO\tUserAgent\tAPPID\tIPAddress\r\n',
            '100000002\t2017/11/05-15:51:17\t2017/11/05-15:51:30\t0:0:13\tナスが好きだから\ttest\tCOMP\t1\tMozilla/5.0 (Macintosh Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36\t  \t106.181.117.144\r\n'])  # NOQA
