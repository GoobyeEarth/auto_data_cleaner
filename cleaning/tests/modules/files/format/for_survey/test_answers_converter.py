from django.test import TestCase
from typing import List  # NOQA

from cleaning.modules.definitions.answer_column import MultiSelectedAnswerColumn  # NOQA
from cleaning.modules.definitions.answer_column import SingleSelectedAnswerColumn  # NOQA
from cleaning.modules.definitions.answer_column import TextAnswerColumn  # NOQA
from cleaning.modules.files.format.for_survey.answer_column_converters.sub_text import \
    SubTextAnswersConverter
from cleaning.modules.files.format.for_survey.answers_converter import DefaultAnswerColumnConverter
from cleaning.modules.files.format.for_survey.answers_converter import MatrixesAnswersConverter


class SubTextAnswersConverterTest(TestCase):
    def test_convert_array_to_entity(self):
        converter = SubTextAnswersConverter()
        text_answers = converter.convert_array_to_entity(
            ['', '', 'Q1T3', 'q1t3', 'FA', '', '6', '3', 'その他', ''])
        self.assertEqual(text_answers.ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(text_answers.item_name, 'Q1T3')
        self.assertEqual(text_answers.column_header, 'q1t3')


class MatrixesAnswerConverterTest(TestCase):
    def test_divide_by_answers_array_mts(self):
        converter = MatrixesAnswersConverter()
        target = \
            [['Q1', 'MTS', '', '', '', '', '', '', 'ここに設問文を入力します。', ''],
             ['', '', 'Q1S1', 'q1s1', 'SA', '3', '5', '', '選択肢A', ''],
             ['', '', '', '', '', '', '', '1', '選択肢A', ''],
             ['', '', '', '', '', '', '', '2', '選択肢B', ''],
             ['', '', '', '', '', '', '', '3', 'その他', ''],
             ['', '', 'Q1S1T3', 'q1s1t3', 'FA', '', '6', '3', 'その他', ''],
             ['', '', 'Q1S2', 'q1s2', 'SA', '3', '7', '', '選択肢B', ''],
             ['', '', '', '', '', '', '', '1', '選択肢A', ''],
             ['', '', '', '', '', '', '', '2', '選択肢B', ''],
             ['', '', '', '', '', '', '', '3', 'その他', ''],
             ['', '', 'Q1S2T3', 'q1s2t3', 'FA', '', '8', '3', 'その他', ''],
             ['', '', 'Q1S3', 'q1s3', 'SA', '3', '9', '', 'その他', ''],
             ['', '', '', '', '', '', '', '1', '選択肢A', ''],
             ['', '', '', '', '', '', '', '2', '選択肢B', ''],
             ['', '', '', '', '', '', '', '3', 'その他', ''],
             ['', '', 'Q1S3T3', 'q1s3t3', 'FA', '', '10', '3', 'その他', ''],
             ['', '', 'Q1S3ST', 'q1s3st', 'FA', '', '11', '', 'その他', '']]

        answers_array_list = converter.divide_by_answers_array(target)

        expected = \
            [[['', '', 'Q1S1', 'q1s1', 'SA', '3', '5', '', '選択肢A', ''],
              ['', '', '', '', '', '', '', '1', '選択肢A', ''],
              ['', '', '', '', '', '', '', '2', '選択肢B', ''],
              ['', '', '', '', '', '', '', '3', 'その他', '']],
             [['', '', 'Q1S1T3', 'q1s1t3', 'FA', '', '6', '3', 'その他', '']],
             [['', '', 'Q1S2', 'q1s2', 'SA', '3', '7', '', '選択肢B', ''],
              ['', '', '', '', '', '', '', '1', '選択肢A', ''],
              ['', '', '', '', '', '', '', '2', '選択肢B', ''],
              ['', '', '', '', '', '', '', '3', 'その他', '']],
             [['', '', 'Q1S2T3', 'q1s2t3', 'FA', '', '8', '3', 'その他', '']],
             [['', '', 'Q1S3', 'q1s3', 'SA', '3', '9', '', 'その他', ''],
              ['', '', '', '', '', '', '', '1', '選択肢A', ''],
              ['', '', '', '', '', '', '', '2', '選択肢B', ''],
              ['', '', '', '', '', '', '', '3', 'その他', '']],
             [['', '', 'Q1S3T3', 'q1s3t3', 'FA', '', '10', '3', 'その他', '']],
             [['', '', 'Q1S3ST', 'q1s3st', 'FA', '', '11', '', 'その他', '']]]

        self.assertEqual(answers_array_list, expected)

    def test_divide_by_answers_array_mtm(self):
        converter = MatrixesAnswersConverter()
        target = \
            [['Q1', 'MTM', '', '', '', '', '', '', 'ここに設問文を入力します。', ''],
             ['', '', 'Q1S1', '', 'MA', '3', '', '', '選択肢A', ''],
             ['', '', '', 'q1s1c1', '', '', '5', '1', '選択肢A', ''],
             ['', '', '', 'q1s1c2', '', '', '6', '2', '選択肢B', ''],
             ['', '', '', 'q1s1c3', '', '', '7', '3', 'その他', ''],
             ['', '', 'Q1S1T3', 'q1s1t3', 'FA', '', '8', '3', 'その他', ''],
             ['', '', 'Q1S2', '', 'MA', '3', '', '', '選択肢B', ''],
             ['', '', '', 'q1s2c1', '', '', '9', '1', '選択肢A', ''],
             ['', '', '', 'q1s2c2', '', '', '10', '2', '選択肢B', ''],
             ['', '', '', 'q1s2c3', '', '', '11', '3', 'その他', ''],
             ['', '', 'Q1S2T3', 'q1s2t3', 'FA', '', '12', '3', 'その他', ''],
             ['', '', 'Q1S3', '', 'MA', '3', '', '', 'その他', ''],
             ['', '', '', 'q1s3c1', '', '', '13', '1', '選択肢A', ''],
             ['', '', '', 'q1s3c2', '', '', '14', '2', '選択肢B', ''],
             ['', '', '', 'q1s3c3', '', '', '15', '3', 'その他', ''],
             ['', '', 'Q1S3T3', 'q1s3t3', 'FA', '', '16', '3', 'その他', ''],
             ['', '', 'Q1S3ST', 'q1s3st', 'FA', '', '17', '', 'その他', '']]

        answers_array_list = converter.divide_by_answers_array(target)

        expected = \
            [[['', '', 'Q1S1', '', 'MA', '3', '', '', '選択肢A', ''],
              ['', '', '', 'q1s1c1', '', '', '5', '1', '選択肢A', ''],
              ['', '', '', 'q1s1c2', '', '', '6', '2', '選択肢B', ''],
              ['', '', '', 'q1s1c3', '', '', '7', '3', 'その他', '']],
             [['', '', 'Q1S1T3', 'q1s1t3', 'FA', '', '8', '3', 'その他', '']],
             [['', '', 'Q1S2', '', 'MA', '3', '', '', '選択肢B', ''],
              ['', '', '', 'q1s2c1', '', '', '9', '1', '選択肢A', ''],
              ['', '', '', 'q1s2c2', '', '', '10', '2', '選択肢B', ''],
              ['', '', '', 'q1s2c3', '', '', '11', '3', 'その他', '']],
             [['', '', 'Q1S2T3', 'q1s2t3', 'FA', '', '12', '3', 'その他', '']],
             [['', '', 'Q1S3', '', 'MA', '3', '', '', 'その他', ''],
              ['', '', '', 'q1s3c1', '', '', '13', '1', '選択肢A', ''],
              ['', '', '', 'q1s3c2', '', '', '14', '2', '選択肢B', ''],
              ['', '', '', 'q1s3c3', '', '', '15', '3', 'その他', '']],
             [['', '', 'Q1S3T3', 'q1s3t3', 'FA', '', '16', '3', 'その他', '']],
             [['', '', 'Q1S3ST', 'q1s3st', 'FA', '', '17', '', 'その他', '']]]
        self.assertEqual(answers_array_list, expected)

    def test_divide_by_answers_array_mtx(self):
        converter = MatrixesAnswersConverter

        target = \
            [['Q1', 'MTX', '', '', '', '', '', '', 'ここに設問文を入力します。', ''],
             ['', '', 'Q1S1', 'q1s1', 'SA', '5', '5', '', '選択肢A', ''],
             ['', '', '', '', '', '', '', '1', '選択肢A', ''],
             ['', '', '', '', '', '', '', '2', '選択肢B', ''],
             ['', '', '', '', '', '', '', '3', '選択肢C', ''],
             ['', '', '', '', '', '', '', '4', '選択肢D', ''],
             ['', '', '', '', '', '', '', '5', 'その他', ''],
             ['', '', 'Q1S2', '', 'MA', '5', '', '', '選択肢B', ''],
             ['', '', '', 'q1s2c1', '', '', '6', '1', '選択肢A', ''],
             ['', '', '', 'q1s2c2', '', '', '7', '2', '選択肢B', ''],
             ['', '', '', 'q1s2c3', '', '', '8', '3', '選択肢C', ''],
             ['', '', '', 'q1s2c4', '', '', '9', '4', '選択肢D', ''],
             ['', '', '', 'q1s2c5', '', '', '10', '5', 'その他', ''],
             ['', '', 'Q1S3', '', '', '', '', '', '選択肢C', ''],
             ['', '', '', 'q1s3t1', 'FA', '', '11', '1', '選択肢A', ''],
             ['', '', '', 'q1s3t2', 'FA', '', '12', '2', '選択肢B', ''],
             ['', '', '', 'q1s3t3', 'FA', '', '13', '3', '選択肢C', ''],
             ['', '', '', 'q1s3t4', 'FA', '', '14', '4', '選択肢D', ''],
             ['', '', '', 'q1s3t5', 'FA', '', '15', '5', 'その他', ''],
             ['', '', 'Q1S4', '', '', '', '', '', '選択肢D', ''],
             ['', '', '', 'q1s4t1', 'FA', '', '16', '1', '選択肢A', ''],
             ['', '', '', 'q1s4t2', 'FA', '', '17', '2', '選択肢B', ''],
             ['', '', '', 'q1s4t3', 'FA', '', '18', '3', '選択肢C', ''],
             ['', '', '', 'q1s4t4', 'FA', '', '19', '4', '選択肢D', ''],
             ['', '', '', 'q1s4t5', 'FA', '', '20', '5', 'その他', '']]

        answers_array_list = converter.divide_by_answers_array(target)

        expected = \
            [[['', '', 'Q1S1', 'q1s1', 'SA', '5', '5', '', '選択肢A', ''],
              ['', '', '', '', '', '', '', '1', '選択肢A', ''],
              ['', '', '', '', '', '', '', '2', '選択肢B', ''],
              ['', '', '', '', '', '', '', '3', '選択肢C', ''],
              ['', '', '', '', '', '', '', '4', '選択肢D', ''],
              ['', '', '', '', '', '', '', '5', 'その他', '']],
             [['', '', 'Q1S2', '', 'MA', '5', '', '', '選択肢B', ''],
              ['', '', '', 'q1s2c1', '', '', '6', '1', '選択肢A', ''],
              ['', '', '', 'q1s2c2', '', '', '7', '2', '選択肢B', ''],
              ['', '', '', 'q1s2c3', '', '', '8', '3', '選択肢C', ''],
              ['', '', '', 'q1s2c4', '', '', '9', '4', '選択肢D', ''],
              ['', '', '', 'q1s2c5', '', '', '10', '5', 'その他', '']],
             [['', '', 'Q1S3', '', '', '', '', '', '選択肢C', ''],
              ['', '', '', 'q1s3t1', 'FA', '', '11', '1', '選択肢A', ''],
              ['', '', '', 'q1s3t2', 'FA', '', '12', '2', '選択肢B', ''],
              ['', '', '', 'q1s3t3', 'FA', '', '13', '3', '選択肢C', ''],
              ['', '', '', 'q1s3t4', 'FA', '', '14', '4', '選択肢D', ''],
              ['', '', '', 'q1s3t5', 'FA', '', '15', '5', 'その他', '']],
             [['', '', 'Q1S4', '', '', '', '', '', '選択肢D', ''],
              ['', '', '', 'q1s4t1', 'FA', '', '16', '1', '選択肢A', ''],
              ['', '', '', 'q1s4t2', 'FA', '', '17', '2', '選択肢B', ''],
              ['', '', '', 'q1s4t3', 'FA', '', '18', '3', '選択肢C', ''],
              ['', '', '', 'q1s4t4', 'FA', '', '19', '4', '選択肢D', ''],
              ['', '', '', 'q1s4t5', 'FA', '', '20', '5', 'その他', '']]]

        self.assertEqual(answers_array_list, expected)

    def test_convert_single_selected_array_to_entity(self):
        converter = MatrixesAnswersConverter()
        target = \
            [['', '', 'Q1S1', 'q1s1', 'SA', '3', '5', '', '選択肢A', ''],
             ['', '', '', '', '', '', '', '1', '選択肢A', ''],
             ['', '', '', '', '', '', '', '2', '選択肢B', ''],
             ['', '', '', '', '', '', '', '3', 'その他', '']]

        answers: SingleSelectedAnswerColumn = converter.convert_single_selected_array_to_entity(
            target)
        self.assertEqual(answers.ANSWERS_TYPE, 'SINGLE_SELECTED_ANSWERS')
        self.assertEqual(answers.item_name, 'Q1S1')
        self.assertEqual(answers.column_header, 'q1s1')
        self.assertEqual(answers.choice_labels, {
            1: '選択肢A', 2: '選択肢B', 3: 'その他'})

    def test_convert_multi_selected_array_to_entity(self):
        converter = MatrixesAnswersConverter()
        target = \
            [['', '', 'Q1S1', '', 'MA', '3', '', '', '選択肢A', ''],
             ['', '', '', 'q1s1c1', '', '', '5', '1', '選択肢A', ''],
             ['', '', '', 'q1s1c2', '', '', '6', '2', '選択肢B', ''],
             ['', '', '', 'q1s1c3', '', '', '7', '3', 'その他', '']]

        answers: MultiSelectedAnswerColumn = converter.convert_multi_selected_array_to_entity(
            target)
        self.assertEqual(answers.ANSWERS_TYPE, 'MULTI_SELECTED_ANSWERS')
        self.assertEqual(answers.item_name, 'Q1S1')
        self.assertEqual(answers.column_header, {
            1: 'q1s1c1', 2: 'q1s1c2', 3: 'q1s1c3'})
        self.assertEqual(answers.choice_labels, {
            1: '選択肢A', 2: '選択肢B', 3: 'その他'})

    def test_convert_text_answered_array_to_entity(self):
        converter = MatrixesAnswersConverter()
        target = \
            [['', '', 'Q1S3', '', '', '', '', '', '選択肢C', ''],
             ['', '', '', 'q1s3t1', 'FA', '', '11', '1', '選択肢A', ''],
             ['', '', '', 'q1s3t2', 'FA', '', '12', '2', '選択肢B', ''],
             ['', '', '', 'q1s3t3', 'FA', '', '13', '3', '選択肢C', ''],
             ['', '', '', 'q1s3t4', 'FA', '', '14', '4', '選択肢D', ''],
             ['', '', '', 'q1s3t5', 'FA', '', '15', '5', 'その他', '']]

        answers_list: List[TextAnswerColumn] = converter.convert_text_answered_array_to_entity(
            target)

        self.assertEqual(answers_list.__len__(), 5)
        self.assertEqual(answers_list[0].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(answers_list[0].item_name, 'Q1S3')
        self.assertEqual(answers_list[0].column_header, 'q1s3t1')

        self.assertEqual(answers_list[1].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(answers_list[1].item_name, 'Q1S3')
        self.assertEqual(answers_list[1].column_header, 'q1s3t2')

        self.assertEqual(answers_list[2].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(answers_list[2].item_name, 'Q1S3')
        self.assertEqual(answers_list[2].column_header, 'q1s3t3')

        self.assertEqual(answers_list[3].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(answers_list[3].item_name, 'Q1S3')
        self.assertEqual(answers_list[3].column_header, 'q1s3t4')

        self.assertEqual(answers_list[4].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(answers_list[4].item_name, 'Q1S3')
        self.assertEqual(answers_list[4].column_header, 'q1s3t5')

    def test_convert_sub_text_answered_array_to_entity(self):
        converter = MatrixesAnswersConverter()
        target = [['', '', 'Q1S1T3', 'q1s1t3', 'FA', '', '6', '3', 'その他', '']]
        answers: TextAnswerColumn = converter.convert_sub_text_answered_array_to_entity(
            target)
        self.assertEqual(answers.ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(answers.item_name, 'Q1S1T3')
        self.assertEqual(answers.column_header, 'q1s1t3')

    def test_is_single_selected_answers(self):
        converter = MatrixesAnswersConverter()
        answer_array = [['', '', 'Q1S1', 'q1s1', 'SA', '5', '5', '', '選択肢A', ''],
                        ['', '', '', '', '', '', '', '1', '選択肢A', ''],
                        ['', '', '', '', '', '', '', '2', '選択肢B', ''],
                        ['', '', '', '', '', '', '', '3', '選択肢C', ''],
                        ['', '', '', '', '', '', '', '4', '選択肢D', ''],
                        ['', '', '', '', '', '', '', '5', 'その他', '']]
        result: bool = converter.is_single_selected_answers(answer_array)

        self.assertEqual(result, True)


class DefaultAnswerConverterTest(TestCase):
    def test_convert_array_to_entity_start(self):
        converter = DefaultAnswerColumnConverter()
        start = [['START', 'D', 'START', 'START', 'D', '', '2', '', '開始日時', '']]

        key, actual = converter.convert_array_to_entity(start)

        self.assertEqual(
            actual.ANSWERS_TYPE, 'DATETIME_ANSWERS')
        self.assertEqual(
            actual.item_name, 'START')
        self.assertEqual(
            actual.column_header, 'START')
        self.assertEqual(key, 'START_DATETIME')

    def test_convert_array_to_entity_end(self):
        converter = DefaultAnswerColumnConverter()
        start = [['END', 'D', 'END', 'END', 'D', '', '3', '', '終了日時', '']]

        key, actual = converter.convert_array_to_entity(start)

        self.assertEqual(
            actual.ANSWERS_TYPE, 'DATETIME_ANSWERS')
        self.assertEqual(
            actual.item_name, 'END')
        self.assertEqual(
            actual.column_header, 'END')
        self.assertEqual(key, 'END_DATETIME')

    def test_convert_array_to_entity_time(self):
        converter = DefaultAnswerColumnConverter()
        start = [['TIME', 'D', 'TIME', 'TIME', 'D', '', '4', '', '回答時間', '']]

        key, actual = converter.convert_array_to_entity(start)

        self.assertEqual(
            actual.ANSWERS_TYPE, 'PERIOD_ANSWERS')
        self.assertEqual(
            actual.item_name, 'TIME')
        self.assertEqual(
            actual.column_header, 'TIME')
        self.assertEqual(key, 'ANSWER_TIME')

    def test_convert_array_to_entity_sta(self):
        converter = DefaultAnswerColumnConverter()
        start = [['STA', 'D', 'STA', 'STA', 'D', '', '18', '', 'STA', '']]

        key, actual = converter.convert_array_to_entity(start)

        self.assertEqual(
            actual.ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(
            actual.item_name, 'STA')
        self.assertEqual(
            actual.column_header, 'STA')
        self.assertEqual(key, 'STATUS')

    def test_convert_array_to_entity_ua(self):
        converter = DefaultAnswerColumnConverter()
        start = [['UserAgent', 'D', 'UserAgent',
                  'UserAgent', 'D', '', '20', '', 'UserAgent', '']]

        key, actual = converter.convert_array_to_entity(start)

        self.assertEqual(
            actual.ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(
            actual.item_name, 'UserAgent')
        self.assertEqual(
            actual.column_header, 'UserAgent')
        self.assertEqual(key, 'USER_AGENT')

    def test_convert_array_to_entity_appid(self):
        converter = DefaultAnswerColumnConverter()
        start = [['APPID', 'D', 'APPID', 'APPID',
                  'D', '', '21', '', 'APPID', '']]

        key, actual = converter.convert_array_to_entity(start)

        self.assertEqual(
            actual.ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(
            actual.item_name, 'APPID')
        self.assertEqual(
            actual.column_header, 'APPID')
        self.assertEqual(key, 'APP_ID')

    def test_convert_array_to_entity_ip_address(self):
        converter = DefaultAnswerColumnConverter()
        start = [['IPAddress', 'D', 'IPAddress',
                  'IPAddress', 'D', '', '22', '', 'IPAddress', '']]

        key, actual = converter.convert_array_to_entity(start)

        self.assertEqual(
            actual.ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(
            actual.item_name, 'IPAddress')
        self.assertEqual(
            actual.column_header, 'IPAddress')
        self.assertEqual(key, 'IP_ADDRESS')

    def test_convert_array_to_entity_gate(self):
        converter = DefaultAnswerColumnConverter()
        start = \
            [['GATE', 'S', 'GATE', 'GATE', 'SA', '8', '265', '', 'GATE', ''],
             ['', '', '', '', '', '', '', '1', 'HSQ2で『1.男性20~24歳』いずれかを選択した方のみ', ''],
             ['', '', '', '', '', '', '', '2', 'HSQ2で『2.男性25~29歳』いずれかを選択した方のみ', ''],
             ['', '', '', '', '', '', '', '3', 'HSQ2で『3.男性30~34歳』いずれかを選択した方のみ', ''],
             ['', '', '', '', '', '', '', '4', 'HSQ2で『4.男性35~39歳』いずれかを選択した方のみ', ''],
             ['', '', '', '', '', '', '', '5', 'HSQ2で『5.女性20~24歳』いずれかを選択した方のみ', ''],
             ['', '', '', '', '', '', '', '6', 'HSQ2で『6.女性25~29歳』いずれかを選択した方のみ', ''],
             ['', '', '', '', '', '', '', '7', 'HSQ2で『7.女性30~34歳』いずれかを選択した方のみ', ''],
             ['', '', '', '', '', '', '', '8', 'HSQ2で『8.女性35~39歳』いずれかを選択した方のみ', '']]

        key, actual = converter.convert_array_to_entity(start)

        self.assertEqual(
            actual.ANSWERS_TYPE, 'SINGLE_SELECTED_ANSWERS')
        self.assertEqual(
            actual.item_name, 'GATE')
        self.assertEqual(
            actual.column_header, 'GATE')
        self.assertEqual(key, 'GATE')
