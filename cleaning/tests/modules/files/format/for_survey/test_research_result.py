from django.core.files import File
from django.test import TestCase

from cleaning.modules.files.format.for_survey.layout import ForSurveyLayoutEditor
from cleaning.modules.files.format.for_survey.research_result import ForSurveyResearchLayout  # NOQA


class ForSurveyResearchResultTest(TestCase):
    def test_filter_answers(self):
        editor = ForSurveyLayoutEditor()
        editor.set_encode('utf-8')
        f = open('./cleaning/tests/modules/files/format/test_files/simple_sa/070005565_Layout.txt',
                 'r')
        file = File(f)
        editor._ForSurveyLayoutEditor__make_reader_from_file = lambda param: file
        research_result: ForSurveyResearchLayout = editor.load(file)
        actual = research_result.filter_answers(lambda q, a: True)
        self.assertEqual(actual.__len__(), 2)

        actual = research_result.filter_answers(lambda q, a: False)
        self.assertEqual(actual.__len__(), 0)

    def test_get_time_column_no(self):
        editor = ForSurveyLayoutEditor()
        editor.set_encode('utf-8')

        editor = ForSurveyLayoutEditor()
        editor.set_encode('utf-8')
        f = open('./cleaning/tests/modules/files/format/test_files/simple_sa/070005565_Layout.txt',
                 'r')
        file = File(f)
        editor._ForSurveyLayoutEditor__make_reader_from_file = lambda param: file
        research_result: ForSurveyResearchLayout = editor.load(file)

        column_no = research_result.get_time().column_no
        self.assertEqual(column_no, 4)

    def test_get_sex(self):
        editor = ForSurveyLayoutEditor()
        editor.set_encode('utf-8')

        editor = ForSurveyLayoutEditor()
        editor.set_encode('utf-8')
        f = open('./cleaning/tests/modules/files/format/test_files/closed/026023545_Layout.txt',
                 'r')
        file = File(f)
        editor._ForSurveyLayoutEditor__make_reader_from_file = lambda param: file
        research_result: ForSurveyResearchLayout = editor.load(file)
        sa = research_result.get_sex()
        self.assertEqual(sa.column_header, 'SEX')
        self.assertEqual(sa.column_no, 269)
