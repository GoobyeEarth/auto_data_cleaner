from django.test import TestCase

from cleaning.modules.files.format.for_survey.layout import ForSurveyLayoutEditor
from cleaning.modules.files.format.for_survey.research_result import ForSurveyResearchLayout  # NOQA
from django.core.files import File


class LoadTest(TestCase):
    def test_simple_sa(self):
        editor = ForSurveyLayoutEditor()
        editor.set_encode('utf-8')
        f = open('./cleaning/tests/modules/files/format/test_files/simple_sa/070005565_Layout.txt',
                 'r')
        file = File(f)
        editor._ForSurveyLayoutEditor__make_reader_from_file = lambda param: file
        layout: ForSurveyResearchLayout = editor.load(file)

        self.assertEqual(layout.survey_title, '●●●に関するアンケート')

        self.assertEqual(
            layout.file_path,
            './cleaning/tests/modules/files/format/test_files/simple_sa/070005565_Layout.txt')
        self.assertEqual(layout.default_answers.__len__(), 9)
        self.assertEqual(
            layout.default_answers['STATUS'].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(layout.default_answers['STATUS'].item_name, 'STA')
        self.assertEqual(
            layout.default_answers['STATUS'].column_header, 'STA')

        self.assertEqual(
            layout.default_answers['QUO'].ANSWERS_TYPE, 'SINGLE_SELECTED_ANSWERS')
        self.assertEqual(layout.default_answers['QUO'].item_name, 'QUO')
        self.assertEqual(layout.default_answers['QUO'].column_header, 'QUO')
        self.assertEqual(
            layout.default_answers['QUO'].choice_labels, {1: '全員対象'})

        self.assertEqual(
            layout.default_answers['USER_AGENT'].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(
            layout.default_answers['USER_AGENT'].item_name, 'UserAgent')
        self.assertEqual(
            layout.default_answers['USER_AGENT'].column_header, 'UserAgent')

        self.assertEqual(
            layout.default_answers['APP_ID'].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(
            layout.default_answers['APP_ID'].item_name, 'APPID')
        self.assertEqual(
            layout.default_answers['APP_ID'].column_header, 'APPID')

        self.assertEqual(
            layout.default_answers['IP_ADDRESS'].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(
            layout.default_answers['IP_ADDRESS'].item_name, 'IPAddress')
        self.assertEqual(
            layout.default_answers['IP_ADDRESS'].column_header, 'IPAddress')

        self.assertEqual(layout.questions.__len__(), 1)
        self.assertEqual(
            layout.questions[0].QUESTION_TYPE, 'SINGLE_SELECTED_QUESTION')
        self.assertEqual(layout.questions[0].title, 'ここに設問文を入力します。')

    def test_closed(self):
        editor = ForSurveyLayoutEditor()
        editor.set_encode('utf-8')
        f = open('./cleaning/tests/modules/files/format/test_files/closed/026023545_Layout.txt',
                 'r')
        file = File(f)
        editor._ForSurveyLayoutEditor__make_reader_from_file = lambda param: file
        layout: ForSurveyResearchLayout = editor.load(file)

        self.assertEqual(layout.survey_title, 'あなたご自身に関するアンケート')

        self.assertEqual(layout.default_answers.__len__(), 10)
        self.assertEqual(
            layout.default_answers['STATUS'].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(layout.default_answers['STATUS'].item_name, 'STA')
        self.assertEqual(
            layout.default_answers['STATUS'].column_header, 'STA')

        self.assertEqual(
            layout.default_answers['QUO'].ANSWERS_TYPE, 'SINGLE_SELECTED_ANSWERS')
        self.assertEqual(layout.default_answers['QUO'].item_name, 'QUO')
        self.assertEqual(layout.default_answers['QUO'].column_header, 'QUO')
        self.assertEqual(
            layout.default_answers['QUO'].choice_labels, {1: '性別：『男性, 女性』'})
        self.assertEqual(
            layout.default_answers['GATE'].ANSWERS_TYPE, 'SINGLE_SELECTED_ANSWERS')
        self.assertEqual(layout.default_answers['GATE'].item_name, 'GATE')
        self.assertEqual(layout.default_answers['GATE'].column_header, 'GATE')
        self.assertEqual(
            layout.default_answers['GATE'].choice_labels,
            {1: 'HSQ2で『1.男性20~24歳』いずれかを選択した方のみ', 2: 'HSQ2で『2.男性25~29歳』いずれかを選択した方のみ',
             3: 'HSQ2で『3.男性30~34歳』いずれかを選択した方のみ', 4: 'HSQ2で『4.男性35~39歳』いずれかを選択した方のみ',
             5: 'HSQ2で『5.女性20~24歳』いずれかを選択した方のみ', 6: 'HSQ2で『6.女性25~29歳』いずれかを選択した方のみ',
             7: 'HSQ2で『7.女性30~34歳』いずれかを選択した方のみ', 8: 'HSQ2で『8.女性35~39歳』いずれかを選択した方のみ'})

        self.assertEqual(
            layout.default_answers['USER_AGENT'].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(
            layout.default_answers['USER_AGENT'].item_name, 'UserAgent')
        self.assertEqual(
            layout.default_answers['USER_AGENT'].column_header, 'UserAgent')

        self.assertEqual(
            layout.default_answers['IP_ADDRESS'].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(
            layout.default_answers['IP_ADDRESS'].item_name, 'IPAddress')
        self.assertEqual(
            layout.default_answers['IP_ADDRESS'].column_header, 'IPAddress')

        self.assertEqual(layout.questions.__len__(), 34)
        # テスト足りないので追加

    def test_all(self):
        editor = ForSurveyLayoutEditor()
        editor.set_encode('utf-8')
        f = open(
            './cleaning/tests/modules/files/format/test_files/simple_fs_all/046005989_Layout.txt',
            'r')
        file = File(f)
        editor._ForSurveyLayoutEditor__make_reader_from_file = lambda param: file
        layout: ForSurveyResearchLayout = editor.load(file)

        self.assertEqual(layout.survey_title, 'SA挙動確認')

        self.assertEqual(layout.default_answers.__len__(), 10)
        self.assertEqual(
            layout.default_answers['STATUS'].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(layout.default_answers['STATUS'].item_name, 'STA')
        self.assertEqual(
            layout.default_answers['STATUS'].column_header, 'STA')

        self.assertEqual(
            layout.default_answers['QUO'].ANSWERS_TYPE, 'SINGLE_SELECTED_ANSWERS')
        self.assertEqual(layout.default_answers['QUO'].item_name, 'QUO')
        self.assertEqual(layout.default_answers['QUO'].column_header, 'QUO')
        self.assertEqual(
            layout.default_answers['QUO'].choice_labels, {1: '全員対象'})

        self.assertEqual(
            layout.default_answers['USER_AGENT'].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(
            layout.default_answers['USER_AGENT'].item_name, 'UserAgent')
        self.assertEqual(
            layout.default_answers['USER_AGENT'].column_header, 'UserAgent')

        self.assertEqual(
            layout.default_answers['APP_ID'].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(
            layout.default_answers['APP_ID'].item_name, 'APPID')
        self.assertEqual(
            layout.default_answers['APP_ID'].column_header, 'APPID')

        self.assertEqual(
            layout.default_answers['IP_ADDRESS'].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(
            layout.default_answers['IP_ADDRESS'].item_name, 'IPAddress')
        self.assertEqual(
            layout.default_answers['IP_ADDRESS'].column_header, 'IPAddress')

        self.assertEqual(
            layout.default_answers['key'].ANSWERS_TYPE, 'TEXT_ANSWERS')
        self.assertEqual(
            layout.default_answers['key'].item_name, 'key')
        self.assertEqual(
            layout.default_answers['key'].column_header, 'key')

        self.assertEqual(layout.questions.__len__(), 1)
        self.assertEqual(
            layout.questions[0].QUESTION_TYPE, 'TEXT_ANSWERED_QUESTION')
        self.assertEqual(layout.questions[0].title, 'ここに設問文を入力します。')
