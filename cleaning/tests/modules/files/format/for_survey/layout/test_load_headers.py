import csv

from django.core.files import File
from django.test import TestCase

from cleaning.modules.files.format.for_survey.layout import ForSurveyLayoutEditor
from cleaning.modules.files.format.for_survey.research_result import ForSurveyResearchLayout  # NOQA


class LoadHeadersTest(TestCase):
    def test_simple(self):
        editor = ForSurveyLayoutEditor()
        editor.set_encode('utf-8')
        f = open('./cleaning/tests/modules/files/format/test_files/layout.txt', 'r')

        file = File(f)
        editor._ForSurveyLayoutEditor__reader = csv.reader(
            file, delimiter='\t')
        layout: ForSurveyResearchLayout = editor._ForSurveyLayoutEditor__load_headers()

        self.assertEqual(layout.survey_title, 'あなたご自身に関するアンケート')
        self.assertEqual(layout.questions, [])
