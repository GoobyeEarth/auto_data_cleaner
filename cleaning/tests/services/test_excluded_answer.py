from ddt import data
from ddt import ddt
from ddt import unpack

from django.core.files import File
from django.test import TestCase
import pandas as pd

from cleaning.services.excluded_answer import ExcludedAnswerService


@ddt
class ExcludedAnswerServiceTest(TestCase):
    def test_exclude_duplications(self):
        target = {
            'q1t1': pd.DataFrame([['ナスが好きだから'], ['ナスが好きだから']], columns=['answer_value']),
            'q1t2': pd.DataFrame([['aaaa'], ['test']], columns=['answer_value'])
        }
        service = ExcludedAnswerService()

        actual = service._exclude_duplications(target)

        self.assertEqual(actual.__len__(), 2)

        self.assertEqual(actual['q1t1'].to_dict('records'),
                         [{'answer_value': 'ナスが好きだから'}])

        self.assertEqual(actual['q1t2'].to_dict('records'),
                         [{'answer_value': 'aaaa'},
                          {'answer_value': 'test'}])

    def test_get_duplication_excluded_answers(self):
        service = ExcludedAnswerService()
        layout_file_path = './cleaning/tests/modules/files/format/test_files/simple_fs_excluded/070005568_Layout.txt'  # NOQA
        layout_file = File(open(layout_file_path, 'r'))
        service._ExcludedAnswerService__layout_editor. \
            _ForSurveyLayoutEditor__make_reader_from_file = lambda param: layout_file
        raw_data_file_path = './cleaning/tests/modules/files/format/test_files/simple_fs_excluded/070005568_Rawdata.txt'  # NOQA
        raw_data_file = File(open(raw_data_file_path, 'r'))
        service._ExcludedAnswerService__raw_data_editor. \
            _ForSurveyRawDataEditor__make_reader_from_file = lambda param: raw_data_file

        research_result = service._ExcludedAnswerService__layout_editor.load(
            layout_file.file)
        actual = service._get_duplication_excluded_answers(research_result, raw_data_file,
                                                           {'too_less_words': 'true',
                                                            'too_match_consonants': 'true',
                                                            'redundant_word': 'true'})

        self.assertEqual(actual, {5: {'item_name': '', 'column_header': 'q1t1', 'column_no': 5,
                                      'answers': [
                                          {'answer_value': 'ナスが好きだから', 'judgement': False}]},
                                  6: {'item_name': '', 'column_header': 'q1t2', 'column_no': 6,
                                      'answers': [{'answer_value': 'aaaa', 'judgement': True},
                                                  {'answer_value': 'test', 'judgement': True}]}})

    @unpack
    @data(
        {
            'excluded_answers': {
                'q1t1': pd.DataFrame([['ナスが好きだから']], columns=['answer_value']),
                'q1t2': pd.DataFrame([['aaaaa'], ['test']], columns=['answer_value'])
            },
            'request_params': {
                'too_less_words': 'true',
                'too_match_consonants': 'true',
                'redundant_word': 'true',
            },

            'expected': {'q1t1': [{'answer_value': 'ナスが好きだから', 'judgement': False}],
                         'q1t2': [{'answer_value': 'test', 'judgement': True},
                                  {'answer_value': 'aaaaa', 'judgement': True}]}
        },
        {
            'excluded_answers': {
                'q1t1': pd.DataFrame([['ナスが好きだから']], columns=['answer_value']),
                'q1t2': pd.DataFrame([['aaaaa'], ['test']], columns=['answer_value'])
            },
            'request_params': {
                'too_less_words': 'false',
                'too_match_consonants': 'false',
                'redundant_word': 'false',
            },

            'expected': {'q1t1': [{'answer_value': 'ナスが好きだから', 'judgement': False}],
                         'q1t2': [{'answer_value': 'test', 'judgement': False},
                                  {'answer_value': 'aaaaa', 'judgement': False}]}
        },
    )
    def test_filter_answers(self, excluded_answers, request_params, expected):
        service = ExcludedAnswerService()

        actual = service.filter_answers(excluded_answers, request_params)

        self.assertEqual(actual, expected)
