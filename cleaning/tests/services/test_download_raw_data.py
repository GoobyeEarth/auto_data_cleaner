from ddt import data
from ddt import ddt
from ddt import unpack

from django.core.files import File
from django.test import TestCase

from cleaning.modules.files.format.for_survey.layout import ForSurveyLayoutEditor
from cleaning.modules.files.format.for_survey.raw_data import ForSurveyRawDataEditor

from cleaning.services.download_raw_data import BasicCheckFlagParser
from cleaning.services.download_raw_data import TextAnswerFlagParser


@ddt
class TextAnswerFlagParserTest(TestCase):
    @unpack
    @data(
        {
            'answers': {5: ['ナスが好きだから', 'ナスが好きだから'], 6: ['aaaa', 'test']},
            'excluded_answers_list': {5: [], 6: ['test']},
            'expected': {0: False, 1: True},
        },
        {
            'answers': {
                5: ['ナスが好きだから', 'ナスが好きだから', 'aaaa'],
                6: ['これはOK', 'test', 'これはOK']
            },
            'excluded_answers_list': {5: ['aaaa'], 6: ['test']},
            'expected': {0: False, 1: True, 2: True},
        }
    )
    def test__parse_flags_list(self, answers, excluded_answers_list, expected):
        parser = TextAnswerFlagParser(None)
        actual = parser._parse_flags_list(excluded_answers_list, answers)
        self.assertEqual(actual.to_dict(), expected)

    def test__parse_column_no_from_excluded_answers_list(self):
        parser = TextAnswerFlagParser(None)
        excluded = {5: ['ナスが好きだから', 'ナスが好きだから'], 6: ['aaaa', 'test']}
        actual = parser._parse_column_nos_from_excluded_answers_list(excluded)
        self.assertEqual(actual, [5, 6])

    @unpack
    @data(
        {
            'excluded_answers_list': {5: [], 6: ['test']},
            'raw_data_file_path': './cleaning/tests/modules/files/format/test_files/simple_fs_excluded/070005568_Rawdata.txt',  # NOQA
            'expected': {0: False, 1: True},
        },

    )
    def test_get_flags(self, excluded_answers_list, raw_data_file_path, expected):
        raw_data_editor = ForSurveyRawDataEditor()
        raw_data_editor.set_encode('utf-8')

        raw_data_file = File(open(raw_data_file_path, 'r'))
        parser = TextAnswerFlagParser(raw_data_editor)
        raw_data_editor._ForSurveyRawDataEditor__make_reader_from_file = lambda param: raw_data_file
        flag = parser.get_flags(excluded_answers_list, raw_data_file)
        self.assertEqual(flag.to_dict(), expected)


@ddt
class BasicCheckAnswerFlagParserTest(TestCase):
    def test_get_flag(self):
        basic_check = \
            {
                'answer_speed': {'active': True, 'cut_ratio': 50},
                'customize_standards': True,
                'monitor_duplication':
                    {
                        'active': True,
                        'age': True,
                        'area': True,
                        'ip_address': True,
                        'sex': True,
                        'user_agent': True
                }
            }

        raw_data_editor = ForSurveyRawDataEditor()
        raw_data_editor.set_encode('utf-8')
        raw_data_file_path = \
            './cleaning/tests/modules/files/format/test_files/monitor_duplicated/026023545_Rawdata.txt'  # NOQA
        raw_data_file = File(open(raw_data_file_path, 'r'))
        raw_data_editor._ForSurveyRawDataEditor__make_reader_from_file = lambda param: raw_data_file
        raw_data_editor._get_file_path = lambda param: raw_data_file_path

        f = open(
            './cleaning/tests/modules/files/format/test_files/monitor_duplicated/026023545_Layout.txt',  # NOQA
            'r')
        file = File(f)
        editor = ForSurveyLayoutEditor()
        editor._ForSurveyLayoutEditor__make_reader_from_file = lambda param: file
        layout = editor.load(file)

        parser = BasicCheckFlagParser(raw_data_editor)
        flag = parser.get_flag(basic_check, layout, raw_data_file)
        self.assertEqual(flag.to_dict(),
                         {0: True, 1: False, 2: False, 3: False, 4: False, 5: False, 6: False,
                          7: True, 8: True,
                          9: True, 10: True, 11: True})

    def test_get_answer_speed_flag(self):
        raw_data_editor = ForSurveyRawDataEditor()
        raw_data_editor.set_encode('utf-8')
        raw_data_file_path = './cleaning/tests/modules/files/format/test_files/simple_fs_excluded/070005568_Rawdata.txt'  # NOQA
        raw_data_file = File(open(raw_data_file_path, 'r'))
        raw_data_editor._ForSurveyRawDataEditor__make_reader_from_file = lambda param: raw_data_file
        raw_data_editor._get_file_path = lambda param: raw_data_file_path

        f = open(
            './cleaning/tests/modules/files/format/test_files/simple_fs_excluded/070005568_Layout.txt',  # NOQA
            'r')
        file = File(f)
        editor = ForSurveyLayoutEditor()

        editor._ForSurveyLayoutEditor__make_reader_from_file = lambda param: file
        layout = editor.load(file)

        parser = BasicCheckFlagParser(raw_data_editor)
        flag = parser.get_answer_speed_flag(50, layout, raw_data_file)
        self.assertEqual(flag.to_dict(), {0: False, 1: True})

    @unpack
    @data(
        {
            'monitor_duplication_request': {
                'user_agent': True,
                'ip_address': True,
                'sex': True,
                'age': True,
                'area': True,
            },
            'layout_file_path': './cleaning/tests/modules/files/format/test_files/monitor_duplicated/026023545_Layout.txt',  # NOQA
            'raw_data_file_path': './cleaning/tests/modules/files/format/test_files/monitor_duplicated/026023545_Rawdata.txt',  # NOQA
            'expected': {0: False, 1: False, 2: False, 3: False, 4: False, 5: False, 6: False,
                         7: False, 8: False, 9: True, 10: False, 11: False},
        },
        {
            'monitor_duplication_request': {
                'user_agent': True,
                'ip_address': True,
                'sex': False,
                'age': True,
                'area': True,
            },
            'layout_file_path': './cleaning/tests/modules/files/format/test_files/monitor_duplicated/026023545_Layout.txt',  # NOQA
            'raw_data_file_path': './cleaning/tests/modules/files/format/test_files/monitor_duplicated/026023545_Rawdata.txt',  # NOQA
            'expected': {0: False, 1: False, 2: False, 3: False, 4: False, 5: False, 6: False,
                         7: False, 8: False, 9: True, 10: True, 11: True},
        },

    )
    def test_get_monitor_duplication_flag(self, monitor_duplication_request, layout_file_path,
                                          raw_data_file_path, expected):
        raw_data_editor = ForSurveyRawDataEditor()
        raw_data_editor.set_encode('utf-8')
        raw_data_file = File(open(raw_data_file_path, 'r'))
        raw_data_editor._ForSurveyRawDataEditor__make_reader_from_file = lambda param: raw_data_file
        raw_data_editor._get_file_path = lambda param: raw_data_file_path
        f = open(layout_file_path, 'r')

        file = File(f)
        editor = ForSurveyLayoutEditor()

        editor._ForSurveyLayoutEditor__make_reader_from_file = lambda param: file
        layout = editor.load(file)

        parser = BasicCheckFlagParser(raw_data_editor)
        actual = parser.get_monitor_duplication_flag(monitor_duplication_request, layout,
                                                     raw_data_file)

        self.assertEqual(actual.to_dict(), expected)
