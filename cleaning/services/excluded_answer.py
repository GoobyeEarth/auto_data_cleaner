from typing import Dict

from django.core.files import File
import pandas as pd

from cleaning.models import Cleaning
from cleaning.models import FileGroup
from cleaning.models import FileTypes
from cleaning.models import UploadedFile
from cleaning.modules.definitions.answer import AnswerColumnDictLabel as Answer
from cleaning.modules.definitions.answer_column import TextAnswerColumn
from cleaning.modules.files.format.for_survey.layout import ForSurveyLayoutEditor
from cleaning.modules.files.format.for_survey.raw_data import ForSurveyRawDataEditor
from cleaning.modules.files.format.for_survey.research_result import ForSurveyResearchLayout  # NOQA
from cleaning.modules.filters import TextAnswerFilter


class ExcludedAnswerService(object):
    def __init__(self):
        self.__layout_editor = ForSurveyLayoutEditor()
        self.__raw_data_editor = ForSurveyRawDataEditor()
        self.__text_answer_filter = TextAnswerFilter()

    def get_duplication_excluded_answers(self, request_params: Dict[str, any]) -> Dict[int, any]:
        cleaning = Cleaning.objects.filter(id=request_params['cleaning_id'])[0]
        file_group = FileGroup.objects.filter(cleaning=cleaning)[0]

        layout_file = UploadedFile.objects.filter(
            file_group=file_group, file_type=FileTypes.LAYOUT)[0]
        raw_data_file = UploadedFile.objects.filter(
            file_group=file_group, file_type=FileTypes.RAW_DATA)[0]

        self.__layout_editor.set_encode(layout_file.encode)
        self.__raw_data_editor.set_encode(raw_data_file.encode)

        research_result: ForSurveyResearchLayout = self.__layout_editor.load(
            layout_file.file)

        return self._get_duplication_excluded_answers(research_result, raw_data_file,
                                                      request_params)

    def _get_duplication_excluded_answers(self, research_result: ForSurveyResearchLayout,
                                          raw_data_file: File, request_params):
        answers_dict: Dict[int, TextAnswerColumn] = research_result.filter_answers(
            lambda question, answers: answers.ANSWERS_TYPE == TextAnswerColumn.ANSWERS_TYPE)

        excluded_answers = self._load_duplications_excluded_answers_from_files(answers_dict,
                                                                               raw_data_file)

        filtered_answers = self.filter_answers(
            excluded_answers, request_params)

        tables = {}
        for column_no, answer_column in answers_dict.items():
            table = answer_column.to_dict()
            table[Answer.ANSWERS] = filtered_answers[answer_column.column_header]
            tables[column_no] = table

        return tables

    def _load_duplications_excluded_answers_from_files(self,
                                                       answers_dict: Dict,
                                                       raw_data_file: File) -> Dict[str, any]:
        raw_data_answers_dict = self.__raw_data_editor.get_all_answer_data_frame(
            list(answers_dict.keys()), raw_data_file.file)

        excluded_answers_dict = self._exclude_duplications(
            raw_data_answers_dict)

        return excluded_answers_dict

    @staticmethod
    def _exclude_duplications(answer_data_flames: Dict[str, pd.DataFrame]):
        excluded = {}
        for column_header, data_flame in answer_data_flames.items():
            excluded[column_header] = data_flame.drop_duplicates(
                [Answer.ANSWER_VALUE])
        return excluded

    def filter_answers(self, answer_data_flames: Dict[str, pd.DataFrame], request_params):
        filtered_answers: Dict[str, pd.DataFrame] = {}
        for column_headers, answer_flame in answer_data_flames.items():
            filters = answer_flame.apply(lambda x: False, axis=1)

            if request_params[TextAnswerFilter.TOO_LESS_WORDS] == 'true':
                too_less_words = answer_flame.apply(
                    lambda x: self.__text_answer_filter.filter_too_less_words(
                        x[Answer.ANSWER_VALUE]),
                    axis=1)
                filters = pd.concat([filters, too_less_words], axis=1).T.any()

            if request_params[TextAnswerFilter.TOO_MATCH_CONSONANTS] == 'true':
                too_match_consonants = answer_flame.apply(
                    lambda x: self.__text_answer_filter.filter_too_match_consonants(
                        x[Answer.ANSWER_VALUE]),
                    axis=1)
                filters = pd.concat(
                    [filters, too_match_consonants], axis=1).T.any()

            if request_params[TextAnswerFilter.REDUNDANT_WORD] == 'true':
                redundant_word = answer_flame.apply(
                    lambda x: self.__text_answer_filter.filter_redundant_word(
                        x[Answer.ANSWER_VALUE]),
                    axis=1)
                filters = pd.concat([filters, redundant_word], axis=1).T.any()

            filtered_answer = pd.concat(
                [
                    answer_flame,
                    filters,
                    answer_flame.apply(
                        lambda x: len(x[Answer.ANSWER_VALUE]), axis=1)
                ], axis=1)

            filtered_answer.columns = [Answer.ANSWER_VALUE, TextAnswerFilter.JUDGEMENT,
                                       TextAnswerFilter.WORD_NUM]
            filtered_answer = filtered_answer.sort_values(
                by=[TextAnswerFilter.WORD_NUM], ascending=True)

            filtered_answers[column_headers] = \
                filtered_answer.ix[:, [Answer.ANSWER_VALUE, TextAnswerFilter.JUDGEMENT]] \
                .to_dict('records')

        return filtered_answers
