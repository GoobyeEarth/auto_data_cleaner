from typing import Dict

from django.core.files import File

from auto_data_cleaner.utils.api import ApiSpec
from auto_data_cleaner.utils.exceptions import BadRequestException
from cleaning.models import FilesTypes
from cleaning.modules.lifecycles import CleaningLifeCycle


class UploadFileService(object):
    @staticmethod
    def save_uploaded_file(requests: Dict[str, any]) -> int:
        """
        :param requests:
        :return: cleaning.id
        """
        if requests['files_type'] == FilesTypes.FOR_SURVEY:
            return UploadFileService.save_for_survey_file(requests['uploaded_files'])

        raise BadRequestException(ApiSpec.create_response(
            ApiSpec.ResponseCode.UNEXPECTED_FILES_TYPE,
            'unexpected files files_type',
            {'files_type': requests['files_type']}
        ))

    @staticmethod
    def save_for_survey_file(uploaded_files: Dict[str, File]) -> int:
        """
        :param uploaded_files:
        :return: cleaning.id
        """
        if uploaded_files['layout'] is None or uploaded_files['raw_data'] is None:
            raise BadRequestException(ApiSpec.create_response(
                ApiSpec.ResponseCode.FILE_NOT_UPLOADED,
                'layout or raw_data file is not uploaded',
                {}
            ))

        return CleaningLifeCycle.upload_target_files(FilesTypes.FOR_SURVEY, uploaded_files)
