import numpy as np
import pandas as pd
from typing import Dict
from typing import List

from django.core.files import File

from cleaning.models import Cleaning
from cleaning.models import FileGroup
from cleaning.models import FileTypes
from cleaning.models import UploadedFile
from cleaning.modules.definitions.answer import AnswerColumnDictLabel as Answer
from cleaning.modules.files.format.for_survey.layout import ForSurveyLayoutEditor
from cleaning.modules.files.format.for_survey.raw_data import ForSurveyRawDataEditor
from cleaning.modules.files.format.for_survey.research_result import ForSurveyResearchLayout


class DownloadRawDataFileService(object):
    def __init__(self):
        self._for_survey_raw_data_editor = ForSurveyRawDataEditor()
        self._for_survey_layout_editor = ForSurveyLayoutEditor()
        self._text_answer_flag_parser = TextAnswerFlagParser(
            self._for_survey_raw_data_editor)
        self._basic_check_flag_parser = BasicCheckFlagParser(
            self._for_survey_raw_data_editor)

    def make_string_io(self, request_params: Dict[str, any]):
        excluded_answers_list = self.normalize_excluded_answers_list(
            request_params['excluded_answers_list'])

        cleaning = Cleaning.objects.filter(id=request_params['cleaning_id'])[0]
        file_group = FileGroup.objects.filter(cleaning=cleaning)[0]

        raw_data_file = UploadedFile.objects.filter(
            file_group=file_group, file_type=FileTypes.RAW_DATA)[0]

        layout_file = UploadedFile.objects.filter(
            file_group=file_group, file_type=FileTypes.LAYOUT)[0]

        self._for_survey_raw_data_editor.set_encode(raw_data_file.encode)
        self._for_survey_layout_editor.set_encode(raw_data_file.encode)

        layout = self._for_survey_layout_editor.load(layout_file.file)

        text_answer_flag = self._text_answer_flag_parser.get_flags(
            excluded_answers_list,
            raw_data_file.file)

        answer_flag = pd.Series()
        basic_answer_flag = self._basic_check_flag_parser.get_flag(
            request_params['basic_check'], layout, raw_data_file.file)

        if len(answer_flag) == 0:
            answer_flag = basic_answer_flag
        else:
            answer_flag = np.logical_or(answer_flag, basic_answer_flag)

        if len(answer_flag) == 0:
            answer_flag = text_answer_flag
        else:
            answer_flag = np.logical_or(answer_flag, text_answer_flag)

        return {
            'io': self._for_survey_raw_data_editor.get_string_io(answer_flag,
                                                                 raw_data_file.file,
                                                                 request_params['leave_flags']),
            'filename': raw_data_file.name,
            'encode': raw_data_file.encode
        }

    @staticmethod
    def normalize_excluded_answers_list(excluded_answers_list: Dict[str, List[any]]):
        normalized = {}
        for column_no, excluded_answers in excluded_answers_list.items():
            normalized[int(column_no)] = excluded_answers
        return normalized


class TextAnswerFlagParser(object):
    def __init__(self, for_survey_raw_data_editor):
        self._for_survey_raw_data_editor = for_survey_raw_data_editor

    def get_flags(self, excluded_answers_list, raw_data_file: File):
        answers_list = self._load_answers(excluded_answers_list, raw_data_file)
        return self._parse_flags_list(excluded_answers_list, answers_list)

    @staticmethod
    def _parse_flags_list(excluded_answers_list, answers_list: Dict[int, List[any]]):
        flags = {}
        for column_no, answers in answers_list.items():
            for count in range(answers.__len__()):
                if count not in flags:
                    flags[count] = False
                if answers[count] in excluded_answers_list[column_no]:
                    flags[count] = flags[count] or True
        return pd.Series(flags)

    def _load_answers(self, excluded_answers_list: Dict[int, Dict],
                      raw_data_file):
        column_nos = self._parse_column_nos_from_excluded_answers_list(
            excluded_answers_list)
        return self._for_survey_raw_data_editor.get_all_answer_list(column_nos, raw_data_file)

    @staticmethod
    def _parse_column_nos_from_excluded_answers_list(excluded_answers_list: Dict[int, any]):
        return list(excluded_answers_list.keys())


class BasicCheckFlagParser(object):
    def __init__(self, for_survey_raw_data_editor):
        self._for_survey_raw_data_editor = for_survey_raw_data_editor

    def get_flag(self, basic_check, layout: ForSurveyResearchLayout, raw_data_file: File):
        flag = pd.Series()

        if basic_check['answer_speed']['active']:
            speed_flag = self.get_answer_speed_flag(
                int(basic_check['answer_speed']['cut_ratio']), layout, raw_data_file)
            if len(flag) == 0:
                flag = speed_flag
            else:
                flag = np.logical_or(flag, speed_flag)

        if basic_check['monitor_duplication']['active']:
            duplication_flag = self.get_monitor_duplication_flag(
                basic_check['monitor_duplication'], layout, raw_data_file)
            if len(flag) == 0:
                flag = duplication_flag
            else:
                flag = np.logical_or(flag, duplication_flag)

        return flag

    def get_answer_speed_flag(self, cut_ratio, layout, raw_data_file):

        time_column_no = layout.get_time().column_no
        data_frames = self._for_survey_raw_data_editor.get_all_answer_data_frame([time_column_no],
                                                                                 raw_data_file)
        data_frame = list(data_frames.values())[0]
        row_num = len(data_frame.index)
        target_num = self._calc_cut_num(int(cut_ratio), row_num)

        def func(x):
            x['answer_speed_flag'] = x[Answer.ANSWER_VALUE] <= target_num
            return x

        rank_flag = data_frame.rank().apply(func, axis=1)
        return rank_flag['answer_speed_flag'].astype('bool')

    def get_monitor_duplication_flag(self, monitor_duplication_request,
                                     layout: ForSurveyResearchLayout, raw_data_file: File):
        column_no_list = []

        if monitor_duplication_request['user_agent']:
            column_no_list.append(layout.get_user_agent().column_no - 1)
        if monitor_duplication_request['ip_address']:
            column_no_list.append(layout.get_ip_address().column_no - 1)
        if monitor_duplication_request['sex']:
            column_no_list.append(layout.get_sex().column_no - 1)
        if monitor_duplication_request['age']:
            column_no_list.append(layout.get_age().column_no - 1)
        if monitor_duplication_request['area']:
            column_no_list.append(layout.get_area().column_no - 1)

        data_frame = self._for_survey_raw_data_editor.get_data_frame(
            column_no_list, raw_data_file)
        return data_frame.duplicated()

    @staticmethod
    def _calc_cut_num(cut_ratio, row_num):
        return row_num * cut_ratio / 100
