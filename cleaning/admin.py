from django.contrib import admin

from cleaning.models import Cleaning
from cleaning.models import FileGroup
from cleaning.models import UploadedFile


admin.site.register(Cleaning)
admin.site.register(FileGroup)
admin.site.register(UploadedFile)
