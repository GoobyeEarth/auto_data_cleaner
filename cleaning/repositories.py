from cleaning.models import Cleaning
from cleaning.models import FileGroup
from cleaning.models import UploadedFile


class CleaningRepository(object):
    @staticmethod
    def create() -> Cleaning:
        return Cleaning.objects.create()

    @staticmethod
    def save(cleaning: Cleaning):
        cleaning.save()


class FileGroupRepository(object):
    @staticmethod
    def create(cleaning: Cleaning) -> FileGroup:
        return FileGroup.objects.create(cleaning=cleaning)

    @staticmethod
    def save(file_group: FileGroup):
        file_group.save()


class UploadedFileRepository(object):
    @staticmethod
    def create(file_group: FileGroup) -> UploadedFile:
        return UploadedFile.objects.create(file_group=file_group)

    @staticmethod
    def save(uploaded_file: UploadedFile):
        uploaded_file.save()
