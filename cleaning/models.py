from django.db import models


class CleaningStatus(object):
    NEW = 'NEW'
    UPLOADED = 'UPLOADED'
    CLEANING_ENDED = 'CLEANING_ENDED'
    choices = (
        (NEW, NEW),
        (UPLOADED, UPLOADED),
        (CLEANING_ENDED, CLEANING_ENDED),
    )


class Encode(object):
    SHIFT_JIS = 'shift-jis'
    UTF_8 = 'utf-8'
    choices = (
        (SHIFT_JIS, SHIFT_JIS),
        (UTF_8, UTF_8)
    )


class FilesTypes(object):
    FOR_SURVEY = 'forSurvey'
    choices = (
        (FOR_SURVEY, FOR_SURVEY),
    )


class FileTypes(object):
    RAW_DATA = 'RAW_DATA'
    LAYOUT = 'LAYOUT'
    choices = (
        (RAW_DATA, RAW_DATA),
    )


class Cleaning(models.Model):
    status = models.CharField(
        choices=CleaningStatus.choices, max_length=16, default=CleaningStatus.NEW)


class FileGroup(models.Model):
    files_type = models.CharField(
        choices=FilesTypes.choices, max_length=16, default='')
    cleaning = models.ForeignKey(Cleaning)


class UploadedFile(models.Model):
    encode = models.CharField(choices=Encode.choices,
                              max_length=32, default='')
    file_type = models.CharField(
        choices=FileTypes.choices, max_length=32, default='')
    file = models.FileField(upload_to='files', default=None)
    name = models.CharField(max_length=64, default='')
    file_group = models.ForeignKey(FileGroup)
