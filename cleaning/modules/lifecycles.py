from typing import Dict

from django.core.files.uploadedfile import File

from cleaning.models import CleaningStatus
from cleaning.models import FileTypes
from cleaning.modules.files.format.for_survey.layout import decide_char_encode
from cleaning.repositories import CleaningRepository
from cleaning.repositories import FileGroupRepository
from cleaning.repositories import UploadedFileRepository


class CleaningLifeCycle(object):
    @staticmethod
    def upload_target_files(file_type: str, uploaded_files: Dict[str, File]) -> int:
        """
        :param file_type:
        :param uploaded_files:
        :return: cleaning.id
        """
        cleaning = CleaningRepository.create()
        cleaning.status = CleaningStatus.NEW
        CleaningRepository.save(cleaning)

        file_group = FileGroupRepository.create(cleaning)
        file_group.files_type = file_type
        FileGroupRepository.save(file_group)

        encode = decide_char_encode(uploaded_files['layout'])

        layout_file = UploadedFileRepository.create(file_group)
        layout_file.file_type = FileTypes.LAYOUT
        layout_file.name = uploaded_files['layout'].name
        layout_file.file = uploaded_files['layout']
        layout_file.encode = encode
        UploadedFileRepository.save(layout_file)

        raw_data_file = UploadedFileRepository.create(file_group)
        raw_data_file.file_type = FileTypes.RAW_DATA
        raw_data_file.name = uploaded_files['raw_data'].name
        raw_data_file.file = uploaded_files['raw_data']
        raw_data_file.encode = encode
        UploadedFileRepository.save(raw_data_file)

        return cleaning.id
