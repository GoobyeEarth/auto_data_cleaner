class UniqueAnswersQuestionNo(object):
    NO = 'NO'
    MID = 'MID'
    RMID = 'RMID'
    START = 'START'
    END = 'END'
    TIME = 'TIME'
    STA = 'STA'
    GATE = 'GATE'
    QUO = 'QUO'
    USER_AGENT = 'UserAgent'
    APPID = 'APPID'
    IP_ADDRESS = 'IPAddress'
    DISPLAYED_ORDER = '設問表示順'
    KEY = 'key'

    def is_footer(self, question_no):
        return question_no == self.STA \
            or question_no == self.DISPLAYED_ORDER \
            or question_no == self.GATE \
            or question_no == self.QUO \
            or question_no == self.USER_AGENT \
            or question_no == self.APPID \
            or question_no == self.IP_ADDRESS \
            or question_no == self.KEY
