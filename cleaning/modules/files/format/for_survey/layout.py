import csv

import chardet
import codecs
from django.core.files import File

from cleaning.modules.files.format.for_survey.attributes import AttributesConverter
from cleaning.modules.files.format.for_survey.column_headers import LayoutColumnLabel as L
from cleaning.modules.files.format.for_survey.question_converters.index import QuestionConverter
from cleaning.modules.files.format.for_survey.research_result import ForSurveyResearchLayout
from cleaning.modules.files.format.for_survey.unique_answers_question_no import \
    UniqueAnswersQuestionNo
from cleaning.modules.files.format.for_survey.uniques_answer_column import \
    UniqueAnswerColumnConverter


class ForSurveyLayoutEditor(object):
    def __init__(self):
        self.__reader = None
        self.__question_array_converter = QuestionConverter()
        self.__unique_answers_converter = UniqueAnswerColumnConverter()
        self.encode: str = None

    def set_encode(self, encode: str):
        self.encode = encode

    def load(self, layout_file: File) -> ForSurveyResearchLayout:
        reader = self.__make_reader_from_file(layout_file)

        self.__reader: csv.reader = csv.reader(
            reader, delimiter='\t')

        research_result = self.__load_headers()
        research_result.file_path = layout_file.name
        questions_array = self.__load_questions_array()

        questions_headers = self.__unique_answers_converter.convert_array_to_entity_from_headers(
            questions_array)
        questions_start = questions_headers['questions_start']
        research_result.default_answers = questions_headers['header_answer_columns']

        questions_end = 0
        for current_count in range(questions_start, questions_array.__len__()):
            question_array = questions_array[current_count]
            if UniqueAnswersQuestionNo().is_footer(question_array[0][L.QUESTION_NO]):
                questions_end = current_count
                break
            research_result.questions.append(
                self.__question_array_converter.convert_array_to_entity(question_array))

        footers = self.__unique_answers_converter.convert_array_to_entity_from_footers(
            questions_array, questions_end)

        attribute_start = footers['attribute_start']
        research_result.default_answers.update(
            footers['footer_answer_columns'])

        research_result.attribute_answers = AttributesConverter().convert_array_to_entity(
            questions_array, attribute_start)

        return research_result

    def __make_reader_from_file(self, layout_file: File):
        return codecs.EncodedFile(layout_file, self.encode).reader

    def __load_headers(self) -> ForSurveyResearchLayout:
        first_row = next(self.__reader)
        for_survey_layout = ForSurveyResearchLayout()
        for_survey_layout.survey_title = first_row[3]

        # second_row = \
        next(self.__reader)

        return for_survey_layout

    def __load_questions_array(self):
        rows_divided_by_question = []
        questions_array = []

        rows_divided_by_question.append(next(self.__reader))

        for row in self.__reader:
            if row[0] == '':
                rows_divided_by_question.append(row)
            else:
                questions_array.append(rows_divided_by_question)
                rows_divided_by_question = []
                rows_divided_by_question.append(row)

        if rows_divided_by_question.__len__() != 0:
            questions_array.append(rows_divided_by_question)

        return questions_array


def decide_char_encode(layout_file: File):
    content = layout_file.read()
    result = chardet.detect(content)
    return result['encoding']
