from typing import Callable
from typing import Dict
from typing import List  # NOQA

from cleaning.modules.definitions.answer_column import AnswerColumn
from cleaning.modules.definitions.answer_column import PeriodAnswerColumn
from cleaning.modules.definitions.answer_column import SingleSelectedAnswerColumn
from cleaning.modules.definitions.answer_column import TextAnswerColumn
from cleaning.modules.definitions.defaultanswers import DefaultAnswers
from cleaning.modules.definitions.questions import Question
from cleaning.modules.files.format.for_survey.attributes import AttributesQuestionNo


class ForSurveyResearchLayout(object):
    def __init__(self):
        self.survey_title: str or None = None
        self.default_answers: Dict[str, AnswerColumn] = {}
        self.attribute_answers: List[AnswerColumn] = []
        self.questions: List[Question] = []
        self.file_path: str = ''

    def filter_answers(self, is_target: Callable[[Question, AnswerColumn], bool]) \
            -> Dict[int, AnswerColumn]:
        answers_dict: Dict[int, AnswerColumn] = {}
        for question in self.questions:
            for answers in question.get_answers_list():
                if is_target(question, answers):
                    answers_dict[answers.column_no] = answers
        return answers_dict

    def get_time(self) -> PeriodAnswerColumn:
        answer_column: PeriodAnswerColumn = self._get_from_default_answers(
            DefaultAnswers.ANSWER_TIME)
        return answer_column

    def get_user_agent(self) -> TextAnswerColumn:
        answer_column: TextAnswerColumn = self._get_from_default_answers(
            DefaultAnswers.USER_AGENT)
        return answer_column

    def get_ip_address(self) -> TextAnswerColumn:
        answer_column: TextAnswerColumn = self._get_from_default_answers(
            DefaultAnswers.IP_ADDRESS)
        return answer_column

    def get_sex(self) -> SingleSelectedAnswerColumn:
        answers_list: List = self._get_from_attributes(
            AttributesQuestionNo.SEX).get_answers_list()
        if answers_list.__len__() == 1:
            return answers_list[0]

        raise Exception(AttributesQuestionNo.SEX +
                        ' is not single : '.answers_list.__len__())

    def get_age(self) -> SingleSelectedAnswerColumn:
        answers_list: List = self._get_from_attributes(
            AttributesQuestionNo.AGE).get_answers_list()
        if answers_list.__len__() == 1:
            return answers_list[0]

        raise Exception(AttributesQuestionNo.AGE +
                        ' is not single : '.answers_list.__len__())

    def get_area(self) -> SingleSelectedAnswerColumn:
        answers_list: List = self._get_from_attributes(
            AttributesQuestionNo.PRE).get_answers_list()
        if answers_list.__len__() == 1:
            return answers_list[0]

        raise Exception(AttributesQuestionNo.PRE +
                        ' is not single : '.answers_list.__len__())

    def _get_from_default_answers(self, key: str) -> AnswerColumn:
        if key in self.default_answers.keys():
            return self.default_answers[key]

        raise Exception(key + ' not found')

    def _get_from_attributes(self, question_no: str) -> AnswerColumn:
        answer_columns = list(
            filter(lambda x: x.question_no == question_no, self.attribute_answers))

        if answer_columns.__len__() == 1:
            return answer_columns[0]
        raise Exception(question_no + ' not found')
