from typing import Dict  # NOQA
from typing import List

from cleaning.modules.definitions.answer_column import DateTimeAnswerColumn
from cleaning.modules.definitions.answer_column import MultiSelectedAnswerColumn
from cleaning.modules.definitions.answer_column import PeriodAnswerColumn
from cleaning.modules.definitions.answer_column import SingleSelectedAnswerColumn
from cleaning.modules.definitions.answer_column import TextAnswerColumn
from cleaning.modules.definitions.defaultanswers import DefaultAnswers
from cleaning.modules.files.format.exception import FormatException
from cleaning.modules.files.format.for_survey.answer_column_converters.sub_text import \
    SubTextAnswersConverter
from cleaning.modules.files.format.for_survey.column_headers import LayoutColumnLabel as L
from cleaning.modules.files.format.for_survey.question_converters.single_selected import \
    SingleSelectedQuestionConverter
from cleaning.modules.files.format.for_survey.unique_answers_question_no import \
    UniqueAnswersQuestionNo


class AnswersFormatLabels(object):
    SA = 'SA'
    MA = 'MA'
    FA = 'FA'
    BLANK = ''


class MatrixesAnswersConverter(object):
    def __init__(self):
        self.__sub_text_answers_converter = SubTextAnswersConverter()

    @staticmethod
    def divide_by_answers_array(question_array: List[List[str]]) -> List[List[List[str]]]:
        answers_array_list: List[List[List[str]]] = []
        answers_array: List[List[str]] = []
        for count in range(1, question_array.__len__()):
            row = question_array[count]
            if row[L.ITEM_NAME] != '':
                answers_array_list.append(answers_array)
                answers_array = []
            answers_array.append(row)

        if answers_array.__len__() != 0:
            answers_array_list.append(answers_array)
        return answers_array_list[1:]

    @staticmethod
    def convert_single_selected_array_to_entity(
            answers_array: List[List[str]]) -> SingleSelectedAnswerColumn:
        item_name = answers_array[0][L.ITEM_NAME]
        column_header = answers_array[0][L.COLUMN_HEADER]
        choice_labels: Dict[int, str] = {}
        for row in answers_array[1:]:
            choice_no = int(row[L.CHOICE_NO])
            choice_labels[choice_no] = row[L.TITLE_CHOICE_LABEL]
        return SingleSelectedAnswerColumn(item_name, column_header,
                                          int(answers_array[0][L.COLUMN_NO]), choice_labels)

    @staticmethod
    def convert_multi_selected_array_to_entity(
            answers_array: List[List[str]]) -> MultiSelectedAnswerColumn:
        item_name = answers_array[0][L.ITEM_NAME]
        column_header: Dict[int, str] = {}
        column_no: Dict[int, int] = {}
        choice_labels: Dict[int, str] = {}
        for row in answers_array[1:]:
            choice_no = int(row[L.CHOICE_NO])
            choice_labels[choice_no] = row[L.TITLE_CHOICE_LABEL]
            column_header[choice_no] = row[L.COLUMN_HEADER]
            column_no[choice_no] = row[L.COLUMN_NO]
        return MultiSelectedAnswerColumn(item_name, column_header, column_no, choice_labels)

    def convert_sub_text_answered_array_to_entity(self,
                                                  answers_array: List[
                                                      List[str]]) -> TextAnswerColumn:
        if answers_array.__len__() != 1:
            raise FormatException(
                'text_answered_array expected 1 row  answers_array:' + str(answers_array))
        return self.__sub_text_answers_converter.convert_array_to_entity(answers_array[0])

    @staticmethod
    def convert_text_answered_array_to_entity(answers_array: List[List[str]]) \
            -> List[TextAnswerColumn]:
        item_name = answers_array[0][L.ITEM_NAME]

        text_answers_list = []
        for row in answers_array[1:]:
            answer_column = TextAnswerColumn(item_name=item_name,
                                             column_header=row[L.COLUMN_HEADER],
                                             column_no=int(row[L.COLUMN_NO]))
            text_answers_list.append(answer_column)
        return text_answers_list

    @staticmethod
    def is_single_selected_answers(answers_array: List[List[str]]) -> bool:
        return answers_array[0][L.ANSWER_TYPE] == AnswersFormatLabels.SA

    @staticmethod
    def is_multi_selected_answers(answers_array: List[List[str]]) -> bool:
        """NOT TESTED."""
        return answers_array[0][L.ANSWER_TYPE] == AnswersFormatLabels.MA

    @staticmethod
    def is_sub_text_answers(answers_array: List[List[str]]) -> bool:
        """NOT TESTED."""
        return answers_array[0][L.ANSWER_TYPE] == AnswersFormatLabels.FA

    @staticmethod
    def is_text_answers(answers_array: List[List[str]]) -> bool:
        """NOT TESTED."""
        return answers_array[0][L.ANSWER_TYPE] == AnswersFormatLabels.BLANK


class DefaultAnswerColumnConverter(object):
    """ここに書かれているが、layoutから直呼びする。"""

    @staticmethod
    def convert_array_to_entity(question_array):
        if question_array[0][L.QUESTION_NO] == UniqueAnswersQuestionNo.START:
            start_answer_column = DateTimeAnswerColumn(
                item_name=question_array[0][L.ITEM_NAME],
                column_header=question_array[0][L.COLUMN_HEADER],
                column_no=int(question_array[0][L.COLUMN_NO]),
            )
            return DefaultAnswers.START_DATETIME, start_answer_column
        if question_array[0][L.QUESTION_NO] == UniqueAnswersQuestionNo.END:
            end_answer_column = DateTimeAnswerColumn(
                item_name=question_array[0][L.ITEM_NAME],
                column_header=question_array[0][L.COLUMN_HEADER],
                column_no=int(question_array[0][L.COLUMN_NO]),
            )
            return DefaultAnswers.END_DATETIME, end_answer_column
        if question_array[0][L.QUESTION_NO] == UniqueAnswersQuestionNo.TIME:
            time_answer_column = PeriodAnswerColumn(
                item_name=question_array[0][L.ITEM_NAME],
                column_header=question_array[0][L.COLUMN_HEADER],
                column_no=int(question_array[0][L.COLUMN_NO]),
            )
            return DefaultAnswers.ANSWER_TIME, time_answer_column
        if question_array[0][L.QUESTION_NO] == UniqueAnswersQuestionNo.STA:
            sta_answer_column = TextAnswerColumn(
                item_name=question_array[0][L.ITEM_NAME],
                column_header=question_array[0][L.COLUMN_HEADER],
                column_no=int(question_array[0][L.COLUMN_NO]),
            )
            return DefaultAnswers.STATUS, sta_answer_column
        if question_array[0][L.QUESTION_NO] == UniqueAnswersQuestionNo.GATE:
            quo_answer_column = SingleSelectedQuestionConverter().convert_array_to_entity(
                question_array).get_answers_list()[0]
            return DefaultAnswers.GATE, quo_answer_column
        if question_array[0][L.QUESTION_NO] == UniqueAnswersQuestionNo.QUO:
            quo_answer_column = SingleSelectedQuestionConverter().convert_array_to_entity(
                question_array).get_answers_list()[0]
            return DefaultAnswers.QUO, quo_answer_column
        if question_array[0][L.QUESTION_NO] == UniqueAnswersQuestionNo.USER_AGENT:
            ua_answer_column = TextAnswerColumn(
                item_name=question_array[0][L.ITEM_NAME],
                column_header=question_array[0][L.COLUMN_HEADER],
                column_no=int(question_array[0][L.COLUMN_NO]),
            )
            return DefaultAnswers.USER_AGENT, ua_answer_column
        if question_array[0][L.QUESTION_NO] == UniqueAnswersQuestionNo.APPID:
            appid_answer_column = TextAnswerColumn(
                item_name=question_array[0][L.ITEM_NAME],
                column_header=question_array[0][L.COLUMN_HEADER],
                column_no=int(question_array[0][L.COLUMN_NO]),
            )
            return DefaultAnswers.APP_ID, appid_answer_column
        if question_array[0][L.QUESTION_NO] == UniqueAnswersQuestionNo.IP_ADDRESS:
            ip_address_answer_column = TextAnswerColumn(
                item_name=question_array[0][L.ITEM_NAME],
                column_header=question_array[0][L.COLUMN_HEADER],
                column_no=int(question_array[0][L.COLUMN_NO]),
            )
            return DefaultAnswers.IP_ADDRESS, ip_address_answer_column

        if question_array[0][L.QUESTION_NO] == UniqueAnswersQuestionNo.KEY:
            key_answer_column = TextAnswerColumn(
                item_name=question_array[0][L.ITEM_NAME],
                column_header=question_array[0][L.COLUMN_HEADER],
                column_no=int(question_array[0][L.COLUMN_NO]),
            )
            return DefaultAnswers.KEY, key_answer_column
