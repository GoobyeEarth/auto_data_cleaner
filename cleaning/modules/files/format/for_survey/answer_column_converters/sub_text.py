from typing import List

from cleaning.modules.definitions.answer_column import TextAnswerColumn
from cleaning.modules.files.format.for_survey.column_headers import LayoutColumnLabel as L


class SubTextAnswersConverter(object):
    @staticmethod
    def convert_array_to_entity(layout_row: List[str]) -> TextAnswerColumn:
        return TextAnswerColumn(item_name=layout_row[L.ITEM_NAME],
                                column_header=layout_row[L.COLUMN_HEADER],
                                column_no=int(layout_row[L.COLUMN_NO]))
