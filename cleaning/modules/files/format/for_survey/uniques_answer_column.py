from typing import List  # NOQA
from typing import Dict  # NOQA

from cleaning.modules.definitions.answer_column import AnswerColumn  # NOQA
from cleaning.modules.definitions.answer_column import TextAnswerColumn
from cleaning.modules.definitions.defaultanswers import DefaultAnswers
from cleaning.modules.files.format.exception import FormatException
from cleaning.modules.files.format.for_survey.answers_converter import DefaultAnswerColumnConverter
from cleaning.modules.files.format.for_survey.column_headers import LayoutColumnLabel as L
from cleaning.modules.files.format.for_survey.question_converters.index import AnswerTypes
from cleaning.modules.files.format.for_survey.unique_answers_question_no import \
    UniqueAnswersQuestionNo


class UniqueAnswerColumnConverter(object):
    def convert_array_to_entity_from_headers(self, layout_array: List[List[List[str]]]):
        headers_answers: Dict[str, any] = {}
        start = 0
        expected_no = layout_array[0]
        if expected_no[0][L.QUESTION_NO] == UniqueAnswersQuestionNo.NO:
            no_answer_column = TextAnswerColumn(
                item_name=expected_no[0][L.ITEM_NAME],
                column_header=expected_no[0][L.COLUMN_HEADER],
                column_no=int(expected_no[0][L.COLUMN_NO]),
            )
            headers_answers[DefaultAnswers.MONITOR_ID] = no_answer_column
            start = 1
        elif expected_no[0][L.QUESTION_NO] == UniqueAnswersQuestionNo.RMID:
            no_answer_column = TextAnswerColumn(
                item_name=expected_no[0][L.ITEM_NAME],
                column_header=expected_no[0][L.COLUMN_HEADER],
                column_no=int(expected_no[0][L.COLUMN_NO]),
            )
            headers_answers[DefaultAnswers.MONITOR_ID] = no_answer_column

            expected_rmid = layout_array[1]
            no_answer_column = TextAnswerColumn(
                item_name=expected_rmid[0][L.ITEM_NAME],
                column_header=expected_rmid[0][L.COLUMN_HEADER],
                column_no=int(expected_rmid[0][L.COLUMN_NO]),
            )
            headers_answers[DefaultAnswers.MONITOR_CODE] = no_answer_column
            start = 2
        else:
            raise FormatException('unique question MONITOR_ID is not found')

        questions_start = 0
        for count in range(start, layout_array.__len__()):
            question_array = layout_array[count]
            if question_array[0][L.ANSWER_TYPE] != AnswerTypes.D:
                questions_start = count
                break
            key, answer_column = DefaultAnswerColumnConverter().convert_array_to_entity(
                question_array)
            headers_answers[key] = answer_column

        return {'questions_start': questions_start,
                'header_answer_columns': headers_answers}

    def convert_array_to_entity_from_footers(self, layout_array: List[List[List[str]]],
                                             questions_end: int):
        footer_answer_columns = {}
        attribute_start = 0
        for count in range(questions_end, layout_array.__len__()):
            question_array = layout_array[count]
            if not UniqueAnswersQuestionNo().is_footer(question_array[0][L.QUESTION_NO]):
                attribute_start = count
                return {'attribute_start': attribute_start,
                        'footer_answer_columns': footer_answer_columns}
            key, answer_column = DefaultAnswerColumnConverter().convert_array_to_entity(
                question_array)
            footer_answer_columns[key] = answer_column

        return {'attribute_start': layout_array.__len__(),
                'footer_answer_columns': footer_answer_columns}
