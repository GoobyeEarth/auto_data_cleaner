from typing import List

from cleaning.modules.files.format.for_survey.question_converters.index import QuestionConverter


class AttributesConverter(object):
    def convert_array_to_entity(self, layout_array: List[List[List[str]]], attributes_start: int):
        attributes = []
        for question_array in layout_array[attributes_start:]:
            attributes.append(
                QuestionConverter().convert_array_to_entity(question_array))
        return attributes


class AttributesQuestionNo(object):
    SEX = 'SEX'
    AGE = 'AGE'
    PRE = 'PRE'
