import codecs
import csv
import io
from typing import Dict
from typing import List
from django.conf import settings

from django.core.files import File
import pandas as pd

from cleaning.modules.definitions.answer import AnswerColumnDictLabel as Answer


class AppendedFlagName(object):
    FA_FLAG = 'FA_FLAG'


class ForSurveyRawDataEditor(object):
    def __init__(self):
        self.__reader = None
        self.encode: str = None

    def set_encode(self, encode: str):
        self.encode = encode

    def get_all_answer_dict(self, column_no_list: List[int], raw_data_file: File):
        reader: csv.reader = self.__make_reader_from_file(raw_data_file)
        reader.seek(0)

        self.__reader: csv.reader = csv.reader(
            reader, delimiter='\t')
        next(self.__reader)  # read headers but not used
        all_answers = {}
        for column_no in column_no_list:
            all_answers[column_no] = {}

        for row in self.__reader:
            for column_no in column_no_list:
                all_answers[column_no][row[0]] = row[column_no - 1]
        return all_answers

    def get_all_answer_list(self, column_no_list: List[int], raw_data_file: File) -> Dict[
            int, List[any]]:
        reader = self.__make_reader_from_file(raw_data_file)
        reader.seek(0)

        self.__reader: csv.reader = csv.reader(
            reader, delimiter='\t')
        next(self.__reader)  # read headers but not used

        all_answers = {}
        for column_no in column_no_list:
            all_answers[column_no] = []

        for row in self.__reader:
            for column_no in column_no_list:
                all_answers[column_no].append(row[column_no - 1])

        return all_answers

    def get_all_answer_data_frame(self, column_no_list: List[int], raw_data_file: File) \
            -> Dict[str, pd.DataFrame]:

        reader = self.__make_reader_from_file(raw_data_file)
        reader.seek(0)

        self.__reader: csv.reader = csv.reader(
            reader, delimiter='\t')
        headers = next(self.__reader)

        answer_list = {}
        for column_no in column_no_list:
            answer_list[headers[column_no - 1]] = []

        for row in self.__reader:
            for column_no in column_no_list:
                answer_list[headers[column_no - 1]].append(row[column_no - 1])

        data_frames = {}
        for column_header, answer_values in answer_list.items():
            data_frame = pd.DataFrame([answer_values]).T
            data_frame.columns = [Answer.ANSWER_VALUE]
            data_frames[column_header] = data_frame

        return data_frames

    def get_data_frame(self, column_no_list: List[int], raw_data_file: File)-> pd.DataFrame:
        df = pd.read_table(self._get_file_path(raw_data_file),
                           encoding=self.encode, usecols=column_no_list)
        return df

    @staticmethod
    def _get_file_path(file: File):
        return settings.MEDIA_ROOT+ '/' + file.name

    def get_string_io(self, flags: Dict[int, bool], raw_data_file: File,
                      append_or_delete_flag: str):
        reader = self.__make_reader_from_file(raw_data_file)
        reader.seek(0)

        csv_reader: csv.reader = csv.reader(reader, delimiter='\t')

        if append_or_delete_flag == 'true':
            header = next(csv_reader)

            header.append(AppendedFlagName.FA_FLAG)
            str_io = io.StringIO()
            writer = csv.writer(str_io, delimiter='\t')
            writer.writerow(header)

            for ans_row_count in range(flags.__len__()):
                row = next(csv_reader)

                # 列が足りなかったら足してからフラグを追加する。
                if row.__len__() < header.__len__() - 1:
                    for count in range(header.__len__() - row.__len__ - 1):
                        row.append('')

                flag_string = '1' if flags[ans_row_count] else '0'
                row.append(flag_string)

                writer.writerow(row)

        else:
            header = next(csv_reader)
            str_io = io.StringIO()
            writer = csv.writer(str_io, delimiter='\t')
            writer.writerow(header)
            for ans_row_count in range(flags.__len__()):

                row = next(csv_reader)
                if flags[ans_row_count] is False:
                    writer.writerow(row)

        return str_io

    def __make_reader_from_file(self, raw_data_file: File):
        return codecs.EncodedFile(raw_data_file, self.encode).reader
