from typing import Dict
from typing import List

from cleaning.modules.definitions.questions import MatrixMultiSelectedQuestion
from cleaning.modules.definitions.questions import MatrixQuestion
from cleaning.modules.definitions.questions import MatrixSingleSelectedQuestion
from cleaning.modules.definitions.questions import MultiSelectedQuestion
from cleaning.modules.definitions.questions import NumberAnsweredQuestion
from cleaning.modules.definitions.questions import Question
from cleaning.modules.definitions.questions import SingleSelectedQuestion
from cleaning.modules.definitions.questions import TextAnsweredQuestion
from cleaning.modules.files.format.exception import FormatException
from cleaning.modules.files.format.for_survey.column_headers import LayoutColumnLabel as L
from cleaning.modules.files.format.for_survey.question_converters.matrix import \
    MatrixQuestionConverter
from cleaning.modules.files.format.for_survey.question_converters.matrix_multi_selected import \
    MatrixMultiSelectedQuestionConverter
from cleaning.modules.files.format.for_survey.question_converters.matrix_single_selected import \
    MatrixSingleSelectedQuestionConverter
from cleaning.modules.files.format.for_survey.question_converters.multi_selected import \
    MultiSelectedQuestionConverter
from cleaning.modules.files.format.for_survey.question_converters.number_answered import \
    NumberAnsweredQuestionConverter
from cleaning.modules.files.format.for_survey.question_converters.single_selected import \
    SingleSelectedQuestionConverter
from cleaning.modules.files.format.for_survey.question_converters.text_answered import \
    TextAnsweredQuestionConverter


class AnswerTypes(object):
    SA = 'S'
    MA = 'M'
    FS = 'FS'
    FL = 'F'
    NUM = 'NUM'
    MTS = 'MTS'
    MTM = 'MTM'
    MTX = 'MTX'
    D = 'D'

    def get(self):
        return [
            self.SA,
            self.MA,
            self.FS,
            self.FL,
            self.NUM,
            self.MTS,
            self.MTM,
            self.MTX,
        ]


class QuestionConverter(object):
    def __init__(self):
        self.__sa_converter = SingleSelectedQuestionConverter()
        self.__ma_converter = MultiSelectedQuestionConverter()
        self.__fa_converter = TextAnsweredQuestionConverter()
        self.__nf_converter = NumberAnsweredQuestionConverter()
        self._mts_converter = MatrixSingleSelectedQuestionConverter()
        self._mtm_converter = MatrixMultiSelectedQuestionConverter()
        self._mtx_converter = MatrixQuestionConverter()

    QUESTION_TYPE_MAP: Dict[str, Question] = {
        AnswerTypes.SA: SingleSelectedQuestion,
        AnswerTypes.MA: MultiSelectedQuestion,
        AnswerTypes.FS: TextAnsweredQuestion,
        AnswerTypes.FL: TextAnsweredQuestion,
        AnswerTypes.NUM: NumberAnsweredQuestion,
        AnswerTypes.MTS: MatrixSingleSelectedQuestion,
        AnswerTypes.MTM: MatrixMultiSelectedQuestion,
        AnswerTypes.MTX: MatrixQuestion,
    }

    def convert_array_to_entity(self, question_array: List[List[str]]) -> Question:
        answer_type = question_array[0][L.QUESTION_TYPE]
        if answer_type not in AnswerTypes().get():
            raise FormatException(
                answer_type + ' does not describe question_type')

        if answer_type == AnswerTypes.SA:
            return self.__sa_converter.convert_array_to_entity(question_array)
        elif answer_type == AnswerTypes.MA:
            return self.__ma_converter.convert_array_to_entity(question_array)
        elif answer_type == AnswerTypes.FS:
            return self.__fa_converter.convert_array_to_entity(question_array)
        elif answer_type == AnswerTypes.FL:
            return self.__fa_converter.convert_array_to_entity(question_array)
        elif answer_type == AnswerTypes.NUM:
            return self.__nf_converter.convert_array_to_entity(question_array)
        elif answer_type == AnswerTypes.MTS:
            return self._mts_converter.convert_array_to_entity(question_array)
        elif answer_type == AnswerTypes.MTM:
            return self._mtm_converter.convert_array_to_entity(question_array)
        elif answer_type == AnswerTypes.MTX:
            return self._mtx_converter.convert_array_to_entity(question_array)
