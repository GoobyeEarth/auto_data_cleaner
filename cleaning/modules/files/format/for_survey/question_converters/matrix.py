from typing import List

from cleaning.modules.definitions.questions import MatrixQuestion
from cleaning.modules.files.format.exception import FormatException
from cleaning.modules.files.format.for_survey.answers_converter import MatrixesAnswersConverter
from cleaning.modules.files.format.for_survey.column_headers import LayoutColumnLabel as L


class MatrixQuestionConverter(object):
    def __init__(self):
        self.__matrixes_answers_converter = MatrixesAnswersConverter()

    def convert_array_to_entity(self, question_array: List[List[str]]) -> MatrixQuestion:
        question = MatrixQuestion(question_array[0][L.TITLE_CHOICE_LABEL],
                                  question_array[0][L.QUESTION_TYPE])

        answers_array_list = self.__matrixes_answers_converter.divide_by_answers_array(
            question_array)

        for answers_array in answers_array_list:
            question = self.__convert_answers_array_to_entity(
                question, answers_array)
        return question

    def __convert_answers_array_to_entity(self, question: MatrixQuestion,
                                          answers_array: List[List[str]]) -> MatrixQuestion:
        """NOT TESTED."""
        if self.__matrixes_answers_converter.is_single_selected_answers(answers_array):
            question.add_answers(
                self.__matrixes_answers_converter.convert_single_selected_array_to_entity(
                    answers_array))
        elif self.__matrixes_answers_converter.is_multi_selected_answers(answers_array):
            question.add_answers(
                self.__matrixes_answers_converter.convert_multi_selected_array_to_entity(
                    answers_array))
        elif self.__matrixes_answers_converter.is_sub_text_answers(answers_array):
            question.add_answers(
                self.__matrixes_answers_converter.convert_sub_text_answered_array_to_entity(
                    answers_array))
        elif self.__matrixes_answers_converter.is_text_answers(answers_array):
            question.add_answers_list(
                self.__matrixes_answers_converter.convert_text_answered_array_to_entity(
                    answers_array))
        else:

            raise FormatException(
                'unexpected answers_array: ' + str(answers_array))
        return question
