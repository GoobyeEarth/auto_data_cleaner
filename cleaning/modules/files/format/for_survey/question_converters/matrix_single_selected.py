from typing import List

from cleaning.modules.definitions.questions import MatrixSingleSelectedQuestion

from cleaning.modules.definitions.answer_column import AnswerColumn
from cleaning.modules.files.format.exception import FormatException

from cleaning.modules.files.format.for_survey.answers_converter import MatrixesAnswersConverter
from cleaning.modules.files.format.for_survey.column_headers import LayoutColumnLabel as L


class MatrixSingleSelectedQuestionConverter(object):
    def __init__(self):
        self.__matrixes_answers_converter = MatrixesAnswersConverter()

    def convert_array_to_entity(self,
                                question_array: List[List[str]]) -> MatrixSingleSelectedQuestion:
        question = MatrixSingleSelectedQuestion(
            question_array[0][L.TITLE_CHOICE_LABEL], question_array[0][L.QUESTION_NO])

        answers_array_list = self.__matrixes_answers_converter.divide_by_answers_array(
            question_array)
        for answers_array in answers_array_list:
            answers = self.__convert_answers_array_to_entity(answers_array)
            question.add_answers(answers)
        return question

    def __convert_answers_array_to_entity(self, answers_array: List[List[str]]) -> AnswerColumn:
        """NOT TESTED."""
        if self.__matrixes_answers_converter.is_single_selected_answers(answers_array):
            return self.__matrixes_answers_converter.convert_single_selected_array_to_entity(
                answers_array)
        if self.__matrixes_answers_converter.is_sub_text_answers(answers_array):
            return self.__matrixes_answers_converter.convert_sub_text_answered_array_to_entity(
                answers_array)
        else:
            raise FormatException(
                'unexpected answers_array: ' + str(answers_array))
