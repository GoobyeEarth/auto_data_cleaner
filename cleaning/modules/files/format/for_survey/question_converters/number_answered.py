from typing import List

from cleaning.modules.definitions.answer_column import NumberAnswerColumn
from cleaning.modules.definitions.questions import NumberAnsweredQuestion
from cleaning.modules.files.format.for_survey.column_headers import LayoutColumnLabel as L


class NumberAnsweredQuestionConverter(object):
    @staticmethod
    def convert_array_to_entity(question_array: List[List[str]]) -> NumberAnsweredQuestion:
        question = NumberAnsweredQuestion(
            question_array[0][L.TITLE_CHOICE_LABEL], question_array[0][L.QUESTION_NO])

        for row in question_array[1:]:
            number_answer_column = NumberAnswerColumn(
                item_name=row[L.ITEM_NAME],
                column_header=row[L.COLUMN_HEADER],
                column_no=int(row[L.COLUMN_NO]),
            )
            question.add_answers(number_answer_column)

        return question
