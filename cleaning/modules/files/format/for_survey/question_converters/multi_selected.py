from typing import Dict  # NOQA
from typing import List

from cleaning.modules.definitions.answer_column import MultiSelectedAnswerColumn
from cleaning.modules.definitions.questions import MultiSelectedQuestion
from cleaning.modules.files.format.for_survey.answer_column_converters.sub_text import \
    SubTextAnswersConverter
from cleaning.modules.files.format.for_survey.column_headers import LayoutColumnLabel as L


class MultiSelectedQuestionConverter(object):
    def __init__(self):
        self.__sub_text_answer_converter = SubTextAnswersConverter()

    def convert_array_to_entity(self, question_array: List[List[str]]) -> MultiSelectedQuestion:
        choice_labels: Dict[int, str] = {}
        column_headers: Dict[int, str] = {}
        column_no: Dict[int, int] = {}

        question = MultiSelectedQuestion(
            question_array[0][L.TITLE_CHOICE_LABEL], question_array[0][L.QUESTION_NO])

        answer_end_point: int = -1
        for count in range(1, question_array.__len__()):
            row = question_array[count]
            if row[2] != '':
                answer_end_point = count
                break
            choice_no = int(row[L.CHOICE_NO])
            choice_labels[choice_no] = row[L.TITLE_CHOICE_LABEL]
            column_headers[choice_no] = row[L.COLUMN_HEADER]
            column_no[choice_no] = int(row[L.COLUMN_NO])

        if answer_end_point == -1:
            answer_end_point = question_array.__len__()

        multi_selected_answer_column = MultiSelectedAnswerColumn(
            item_name=question_array[0][L.ITEM_NAME],
            column_header=column_headers,
            column_no=column_no,
            choice_labels=choice_labels)
        question.add_answers(multi_selected_answer_column)

        for count in range(answer_end_point, question_array.__len__()):
            text_answers = \
                self.__sub_text_answer_converter.convert_array_to_entity(
                    question_array[count])
            question.add_answers(text_answers)

        return question
