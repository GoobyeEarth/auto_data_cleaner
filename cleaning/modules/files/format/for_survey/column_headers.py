class LayoutColumnLabel(object):
    QUESTION_NO = 0  # 質問番号
    QUESTION_TYPE = 1  # 質問タイプ
    ITEM_NAME = 2  # アイテム名
    COLUMN_HEADER = 3  # ラベル
    ANSWER_TYPE = 4  # 回答タイプ
    CATEGORY_NUM = 5  # カテゴリ数
    COLUMN_NO = 6  # カラム
    CHOICE_NO = 7  # 選択肢番号
    TITLE_CHOICE_LABEL = 8  # 質問文／選択肢
    GROUP_CAPTION = 9  # 設問タイトル／選択肢グループキャプション
