import re
from typing import Dict

from pykakasi import kakasi


class TextAnswerFilter(object):
    TOO_LESS_WORDS = 'too_less_words'
    TOO_MATCH_CONSONANTS = 'too_match_consonants'
    REDUNDANT_WORD = 'redundant_word'
    JUDGEMENT = 'judgement'
    WORD_NUM = 'letter_num'

    @staticmethod
    def filter_too_less_words(answer_value: str) -> bool:
        return len(answer_value) == 1

    def filter_too_match_consonants(self, answer_value: str) -> bool:
        if answer_value.__len__() == 0:
            return False
        romaji_generator = KanjiToRomajiGenerator()
        romaji = romaji_generator.generate(answer_value)

        return 0.7 < self._calculate_consonants_rate(romaji)

    @staticmethod
    def filter_redundant_word(answer_value: str) -> bool:
        pattern = r"(.)\1{2,}"
        result = re.search(pattern, answer_value)
        return result is not None

    @staticmethod
    def _calculate_consonants_rate(romaji: str):
        romaji_count = len(romaji)
        vowel_count = len(list(
            filter(lambda letter: letter in (
                'a', 'i', 'u', 'e', 'o'), list(romaji))
        ))
        return (romaji_count - vowel_count) / romaji_count

    def judge_by_indexes(self, indexes: Dict[str, bool]) -> bool:
        result = False
        if self.TOO_LESS_WORDS in indexes:
            result = result or indexes[self.TOO_LESS_WORDS]
        if self.TOO_MATCH_CONSONANTS in indexes:
            result = result or indexes[self.TOO_MATCH_CONSONANTS]
        if self.REDUNDANT_WORD in indexes:
            result = result or indexes[self.REDUNDANT_WORD]
        return result


class KanjiToRomajiGenerator(object):
    @staticmethod
    def generate(text):
        k = kakasi()
        k.setMode('H', 'a')
        k.setMode('K', 'a')
        k.setMode('J', 'a')
        converter = k.getConverter()
        result = converter.do(text)
        return result
