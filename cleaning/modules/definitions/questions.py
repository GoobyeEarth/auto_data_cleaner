from abc import ABC
from typing import List

from cleaning.modules.definitions.answer_column import AnswerColumn
from cleaning.modules.definitions.answer_column import MultiSelectedAnswerColumn
from cleaning.modules.definitions.answer_column import NumberAnswerColumn
from cleaning.modules.definitions.answer_column import SingleSelectedAnswerColumn
from cleaning.modules.definitions.answer_column import TextAnswerColumn
from cleaning.modules.definitions.exceptions import DefinitionException


class Question(ABC):
    QUESTION_TYPE = ''
    AVAILABLE_ANSWERS_LIST = []

    def __init__(self, title: str, question_no: str):
        self.title = title
        self.question_no = question_no
        self._answers: List[AnswerColumn] = []

    def add_answers(self, answers: AnswerColumn):
        if self._is_answers_available(answers):
            self._answers.append(answers)
        else:
            raise DefinitionException(
                'ANSWERS_TYPE not allowed :' + answers.ANSWERS_TYPE)

    def get_answers_list(self):
        return self._answers

    def _is_answers_available(self, answers: AnswerColumn):
        for available_answers in self.AVAILABLE_ANSWERS_LIST:
            if available_answers.ANSWERS_TYPE == answers.ANSWERS_TYPE:
                return True

        return False


class SingleSelectedQuestion(Question):
    QUESTION_TYPE = 'SINGLE_SELECTED_QUESTION'
    AVAILABLE_ANSWERS_LIST = [
        SingleSelectedAnswerColumn,
        TextAnswerColumn,
        NumberAnswerColumn,
    ]


class MultiSelectedQuestion(Question):
    QUESTION_TYPE = 'MULTI_SELECTED_QUESTION'
    AVAILABLE_ANSWERS_LIST = [
        MultiSelectedAnswerColumn,
        TextAnswerColumn,
        NumberAnswerColumn,
    ]


class TextAnsweredQuestion(Question):
    QUESTION_TYPE = 'TEXT_ANSWERED_QUESTION'
    AVAILABLE_ANSWERS_LIST = [
        TextAnswerColumn,
    ]


class NumberAnsweredQuestion(Question):
    QUESTION_TYPE = 'NUMBER_ANSWERED_QUESTION'
    AVAILABLE_ANSWERS_LIST = [
        NumberAnswerColumn
    ]


class MatrixSingleSelectedQuestion(Question):
    QUESTION_TYPE = 'MATRIX_SINGLE_SELECTED_QUESTION'
    AVAILABLE_ANSWERS_LIST = [
        SingleSelectedAnswerColumn,
        TextAnswerColumn,
        NumberAnswerColumn,
    ]


class MatrixMultiSelectedQuestion(Question):
    QUESTION_TYPE = 'MATRIX_MULTI_SELECTED_QUESTION'
    AVAILABLE_ANSWERS_LIST = [
        MultiSelectedAnswerColumn,
        TextAnswerColumn,
        NumberAnswerColumn,
    ]


class MatrixQuestion(Question):
    QUESTION_TYPE = 'MATRIX_QUESTION'
    AVAILABLE_ANSWERS_LIST = [
        SingleSelectedAnswerColumn,
        MultiSelectedAnswerColumn,
        TextAnswerColumn,
        NumberAnswerColumn,
    ]

    def add_answers_list(self, answers_list: List[AnswerColumn]):
        """NOT TESTED."""
        for answers in answers_list:
            self.add_answers(answers)
