from abc import ABC
from abc import abstractmethod
from typing import Dict
from typing import List


class AnswerColumn(ABC):
    ANSWERS_TYPE = ''

    @abstractmethod
    def add_answer(self, monitor_id: str, answer):
        pass


class SingleSelectedAnswerColumn(AnswerColumn):
    ANSWERS_TYPE = 'SINGLE_SELECTED_ANSWERS'

    def __init__(self,
                 item_name: str,
                 column_header: str,
                 column_no: int,
                 choice_labels: Dict[int, str]):
        self.item_name: str = item_name
        self.column_header: str = column_header
        self.column_no = column_no
        self._answers: Dict[str, int] = {}

        self.choice_labels = choice_labels

    def add_answer(self, monitor_id: str, answer: int):
        self._answers[monitor_id] = answer


class MultiSelectedAnswerColumn(AnswerColumn):
    ANSWERS_TYPE = 'MULTI_SELECTED_ANSWERS'

    def __init__(self,
                 item_name: str or Dict[int, str],
                 column_header: Dict[int, str],
                 column_no: Dict[int, int],
                 choice_labels: Dict[int, str]):
        self.item_name: str or Dict[int, str] = item_name
        self.column_header: Dict[int, str] = column_header
        self.column_no = column_no
        self._answers: Dict[str, List[int]] = {}

        self.choice_labels = choice_labels

    def add_answer(self, monitor_id: str, answer: List[int]):
        self._answers[monitor_id] = answer


class TextAnswerColumn(AnswerColumn):
    ANSWERS_TYPE = 'TEXT_ANSWERS'

    def __init__(self,
                 item_name: str,
                 column_header: str,
                 column_no: int):
        self.item_name: str = item_name
        self.column_header: str = column_header
        self.column_no = column_no
        self._answers: Dict[str, str] = {}

    def add_answer(self, monitor_id: str, answer: str):
        self._answers[monitor_id] = answer

    def to_dict(self):
        return {
            'item_name': self.item_name,
            'column_header': self.column_header,
            'column_no': self.column_no,
        }


class NumberAnswerColumn(AnswerColumn):
    ANSWERS_TYPE = 'NUMBER_ANSWERS'

    def __init__(self,
                 item_name: str,
                 column_header: str,
                 column_no: int,):
        self.item_name: str = item_name
        self.column_header: str = column_header
        self.column_no = column_no
        self._answers: Dict[str, int or float] = {}

    def add_answer(self, monitor_id: str, answer: int or float):
        self._answers[monitor_id] = answer


class PeriodAnswerColumn(AnswerColumn):
    ANSWERS_TYPE = 'PERIOD_ANSWERS'

    def __init__(self,
                 item_name: str,
                 column_header: str,
                 column_no: int):
        self.item_name: str = item_name
        self.column_header: str = column_header
        self.column_no = column_no
        self.item_name: str = item_name

    def add_answer(self, monitor_id: str, answer):
        pass


class DateTimeAnswerColumn(AnswerColumn):
    ANSWERS_TYPE = 'DATETIME_ANSWERS'

    def __init__(self,
                 item_name: str,
                 column_header: str,
                 column_no: int):
        self.item_name: str = item_name
        self.column_header: str = column_header
        self.column_no = column_no

    def add_answer(self, monitor_id: str, answer):
        pass
