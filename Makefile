python-test:
	python manage.py test

flake:
	flake8 ./

flake-format:
	 pyformat -r -i ./
	 flake8 ./

npm-build:
	`npm bin`/webpack --display-error-details

npm-build-watch:
	`npm bin`/webpack --watch --display-error-details

npm-test:
	npm run test

lint:
	`npm bin`/eslint nodes/

lint-format:
	`npm bin`/eslint nodes/ --fix

test-all:
	make python-test flake-format npm-test lint-format
