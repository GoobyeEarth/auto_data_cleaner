import json


class ApiSpec(object):
    @staticmethod
    def create_response(response_code: str, message: str, payload: dict) -> str:
        return json.dumps({
            'response_code': response_code,
            'message': message,
            'payload': payload,
        })

    class ResponseCode(object):
        SUCCESS = '0000'
        FILE_NOT_UPLOADED = '0001'
        DB_ERROR = '0002'
        UNEXPECTED_FILES_TYPE = '0003'
