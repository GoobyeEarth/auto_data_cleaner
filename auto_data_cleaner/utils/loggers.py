import logging


class Logger(object):
    @staticmethod
    def get():
        return logging.getLogger('command')
