from django.test import TestCase

from auto_data_cleaner.utils.api import ApiSpec


class ApiSpecTest(TestCase):
    def test_create_response(self):
        expected = '{"response_code": "this is response_code", "message": "this is message", ' \
                   '"payload": {"data": "this is payload"}}'

        actual = ApiSpec.create_response(
            'this is response_code',
            'this is message',
            {'data': 'this is payload'}
        )

        self.assertEqual(expected, actual)
