"""auto_data_cleaner URL Configuration.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))

"""
from django.conf.urls import url
from django.contrib import admin

from cleaning.views import api_excluded_answer
from cleaning.views import api_upload
from cleaning.views import download_raw_data_file
from cleaning.views import index as cleaning_index
from cleaning.views import test


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^cleaning/$', cleaning_index, name='cleaning'),
    url(r'^cleaning/api/upload/$', api_upload, name='cleaning_api_upload'),
    url(r'^cleaning/api/excluded_answer/$', api_excluded_answer,
        name='cleaning_api_excluded_answer'),
    url(r'^cleaning/download/raw-data/$', download_raw_data_file,
        name='cleaning_download_raw_data_file'),
    url(r'^test/$', test, name='test'),
]
