#!/usr/bin/env bash
TAG='goobyeearth/auto_data_cleaner'

docker build . -t goobyeearth/auto_data_cleaner


# on production
docker run \
    --privileged\
    -d \
    -p 80:80 \
    --name auto_data_cleaner_web \
    --mount  type=bind,source="$(pwd)",target=/opt/auto_data_cleaner \
    goobyeearth/auto_data_cleaner /sbin/init


# on local
docker run \
    --privileged\
    -d \
    -p 4000:80 \
    --name auto_data_cleaner_web \
    --mount  type=bind,source="$(pwd)",target=/opt/auto_data_cleaner \
    goobyeearth/auto_data_cleaner /sbin/init


docker exec -it auto_data_cleaner_web bash

# on docker
ln -s /opt/auto_data_cleaner/installation/wsgi.conf /etc/httpd/conf.d/wsgi.conf

cd /opt/auto_data_cleaner

npm install
pip3.6 install -r requirements.txt
pip3.6 install mod-wsgi
pip3.6 install git+https://github.com/miurahr/pykakasi
python3.6 manage.py migrate

make npm-build
python3.6 manage.py collectstatic

systemctl enable httpd.service
systemctl start httpd.service



